/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  typescript: {
    ignoreBuildErrors: true,
  },
  images: {
    domains: ['via.placeholder.com', 'i.ibb.co', 'https://c68b-177-128-192-93.ngrok-free.app'],
  },
};

module.exports = nextConfig;
