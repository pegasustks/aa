const cnpj = '00.000.000/0000-00';
const address = `Av. endereço da empresa, n.º XXXX, Cidade, Bairro/Estado - CEP 00000-000`;

export const company = {
  cnpj,
  address,
};
