export const linksFooter = [
  {
    id: '2',
    value: 'Termos de uso',
    link: '/company/information/terms_of_use',
  },
  {
    id: '3',
    value: 'Politicas de privacidade',
    link: '/company/information/privacy_terms',
  },
];
