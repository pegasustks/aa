import { Product_I } from 'typescript';

export const offers: Product_I[] = [
  {
    id: '1',
    frete: true,
    price: {
      actual: '137,00',
      old: '149,98',
      porcent: '10',
    },
    title: 'Casaco Macio com Capuz de Pelucia Quente Pele',
    image: 'https://via.placeholder.com/290x320',
    slug: {
      category: 'informática',
      filter: 'capa',
      subCategory: 'moto-g3',
      idProduct: '12154898765465',
    },
  },
  {
    id: '2',
    frete: true,
    price: {
      actual: '137,00',
      old: '149,98',
      porcent: '10',
    },
    title: 'Kit Sapatenis + Relógio',
    image: 'https://via.placeholder.com/290x320',
    slug: {
      category: 'informática',
      filter: 'Capa',
      subCategory: 'moto-g3',
      idProduct: '12154898765465',
    },
  },
  {
    id: '3',
    frete: true,
    price: {
      actual: '137,00',
      old: '149,98',
      porcent: '10',
    },
    title: 'Casaco Macio com Capuz de Pelucia Quente Pele',
    image: 'https://via.placeholder.com/290x320',
    slug: {
      category: 'informática',
      filter: 'Capa',
      subCategory: 'moto-g3',
      idProduct: '12154898765465',
    },
  },
  {
    id: '4',
    frete: true,
    price: {
      actual: '137,00',
      old: '149,98',
      porcent: '10',
    },
    title: 'Kit Sapatenis + Relógio',
    image: 'https://via.placeholder.com/290x320',
    slug: {
      category: 'informática',
      filter: 'Capa',
      subCategory: 'moto-g3',
      idProduct: '12154898765465',
    },
  },
  {
    id: '5',
    frete: true,
    price: {
      actual: '137,00',
      old: '149,98',
      porcent: '10',
    },
    title: 'Casaco Macio com Capuz de Pelucia Quente Pele',
    image: 'https://via.placeholder.com/290x320',
    slug: {
      category: 'informática',
      filter: 'Capa',
      subCategory: 'moto-g3',
      idProduct: '12154898765465',
    },
  },
];
