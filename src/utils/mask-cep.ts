export const mask_input_cep = (value: string): string => {
  const next_value = value
    .replace(/\D/g, '')
    .replace(/(\d{5})(\d)/, '$1-$2')
    .replace(/(-\d{3})\d+?$/, '$1');

  return next_value;
};
