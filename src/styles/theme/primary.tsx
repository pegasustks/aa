export const theme = {
  colors: {
    primary: '#193212',
    white: '#fff',
    grey: '#555',
    black: '#0f0f0f',
    black2: '#282828',
    hover: {
      primary: '#1b3a13',
      white: '#f0f0f0',
      black: '#111111',
    }
  },
  font: {
    primary: "'Roboto', sans-serif, sans ",
    secundary: "'Fraunces', sans-serif",
  },
  background: {
    white: '#fff',
    white2: '#fafafa',
    primary: '#193212',
    hover: {
      primary: '#1b3a13',
      white2: '#e8e8e8',
    },
    buttons: {
      primary: '#193212',
      hover: {
        primary: '#1b3a13',
      }
    }
  },
  'box-shadow': {
    primary: '0 2px 4px #5c5c5c58'
  }
};
