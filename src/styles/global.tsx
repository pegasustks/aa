import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: ${({ theme }) => theme.font.primary};
  }

  body {
    background-color: #e7e7e7;
    height: 100vh;
    overflow-y: scroll;
  }

  a {
    text-decoration: none;
  }
`;

export default GlobalStyles;
