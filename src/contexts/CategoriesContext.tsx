import React, {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";

interface HeaderCategory_I {
  name: string;
  id: string;
}

interface CategoriesProps {
  headerSubCategory: HeaderCategory_I[] | null;
  headerSubCategory2: HeaderCategory_I[] | null;
  headerSubCategory3: HeaderCategory_I[] | null;
  getSubCategory(idCategory: string): Promise<string>;
  getSubCategory2(idCategory: string): Promise<void>;
  getSubCategory3(idCategory: string): Promise<void>;
}

export const CategoriesContext = createContext({} as CategoriesProps);

interface porp {
  children: JSX.Element;
}

export const ProviderCategoriesContext: React.FC<porp> = ({
  children,
}: porp): JSX.Element => {
  const [headerSubCategory, setHeaderSubCategory] = useState<
    HeaderCategory_I[] | null
  >(null as HeaderCategory_I[] | null);

  const [headerSubCategory2, setHeaderSubCategory2] = useState<
    HeaderCategory_I[] | null
  >(null as HeaderCategory_I[] | null);

  const [headerSubCategory3, setHeaderSubCategory3] = useState<
    HeaderCategory_I[] | null
  >(null as HeaderCategory_I[] | null);

  const getSubCategory = useCallback(
    async (idCategory: string): Promise<string> => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/categorie/sub_category/${idCategory}`
        );
        setHeaderSubCategory(data.data.subcategories);
        setHeaderSubCategory2(null);
        setHeaderSubCategory3(null);
        return data.data.subcategories[0].id;
      } catch (error: any) {
        return "";
      }
    },
    []
  );

  const getSubCategory2 = useCallback(async (idCategory: string) => {
    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/categorie/sub_category2/${idCategory}`
      );
      setHeaderSubCategory2(data.data.subcategories2);
      setHeaderSubCategory3(null);
    } catch (error) {
      return;
    }
  }, []);

  const getSubCategory3 = useCallback(async (idCategory: string) => {
    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/categorie/sub_category3/${idCategory}`
      );
      setHeaderSubCategory3(data.data.subcategories3);
    } catch (error) {
      return;
    }
  }, []);

  const dataValue = useMemo(() => {
    return {
      getSubCategory,
      getSubCategory2,
      headerSubCategory,
      headerSubCategory2,
      getSubCategory3,
      headerSubCategory3,
    };
  }, [
    getSubCategory,
    getSubCategory2,
    getSubCategory3,
    headerSubCategory,
    headerSubCategory2,
    headerSubCategory3,
  ]);

  return (
    <CategoriesContext.Provider value={dataValue}>
      {children}
    </CategoriesContext.Provider>
  );
};
