import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { CategoriesContext } from "./CategoriesContext";

interface Open_I {
  sub1?: string;
  sub2?: string;
  id: string;
}

interface HeaderContextProps_I {
  open: Open_I;
  setOpen(e: Open_I): void;
  handleCategory(idCategory: string): Promise<void>;
  handleSubCategory(idCategory: string): Promise<void>;
  handleSubCategory2(idCategory: string): Promise<void>;
}

export const HeaderContext = createContext({} as HeaderContextProps_I);

interface ProviderProps_I {
  children: JSX.Element;
}

export const ProviderHeaderContext: React.FC<ProviderProps_I> = ({
  children,
}): JSX.Element => {
  const [open, setOpen] = useState<Open_I>({ id: "" } as Open_I);
  const { getSubCategory, getSubCategory2, getSubCategory3 } =
    useContext(CategoriesContext);

  const handleCategory = useCallback(
    async (category_id: string) => {
      await disableBodyScroll(document.querySelector("body")!);

      if (open.id !== category_id) {
        const id_subcategoria1 = await getSubCategory(category_id);
        await getSubCategory2(id_subcategoria1);
        setOpen({ id: category_id, sub1: id_subcategoria1 });
      }

      if (open.id === category_id) {
        await enableBodyScroll(document.querySelector("body")!);
        setOpen({ id: "" });
      }
    },
    [getSubCategory, getSubCategory2, open.id]
  );

  const handleSubCategory = useCallback(
    async (category_id: string) => {
      await disableBodyScroll(document.querySelector("body")!);

      if (open.id !== category_id) {
        await getSubCategory2(category_id);
        setOpen({ ...open, sub1: category_id });
      }
    },
    [getSubCategory2, open]
  );

  const handleSubCategory2 = useCallback(
    async (category_id: string) => {
      await disableBodyScroll(document.querySelector("body")!);

      if (open.id !== category_id) {
        await getSubCategory3(category_id);
        setOpen({ ...open, sub2: category_id });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [getSubCategory2, open]
  );

  const dataValue = useMemo(() => {
    return {
      handleCategory,
      handleSubCategory,
      open,
      setOpen,
      handleSubCategory2,
    };
  }, [handleCategory, handleSubCategory2, handleSubCategory, open, setOpen]);

  return (
    <HeaderContext.Provider value={dataValue}>
      {children}
    </HeaderContext.Provider>
  );
};
