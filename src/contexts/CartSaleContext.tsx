/* eslint-disable react/no-unused-prop-types */
import { useFrete } from "hooks/frete";
import produce from "immer";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import { AuthContext } from "./AuthContext";
import { TotalLoadingDataContext } from "./TotalLoadingDataContextContext";

type size_prop = {
  value: string;
  sizes_fashion_id: string;
};

type color_prop<S = any> = {
  color_value: string;
  color_name: string;
  id: string;
  size?: S;
};

export interface cart_prop {
  name_company: string;
  amount: number;
  id: string;
  product_id: string;
  title: string;
  price1: string;
  price2: string;
  repository: number;
  main_image: string;
  city: string;
  color: color_prop<size_prop>;
  frete: {
    de: number;
    por: number;
  };
  options?: {
    colors?: color_prop[];
    sizes?: size_prop[];
  };
}

interface change_size_params {
  item_id: string;
  product_id: string;
  sizes_fashion_id: string;
}

interface get_main_image_params {
  color_id: string;
  product_id: string;
}

interface get_price_size_params {
  color_id: string;
  sizes_fashion_id: string;
}

interface change_color_params {
  item_id: string;
  product_id: string;
  color_id: string;
}

interface change_amount_params {
  item_id: string;
  product_id: string;
  new_amount: number;
  size_fashion_id: string;
}

interface CartSaleContextProp {
  data_cart: cart_prop[];
  load_color: boolean;
  load_amount: boolean;
  load_delete: string | null;
  load_size_change: boolean;
  load_image_get: boolean;
  on_change_size_item(e: change_size_params): Promise<void>;
  on_delete_item_cart_sale(product_id: string, item_id: string): Promise<void>;
  on_change_amount_item(e: change_amount_params): Promise<void>;
  on_change_color_item(e: change_color_params): Promise<void>;
}

export const CartSaleContext = createContext({} as CartSaleContextProp);

interface prop {
  children: JSX.Element;
  data_cart_server: cart_prop[];
}

export const ProviderCartSaleContext: React.FC<prop> = ({
  children,
  data_cart_server
}: prop): JSX.Element => {
  const { auth_data, on_logout } = useContext(AuthContext);
  const { set_exist, on_get_amount_cart_sale } = useContext(
    TotalLoadingDataContext
  );

  const [data_cart, set_data_cart] = useState<cart_prop[]>(data_cart_server as cart_prop[]);

  const on_get_sizes_values_of_product = useCallback(
    async (id: string): Promise<any> => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-4-sizes-values/${id}`
        );
        return data.data.map((e: any) => {
          return {
            value: e.value,
            sizes_fashion_id: e.sizes_fashion_id,
            repository: e.repository,
          };
        });
      } catch (error) {
        return null;
      }
    },
    []
  );

  const [load_delete, set_load_delete] = useState<string | null>(null);
  const on_delete_item_cart_sale = useCallback(
    async (product_id: string, item_id: string) => {
      try {
        await set_load_delete(item_id);
        await ProviderAPI.delete(
          `/v1/user/delete/cart-sale/${product_id}/${item_id}`,
          {
            headers: {
              authorization: `BEARER ${auth_data !== 0 ? auth_data?.token : undefined ?? ''}`,
            },
          }
        );
        set_data_cart(data_cart.filter((e) => e.id !== item_id));
        await on_get_amount_cart_sale();
        await set_exist(null);
        await set_load_delete(null);
      } catch (error: any) {
        if (error?.response.status === 401) return on_logout();
      }
    },
    [auth_data, data_cart, on_get_amount_cart_sale, on_logout, set_exist]
  );

  const get_size_price_when_selecting = useCallback(
    async ({
      color_id,
      sizes_fashion_id,
    }: get_price_size_params): Promise<any> => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-7-prices-color/${sizes_fashion_id}/${color_id}/`
        );
        return data.data;
      } catch (error) {
        return null;
      }
    },
    []
  );

  const [load_size_change, set_load_size_change] = useState<boolean>(false);
  const on_change_size_item = useCallback(
    async ({ item_id, product_id, sizes_fashion_id }: change_size_params) => {
      try {
        set_load_size_change(true);
        await ProviderAPI.put(
          `/v1/user/update/cart-sale-item-size/${item_id}/${product_id}/${sizes_fashion_id}`,
          undefined,
          {
            headers: {
              authorization: `BEARER ${auth_data?.token}`,
            },
          }
        );
        const find = data_cart.find((e) => e.id === item_id);

        const data_price = await get_size_price_when_selecting({
          color_id: find?.color.id ?? "",
          sizes_fashion_id,
        });

        const find_sizes_selected = find?.options?.sizes?.find(
          (e) => e.sizes_fashion_id === sizes_fashion_id
        );

        const next_state_options = find?.options?.sizes?.filter(
          (e) => e.sizes_fashion_id !== sizes_fashion_id
        );

        const next_state = await produce(data_cart, (draft) => {
          const d = draft.map((item) => {
            if (item.id === item_id) {
              // atualizando preço
              item.price1 = data_price.price_unit;
              item.repository = data_price.repository;
              item.price2 = data_price.price_unit2;

              // atualizando tamanho
              item.color.size!.sizes_fashion_id =
                find_sizes_selected?.sizes_fashion_id ?? "";
              item.color.size!.value = find_sizes_selected?.value ?? "";

              next_state_options?.push({
                sizes_fashion_id: find?.color?.size?.sizes_fashion_id ?? "",
                value: find?.color?.size?.value ?? "",
              });

              item.options!.sizes = next_state_options;
              return item;
            }
            return item;
          });
          draft = d;
        });
        set_data_cart(next_state);
        set_load_size_change(false);
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [auth_data?.token, data_cart, get_size_price_when_selecting, on_logout]
  );

  const [load_image_get, set_load_image_get] = useState<boolean>(false);
  const on_get_image_item = useCallback(
    async ({ product_id, color_id }: get_main_image_params): Promise<string | void> => {
      try {
        set_load_image_get(true);
        const { data } = await ProviderAPI.get(
          `/v1/user/get/main-image-color/${product_id}/${color_id}`,
          {
            headers: {
              authorization: `BEARER ${auth_data?.token}`,
            },
          }
        );
        set_load_image_get(false);
        return data.data;
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [auth_data?.token, on_logout]
  );

  const [load_color, set_load_color] = useState<boolean>(false);
  const on_change_color_item = useCallback(
    async ({ item_id, product_id, color_id }: change_color_params) => {
      try {
        set_load_color(true);
        await ProviderAPI.put(
          `/v1/user/update/cart-sale-item-color/${item_id}/${product_id}/${color_id}`,
          undefined,
          {
            headers: {
              authorization: `BEARER ${auth_data?.token}`,
            },
          }
        );

        const main_image = await on_get_image_item({ product_id, color_id });

        const data_sizes_value = await on_get_sizes_values_of_product(color_id);

        const data_price = await get_size_price_when_selecting({
          color_id,
          sizes_fashion_id: data_sizes_value[0].sizes_fashion_id,
        });

        const find = data_cart.find((e) => e.id === item_id);
        const color_select = find?.options?.colors?.find(
          (e) => e.id === color_id
        );

        const next_state = await produce(data_cart, (draft) => {
          const d = draft.map((item) => {
            if (item.id === item_id) {
              // setar primeiro tamanho
              item.color.size!.sizes_fashion_id =
                data_sizes_value[0].sizes_fashion_id;
              item.color.size!.value = data_sizes_value[0].value;
              item.main_image = main_image || "";

              const next_state_options = data_sizes_value.filter(
                (e: any) =>
                  e.sizes_fashion_id !== data_sizes_value[0].sizes_fashion_id
              );

              // setar sizes
              item.options!.sizes = next_state_options;

              // setar preço
              item.price1 = data_price.price_unit;
              item.repository = data_price.repository;
              item.price2 = data_price.price_unit2;

              item.color.color_value = color_select?.color_value ?? "";
              item.color.id = color_select?.id ?? "";
              item.color.color_name = color_select?.color_name ?? "";

              const remove_color_selected_in_options =
                item.options?.colors?.filter((e) => e.id !== color_id);

              remove_color_selected_in_options?.push({
                id: find?.color.id ?? "",
                color_name: find?.color.color_name ?? "",
                color_value: find?.color.color_value ?? "",
              });
              item.options!.colors = remove_color_selected_in_options;
              return item;
            }
            return item;
          });
          draft = d;
        });
        set_data_cart(next_state);
        set_load_color(false);
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [
      auth_data?.token,
      data_cart,
      get_size_price_when_selecting,
      on_get_image_item,
      on_get_sizes_values_of_product,
      on_logout,
    ]
  );

  const [load_amount, set_load_amount] = useState<boolean>(false);
  const on_change_amount_item = useCallback(
    async ({ item_id, product_id, new_amount, size_fashion_id }: change_amount_params) => {
      try {
        const obj_auth = {
          headers: {
            authorization: `BEARER ${auth_data?.token}`,
          },
        }

        set_load_amount(true);
        await ProviderAPI.put(
          `/v1/user/update/cart-sale-amount/${item_id}/${product_id}/${new_amount}`,
          undefined,
          obj_auth,
        );

        const { data } = await ProviderAPI.get(`/v1/public/get/product-2-step-9-prices-frete/${product_id}/${size_fashion_id}/${new_amount}`, obj_auth);

        const next_state = produce(data_cart, (draft) => {
          const d = draft.map((item) => {
            if (item.id === item_id) {
              item.amount = new_amount;
              item.frete = data.data.response.vlrFrete;
              return item;
            }
            return item;
          });
          draft = d;
        });

        set_data_cart(next_state);
        set_load_amount(false);
        on_get_amount_cart_sale();
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [auth_data?.token, data_cart, on_get_amount_cart_sale, on_logout]
  );

  const value_data = useMemo(() => {
    return {
      data_cart,
      load_color,
      load_amount,
      load_delete,
      load_image_get,
      load_size_change,
      on_change_size_item,
      on_delete_item_cart_sale,
      on_change_amount_item,
      on_change_color_item,
    };
  }, [
    data_cart,
    load_color,
    load_amount,
    load_delete,
    load_image_get,
    load_size_change,
    on_change_size_item,
    on_delete_item_cart_sale,
    on_change_amount_item,
    on_change_color_item,
  ]);

  return (
    <CartSaleContext.Provider value={value_data}>
      {children}
    </CartSaleContext.Provider>
  );
};
