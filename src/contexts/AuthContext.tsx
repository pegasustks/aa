import { deleteCookie, setCookie } from "cookies-next";
import { useRouter } from "next/router";
import { createContext, useCallback, useMemo, useReducer, useState } from "react";
import { ProviderAPI } from "services/api";
import { sign } from "jsonwebtoken";
import { AuthDefaultValues, AuthReducer } from "./reducers/AuthReducer";

interface user_prop {
  full_name?: string | null;
  cpf?: string | null;
  city?: string | null;
  state?: string | null;
  cep?: string | null;
}

export interface auth_prop {
  id: string | null;
  token: string | null;
}

interface error_prop {
  message: string;
  field: string;
}

interface AuthProps {
  user_data?: user_prop;
  auth_data?: auth_prop | null | 0;
  load_login: boolean;
  error_data: error_prop[];
  set_error_data(e: error_prop[]): void;
  set_user_data(e: user_prop): void;
  set_auth_data(e: auth_prop | 0): void;
  on_login(e: any, query?: any): Promise<void>;
  on_logout(): void;
}

export const AuthContext = createContext<AuthProps>({} as AuthProps);

interface prop {
  children: JSX.Element;
}

export const ProviderAuthContext: React.FC<prop> = ({
  children,
}): JSX.Element => {
  const [auth_data, set_auth_data] = useState<auth_prop | null | 0>(null as auth_prop | null | 0);
  const [user_data, set_user_data] = useState<user_prop>({} as user_prop);

  const [load_login, set_load_login] = useState<boolean>(false);
  const [error_data, set_error_data] = useState<error_prop[]>(
    [] as error_prop[]
  );

  const { push } = useRouter();

  const on_login = useCallback(
    async (data: any, query?: any) => {
      try {
        set_load_login(true);
        const { data: data_response } = await ProviderAPI.post(
          "/v1/public/create/login",
          data
        );
        deleteCookie("auth");
        setCookie("auth", data_response.data.token, {
          secure: true,
          path: "/",
          sameSite: "strict",
          maxAge: 60 * 60 * 12,
        });

        if (query?.p && query?.a && query?.c && query?.s) {
          await ProviderAPI.post(
            "/v1/user/create/cart-sale",
            {
              product2_id: query.p,
              amount: query.a,
              color_product_id: query.c,
              sizes_fashion_id: query.s,
              ...(query?.it && { influencer: query?.it })
            },
            {
              headers: {
                authorization: `BEARER ${data_response.data.token}`,
              },
            }
          );
          window.location.href = "/carrinho";
          return;
        };
        window.location.href = "/";
      } catch (error: any) {
        set_load_login(false);
        if (error?.response.status === 422)
          return set_error_data(error?.response?.data.data);
        if (error?.response.status === 301) {
          const e = sign({ e: data.whatsapp }, "whatsapp_Lariizana", {
            expiresIn: 60,
          });
          push(`/account/confirmacao-de-conta?e=${e}`);
          return;
        }
      }
    },
    [push]
  );

  const on_logout = useCallback(() => {
    deleteCookie("auth");
    window.location.href = "/";
  }, []);

  const data_values = useMemo(() => {
    return {
      set_user_data,
      user_data,
      set_auth_data,
      auth_data,
      on_login,
      load_login,
      error_data,
      set_error_data,
      on_logout,
    };
  }, [
    set_user_data,
    user_data,
    set_auth_data,
    auth_data,
    on_login,
    load_login,
    error_data,
    set_error_data,
    on_logout,
  ]);

  return (
    <AuthContext.Provider value={data_values}>{children}</AuthContext.Provider>
  );
};
