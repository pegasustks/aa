import { setCookie } from "cookies-next";
import Head from "next/head";
import { useRouter } from "next/router";
import { createContext, useCallback, useMemo, useState } from "react";
import { ProviderAPI } from "services/api";

interface data_prop {
  full_name: string;
  whatsapp: string;
  cpf: string;
  birth_date: string;
  genre: number;
  password: string;
  confirm_password: string;
}

interface data_errors_prop {
  field: string;
  message: string;
}

interface user_prop {
  first_name: string;
  id: string;
  token: string;
}

interface RegisterProps {
  data_fields: data_prop;
  data_errors: data_errors_prop[];
  response_error: string | null;
  load_register: boolean;
  load_confirm: boolean;
  set_data_fields(e: data_prop): void;
  set_data_errors(e: data_errors_prop[]): void;
  on_register(setStepRegister: (e: number) => void): Promise<void>;
  on_confirm_whatsapp(
    setStepRegister: (e: number) => void,
    code: string
  ): Promise<void>;
  load_address: boolean;
  on_create_address(e: any): Promise<void>;
}

export const RegisterContext = createContext({} as RegisterProps);

interface props {
  children: JSX.Element;
}

export const ProviderRegisterContext: React.FC<props> = ({
  children,
}: props): JSX.Element => {
  const { query } = useRouter();
  const [response_error, set_response_error] = useState<string | null>(null);
  const [user, set_user] = useState<user_prop | null>(null);

  const [data_fields, set_data_fields] = useState<data_prop>({
    genre: 1,
  } as unknown as data_prop);

  const [data_errors, set_data_errors] = useState<data_errors_prop[]>(
    [] as data_errors_prop[]
  );

  const [load_register, set_load_register] = useState<boolean>(false);

  const on_register = useCallback(
    async (setStepRegister: (e: number) => void) => {
      try {
        await set_load_register(true);
        const cpf = data_fields?.cpf?.replace(/\D/g, "");
        const { data: data_ } = await ProviderAPI.post(
          "/v1/public/create/register",
          { ...data_fields, cpf }
        );
        set_user(data_.data);
        await set_load_register(false);
        setStepRegister(1);
      } catch (error: any) {
        await set_load_register(false);
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }

        await set_response_error(error?.response?.data.message);
        setTimeout(() => {
          set_response_error(null);
        }, 10000);
      }
    },
    [data_fields]
  );

  const [load_confirm, set_load_confirm] = useState<boolean>(false);

  // confirm code
  const on_confirm_whatsapp = useCallback(
    async (setStepRegister: (e: number) => void, code: string) => {
      try {
        set_load_confirm(true);
        const { data: data_ } = await ProviderAPI.put(
          `/v1/public/update/confirm-code-user/${code}?id=${user?.id}`,
          undefined
        );
        set_user({ ...user!, token: data_.data });
        setCookie("auth", data_.data, {
          secure: true,
          path: "/",
          sameSite: "strict",
          maxAge: 60 * 60 * 12,
        });
        if (query?.p && query?.a && query?.c, query?.s) {
          await ProviderAPI.post(
            "/v1/user/create/cart-sale",
            {
              product2_id: query.p,
              amount: query.a,
              color_product_id: query.c,
              sizes_fashion_id: query.s,
              ...(query?.it && { influencer: query.it })
            },
            {
              headers: {
                authorization: `BEARER ${data_.data}`,
              },
            }
          );
        }
        set_load_confirm(false);
        setStepRegister(2);
      } catch (error: any) {
        set_load_confirm(false);
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }

        set_response_error(error?.response?.data.message);
        setTimeout(() => {
          set_response_error(null);
        }, 10000);
      }
    },
    [query, user]
  );

  const [load_address, set_load_address] = useState<boolean>(false);

  // create address
  const on_create_address = useCallback(
    async (data: any) => {
      try {
        await set_load_address(true);
        await ProviderAPI.post(
          `/v1/user/create/address`,
          {
            ...data,
            city: data.city.value,
            district: data.district.value,
            state: data.state.value,
          },
          {
            headers: {
              authorization: `BEARER ${user?.token}`,
            },
          }
        );
        await set_load_address(false);
        window.location.href = query.p ? "/carrinho" : "/";
      } catch (error: any) {
        await set_load_address(false);
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }

        await set_response_error(error?.response?.data.message);
        setTimeout(() => {
          set_response_error(null);
        }, 10000);
      }
    },
    [query, user]
  );

  const dataValue = useMemo(() => {
    return {
      data_fields,
      data_errors,
      response_error,
      load_register,
      load_confirm,
      set_data_fields,
      set_data_errors,
      on_register,
      on_confirm_whatsapp,
      load_address,
      on_create_address,
    };
  }, [
    data_fields,
    data_errors,
    response_error,
    load_register,
    load_confirm,
    set_data_fields,
    set_data_errors,
    on_register,
    on_confirm_whatsapp,
    load_address,
    on_create_address,
  ]);

  return (
    <>
      <Head>
        <title>Criar conta</title>
      </Head>
      <RegisterContext.Provider value={dataValue}>
        {children}
      </RegisterContext.Provider>
    </>
  );
};
