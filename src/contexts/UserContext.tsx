import Head from "next/head";
import { useRouter } from "next/router";
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import { AuthContext } from "./AuthContext";

interface user_data_prop {
  cpf?: string | null;
  whatsapp?: string | null;
  genre?: string | null;
  full_name?: string | null;
  birth_date?: string | null;
}

interface address_prop {
  cep: string;
  state: string;
  city: string;
  address: string;
  district: string;
  number: string;
  complement: string;
  id?: string;
  geolocation?: string;
}

interface data_errors_prop {
  field: string;
  message: string;
}

interface status_public_T {
  message: string;
  date_create: Date;
}

interface my_sale_prop {
  id: string;
  code_sale: string;
  address: string;
  state: string;
  date_created: string;
  full_name: string;
  total_price: string;
  complement: string;
  cep: string;
  district: string;
  total_frete: string;
  cpf: string;
  status: string;
  number: number;
  items: [
    {
      amount: number;
      color_value: string;
      frete_price: string;
      id: string;
      price: string;
      reversed: null | number;
      size_value: string;
      status: string;
      title: string;
      delivery_date: Date;
      delivery_service: string;
      tracking_code: string | null;
      historico: string[];
      data_status: string;
      status_public?: status_public_T[];
    }
  ];
}

interface UserContextProp {
  step: number;
  change_pass: boolean;
  load_delete: boolean;
  address: address_prop | null;
  load_address: boolean;
  load_change_p: boolean;
  user_data: user_data_prop;
  load_create_address: boolean;
  status_change_p: boolean | null;
  data_errors: data_errors_prop[];
  load_update_address: boolean;
  on_update_address_user(
    data: Omit<Partial<address_prop>, "id">,
    id: string
  ): Promise<void>;
  load_my_sales: boolean;
  my_sales: my_sale_prop[] | null;
  setStep(e: number): void;
  on_get_my_sales_user(): Promise<void>;
  on_get_data_user(): Promise<void>;
  set_change_pass(e: boolean): void;
  on_get_address_user(): Promise<void>;
  on_change_password(e: any): Promise<void>;
  set_data_errors(e: data_errors_prop[]): void;
  on_delete_address_user(e: string): Promise<void>;
  on_create_address_user(daat: address_prop): Promise<void>;
  on_cancel_item_request_user(
    parther_request_id: string,
    number_whatsapp: string
  ): Promise<void>;
}

export const UserContext = createContext({} as UserContextProp);

interface prop {
  children: JSX.Element;
  data_user: any;
}

export const ProviderUserContext: React.FC<prop> = ({
  children,
  data_user,
}): JSX.Element => {
  const { query } = useRouter();

  const [user_data, set_user_data] = useState<user_data_prop>(
    {} as user_data_prop
  );
  const { user_data: d_user, on_logout } = useContext(AuthContext);

  const [data_errors, set_data_errors] = useState<data_errors_prop[]>(
    [] as data_errors_prop[]
  );
  const [change_pass, set_change_pass] = useState<boolean>(false);
  const [step, setStep] = useState<number>(1);
  const [status_change_p, set_status_change_p] = useState<boolean | null>(null);
  const [load_change_p, set_load_change_p] = useState<boolean>(false);
  const [load_address, set_load_address] = useState<boolean>(false);
  const [address, set_address] = useState<address_prop | null>(
    {} as address_prop | null
  );
  const [load_my_sales, set_load_my_sales] = useState<boolean>(false);
  const [my_sales, set_my_sales] = useState<my_sale_prop[] | null>(
    {} as my_sale_prop[] | null
  );

  useEffect(() => {
    if (query?.address === "1") setStep(2);
    if (query?.mysales === "1") setStep(3);
  }, [query]);

  const on_cancel_item_request_user = useCallback(
    async (parther_request_id: string, number_whatsapp: string) => {
      try {
        const { data } = await ProviderAPI.put(
          `/v1/user/update/cancel-parther-request/${parther_request_id}/${number_whatsapp}`,
          undefined,
          {
            headers: { authorization: `BEARER ${data_user.token}` },
          }
        );
        if (data.message.length > 9) {
          return alert(data.message);
        }
        const next_state = my_sales?.map((e) => {
          if (e.id === parther_request_id) {
            e.status = "canceled";
            return e;
          }
          return e;
        });
        set_my_sales(next_state ?? null);
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [data_user?.token, my_sales, on_logout]
  );

  const on_get_my_sales_user = useCallback(async () => {
    try {
      set_load_my_sales(true);
      const { data } = await ProviderAPI.get(`/v1/user/get/my-sales/0`, {
        headers: { authorization: `BEARER ${data_user.token}` },
      });
      set_my_sales(data.data);
      set_load_my_sales(false);
    } catch (error: any) {
      set_load_my_sales(false);
      if (error?.response?.status === 401) return on_logout();
    }
  }, [data_user?.token, on_logout]);

  const on_change_password = useCallback(
    async (data_p: any) => {
      try {
        set_load_change_p(true);
        await ProviderAPI.put(
          `/v1/user/update/password/${data_p?.actual ?? "0"}/${data_p?.new ?? "0"
          }/${data_p?.repeat ?? "0"}`,
          undefined,
          {
            headers: {
              authorization: `BEARER ${data_user?.token}`,
            },
          }
        );
        set_load_change_p(false);
        set_status_change_p(true);
        setTimeout(() => {
          set_status_change_p(null);
          set_change_pass(false);
        }, 4000);
      } catch (error: any) {
        set_load_change_p(false);
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }
        if (error?.response?.status === 401) {
          return on_logout();
        }
      }
    },
    [data_user?.token, on_logout]
  );

  const on_get_address_user = useCallback(async () => {
    try {
      set_load_address(true);
      const { data } = await ProviderAPI.get(`/v1/user/get/address-user`, {
        headers: { authorization: `BEARER ${data_user.token}` },
      });
      set_address(data.data);
      set_load_address(false);
    } catch (error: any) {
      set_load_address(false);
      if (error?.response?.status === 401) return on_logout();
    }
  }, [data_user?.token, on_logout]);

  const [load_delete, set_load_delete] = useState<boolean>(false);
  const on_delete_address_user = useCallback(
    async (id: string) => {
      try {
        set_load_delete(true);
        await ProviderAPI.delete(`/v1/user/deleto/address/${id}`, {
          headers: { authorization: `BEARER ${data_user.token}` },
        });
        set_address(null);
        set_load_delete(false);
      } catch (error: any) {
        set_load_delete(false);
        if (error?.response?.status === 401) return on_logout();
      }
    },
    [data_user?.token, on_logout]
  );

  const [load_create_address, set_load_create_address] =
    useState<boolean>(false);
  const on_create_address_user = useCallback(
    async (daat: address_prop) => {
      try {
        set_load_create_address(true);
        await ProviderAPI.post(`/v1/user/create/address`, daat, {
          headers: { authorization: `BEARER ${data_user.token}` },
        });
        set_address(daat);
        set_load_create_address(false);
        if (query.prod) {
          window.location.href = `/${query.prod}?it=${query.it ?? ''}`
        }
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }
      }
    },
    [data_user.token, on_logout, query.it, query.prod]
  );

  const [load_update_address, set_load_update_address] =
    useState<boolean>(false);
  const on_update_address_user = useCallback(
    async (daat: Omit<Partial<address_prop>, "id">, id: string) => {
      try {
        set_load_update_address(true);
        await ProviderAPI.put(
          `/v1/user/update/address/${id}`,
          {
            geolocation: daat?.geolocation,
            state: daat?.state,
            number: daat?.number,
            district: daat?.district,
            complement: daat?.complement,
            city: daat?.city,
            cep: daat?.cep,
            address: daat?.address,
          },
          {
            headers: { authorization: `BEARER ${data_user.token}` },
          }
        );
        set_address({ ...address!, ...daat });
        set_load_update_address(false);
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }
      }
    },
    [address, data_user.token, on_logout]
  );

  const on_get_data_user = useCallback(async () => {
    const dataa = await Promise.all(
      ["whatsapp", "birth_date", "genre"].map(async (field) => {
        const { data } = await ProviderAPI.get(
          `/v1/user/get/one-field/${field}`,
          {
            headers: { authorization: `BEARER ${data_user.token}` },
          }
        );
        return {
          [field]: data.data[field],
        };
      })
    );
    set_user_data({
      ...d_user,
      whatsapp: dataa[0].whatsapp,
      birth_date: dataa[1].birth_date,
      genre: dataa[2].genre === 1 ? "Femenino" : "Masculino",
    });
  }, [d_user, data_user?.token]);

  const data_value = useMemo(() => {
    return {
      step,
      address,
      user_data,
      load_delete,
      load_address,
      data_errors,
      change_pass,
      load_change_p,
      status_change_p,
      load_create_address,
      load_update_address,
      setStep,
      set_change_pass,
      set_data_errors,
      on_get_data_user,
      on_change_password,
      on_get_address_user,
      on_update_address_user,
      on_create_address_user,
      on_delete_address_user,
      on_get_my_sales_user,
      on_cancel_item_request_user,
      load_my_sales,
      my_sales,
    };
  }, [
    step,
    address,
    user_data,
    load_delete,
    data_errors,
    load_address,
    change_pass,
    load_change_p,
    status_change_p,
    load_create_address,
    load_update_address,
    setStep,
    set_change_pass,
    set_data_errors,
    on_get_data_user,
    on_change_password,
    on_get_address_user,
    on_update_address_user,
    on_create_address_user,
    on_delete_address_user,
    on_get_my_sales_user,
    on_cancel_item_request_user,
    load_my_sales,
    my_sales,
  ]);

  return (
    <>
      <Head>
        <title>Minha conta</title>
      </Head>
      <UserContext.Provider value={data_value}>{children}</UserContext.Provider>
    </>
  );
};
