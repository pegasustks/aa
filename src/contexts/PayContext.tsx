import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import QRCode from "qrcode";
import { AuthContext } from "./AuthContext";

interface prop_order_payment {
  qr_code: string;
  url_qr: string;
  code_request: string;
  total_price: number;
  id_request: string;
  id: string;
}

interface PayContextProp {
  sale_code?: string;
  status_pay: boolean;
  method_pay?: string;
  data_list_request?: any[];
  data_order_payment: prop_order_payment;
  load_verify_status: boolean;
  set_method_pay(e: string): void;
  on_get_cart_sale(): Promise<void>;
  on_verify_order_payment(): Promise<void>;
  on_create_payment_sale(): Promise<void>;
}

export const PayContext = createContext({} as PayContextProp);

interface prop {
  children: JSX.Element;
}

export const ProviderPayContext: React.FC<prop> = ({
  children,
}: prop): JSX.Element => {
  const { auth_data, on_logout } = useContext(AuthContext);
  const [data_order_payment, set_data_order_payment] = useState(
    {} as prop_order_payment
  );

  const [data_list_request, set_data_list_request] = useState<any[]>(
    [] as any[]
  );

  const [method_pay, set_method_pay] = useState<string>("");

  const on_get_prices_of_frete_product = useCallback(
    async (product_id: string, sizes_fashion_id: number, amount: number) => {
      try {
        const { data: response_step_9_price_frete } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-9-prices-frete/${product_id}/${sizes_fashion_id}/${amount}`, {
          headers: {
            authorization: `BEARER ${auth_data?.token}`,
          },
        }
        );
        return response_step_9_price_frete.data.response.vlrFrete;
      } catch (error) {
        return null;
      }
    },
    [auth_data?.token]
  );

  const on_get_cart_sale = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get("/v1/user/get/cart-sale", {
        headers: {
          authorization: `BEARER ${auth_data?.token}`,
        },
      });
      const next_state = await data.data.map((e: any) => {
        (async () => {
          const frete = await on_get_prices_of_frete_product(e.product_id, e.color.size.sizes_fashion_id, e.amount);
          e.frete = Number(frete.por).toFixed(2);
        })();
        return e;
      });
      set_data_list_request(next_state);
    } catch (error: any) {
      if (error?.response.status === 401) return on_logout();
    }
  }, [auth_data?.token, on_get_prices_of_frete_product, on_logout]);

  const on_create_payment_sale = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.post(
        "/v1/user/create/order-payment",
        undefined,
        {
          headers: { authorization: `BEARER ${auth_data?.token}` },
        }
      );
      const url_qr = await QRCode.toDataURL(data.data.qr_code);
      set_data_order_payment({
        ...data.data,
        url_qr,
      });
    } catch (error: any) {
      if (error?.response?.status === 401) return on_logout();
    }
  }, [auth_data?.token, on_logout]);

  const [status_pay, set_status_pay] = useState(false);
  const [load_verify_status, set_load_verify_status] = useState(false);

  const on_verify_order_payment = useCallback(async () => {
    try {
      set_load_verify_status(true);
      const { data } = await ProviderAPI.patch(
        `/v1/user/patch/order-verify/${data_order_payment.id_request}`,
        undefined,
        { headers: { authorization: `BEARER ${auth_data?.token}` } }
      );
      set_status_pay(data.data === "approved");

      if (data.data === "approved") {
        setTimeout(() => {
          window.location.href = "/account/user?mysales=1";
        }, 3000);
      }
      set_load_verify_status(false);
    } catch (error: any) {
      if (error?.response?.status === 401) return on_logout();
    }
  }, [data_order_payment, auth_data?.token, on_logout]);

  const data_value = useMemo(() => {
    return {
      on_create_payment_sale,
      on_get_cart_sale,
      set_method_pay,
      on_verify_order_payment,
      status_pay,
      method_pay,
      data_list_request,
      data_order_payment,
      load_verify_status,
    };
  }, [
    on_create_payment_sale,
    on_get_cart_sale,
    set_method_pay,
    on_verify_order_payment,
    status_pay,
    method_pay,
    data_list_request,
    data_order_payment,
    load_verify_status,
  ]);

  return (
    <PayContext.Provider value={data_value}>{children}</PayContext.Provider>
  );
};
