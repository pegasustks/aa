import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import { AuthContext } from "./AuthContext";

interface user_data_prop {
  cpf?: string;
  email?: string;
  genre?: string;
  full_name?: string;
  birth_date?: string;
}

interface address_prop {
  cep: string;
  state: string;
  city: string;
  address: string;
  district: string;
  number: string;
  complement: string;
  id?: string;
  geolocation?: string;
}

interface data_errors_prop {
  field: string;
  message: string;
}

interface PayAddressContextProp {
  address: address_prop | null;
  data_errors: data_errors_prop[];
  load_address: boolean;
  load_create_address: boolean;
  set_data_errors(e: data_errors_prop[]): void;
  on_get_address_user(): Promise<void>;
  on_create_address_user(daat: address_prop): Promise<void>;
  set_address_id(address_id: string): void;
  address_selected_id: string | null;
}

export const PayAddressContext = createContext({} as PayAddressContextProp);

interface prop {
  children: JSX.Element;
  data_user: any;
}

export const ProviderPayAddressContext: React.FC<prop> = ({
  children,
  data_user,
}): JSX.Element => {
  // const [user_data, set_user_data] = useState<user_data_prop>(
  //   {} as user_data_prop
  // );
  const { user_data: d_user, on_logout } = useContext(AuthContext);

  const [data_errors, set_data_errors] = useState<data_errors_prop[]>(
    [] as data_errors_prop[]
  );
  const [load_address, set_load_address] = useState<boolean>(false);
  const [address, set_address] = useState<address_prop | null>(
    {} as address_prop | null
  );

  const [address_selected_id, set_address_selected_id] = useState<
    string | null
  >(null);

  /**
   * @description Guardar ID do endereço selecionado.
   * @param address_id string
   */
  const set_address_id = useCallback((address_id: string): void => {
    set_address_selected_id(address_id);
  }, []);

  const on_get_address_user = useCallback(async () => {
    try {
      set_load_address(true);
      const { data } = await ProviderAPI.get(`/v1/user/get/address-user`, {
        headers: { authorization: `BEARER ${data_user.token}` },
      });
      set_address(data.data);
      set_load_address(false);
    } catch (error: any) {
      set_load_address(false);
      if (error?.response?.status === 401) return on_logout();
    }
  }, [data_user?.token, on_logout]);

  const [load_create_address, set_load_create_address] =
    useState<boolean>(false);
  const on_create_address_user = useCallback(
    async (daat: address_prop) => {
      try {
        set_load_create_address(true);
        const { data } = await ProviderAPI.post(
          `/v1/user/create/address`,
          daat,
          {
            headers: { authorization: `BEARER ${data_user.token}` },
          }
        );
        set_address(daat);
        set_load_create_address(false);
      } catch (error: any) {
        if (error?.response?.status === 401) return on_logout();
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }
      }
    },
    [data_user?.token, on_logout]
  );

  // const on_get_data_user = useCallback(async () => {
  //   const dataa = await Promise.all(
  //     ['email', 'birth_date', 'genre'].map(async field => {
  //       const { data } = await ProviderAPI.get(
  //         `/v1/user/get/one-field/${field}`,
  //         {
  //           headers: { authorization: `BEARER ${data_user.token}` },
  //         }
  //       );
  //       return {
  //         [field]: data.data[field],
  //       };
  //     })
  //   );
  //   set_user_data({
  //     ...d_user,
  //     email: dataa[0].email,
  //     birth_date: dataa[1].birth_date,
  //     genre: dataa[2].genre === 1 ? 'Femenino' : 'Masculino',
  //   });
  // }, [d_user, data_user?.token]);

  const data_value = useMemo(() => {
    return {
      address,
      // user_data,
      load_address,
      data_errors,
      set_data_errors,
      // on_get_data_user,
      on_get_address_user,
      on_create_address_user,
      load_create_address,
      set_address_id,
      address_selected_id,
    };
  }, [
    address,
    data_errors,
    load_address,
    set_data_errors,
    on_get_address_user,
    on_create_address_user,
    load_create_address,
    set_address_id,
    address_selected_id,
  ]);

  return (
    <PayAddressContext.Provider value={data_value}>
      {children}
    </PayAddressContext.Provider>
  );
};
