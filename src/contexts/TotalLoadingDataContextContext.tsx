import { CookiesInfoComponent } from "@components/CookiesInfo";
import { LayoutComponent } from "@components/Global/Layout";
import { LogoComponent } from "@components/Logo";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import styled from "styled-components";
import { AuthContext } from "./AuthContext";

interface prop_location_user_logout {
  cep: string,
  city: string,
}

interface social_I {
  instagram: null | string;
  linkedin: null | string;
  facebook: null | string;
  twitter: null | string;
}

const DivLoading = styled.div`
  position: fixed;
  width: 100%;
  height: 100vh;
  background: #f7f7f7;
  z-index: 9999;

  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

interface Provider {
  children: JSX.Element;
}

interface HeaderCategory {
  name: string;
  id: string;
}

export interface Advertising_I {
  title: string;
  long_image: string;
  medium_image: string;
  small_image: string;
  link?: string;
  id: string;
}


interface TotalLoadingDataProps {
  header_Category: HeaderCategory[] | null;
  setHeader_Category(e: HeaderCategory[] | null): void;
  city: string | null;
  set_city(e: string | null): void;
  open_modal_location: boolean;
  set_open_modal_location(e: boolean): void;
  amount_cart_sale: number;
  on_get_amount_cart_sale(token?: string): Promise<void>;
  exist: boolean | null;
  set_exist(e: boolean | null): void;
  social: social_I;
  location_user_logout: prop_location_user_logout | null;
  set_load_click(e: boolean): void;
  load_click: boolean;
}

export const TotalLoadingDataContext = createContext(
  {} as TotalLoadingDataProps
);

export const ProviderTotalLoadingDataContext: React.FC<Provider> = ({
  children,
}: Provider): JSX.Element => {
  const { auth_data, set_user_data, on_logout } = useContext(AuthContext);

  const { asPath } = useRouter();
  const [loading, setLoading] = useState<number>(0);

  // CATEGORIAS HEADER
  const [header_Category, setHeader_Category] = useState<
    HeaderCategory[] | null
  >(null as HeaderCategory[] | null);

  const [errorServer, setErrorServer] = useState<boolean>(false);

  const [city, set_city] = useState<string | null>(null);
  const [city_user, set_city_user] = useState<string | null>(null);

  const [open_modal_location, set_open_modal_location] =
    useState<boolean>(false);

  const [load_click, set_load_click] = useState<boolean>(false as boolean);

  const [exist, set_exist] = useState<boolean | null>(null);

  // const getHeaderCategory = useCallback(async () => {
  //   try {
  //     const { data } = await ProviderAPI.get(
  //       "/v1/public/get/categorie/category/0"
  //     );
  //     setHeader_Category(data.data.categories);
  //   } catch (error: any) {
  //     if (error?.response?.status === 401) throw { status: 401 };
  //     if (error?.response?.status === 500) throw { status: 500 };
  //     throw { status: 0 };
  //   }
  // }, []);

  const on_get_one_field_user = useCallback(
    async (field: string, token?: string): Promise<string | null | undefined | any> => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/user/get/one-field/${field}`,
          {
            headers: {
              authorization: `BEARER ${token ?? ""}`,
            },
          }
        );
        return data.data[field];
      } catch (error: any) {
        if (error?.response?.status === 401) throw { status: 401 };
        if (error?.response?.status === 500) throw { status: 500 };
        throw { status: 0 };
      }
    },
    []
  );

  const on_get_city_user = useCallback(async (field: string, token?: string): Promise<
    string | null | undefined | any
  > => {
    try {
      const { data } = await ProviderAPI.get(`/v1/user/get/one-field-location/${field}`, {
        headers: {
          authorization: `BEARER ${token}`,
        },
      });
      return data.data?.[field];
    } catch (error: any) {
      console.log(error);
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const on_get_cep_user = useCallback(async (token?: string): Promise<
    string | null | undefined | any
  > => {
    try {
      const { data } = await ProviderAPI.get(`/v1/user/get/cep-user`, {
        headers: {
          authorization: `BEARER ${token}`,
        },
      });
      return data.data;
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const [amount_cart_sale, set_amount_cart_sale] = useState<number>(0);

  const on_get_amount_cart_sale = useCallback(async (token?: string) => {
    try {
      const { data } = await ProviderAPI.get(`/v1/user/get/amount-cart-sale`, {
        headers: {
          authorization: `BEARER ${token}`,
        },
      });
      set_amount_cart_sale(data.data);
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const on_verify_user_exist = useCallback(async (token?: string): Promise<any> => {
    try {
      await ProviderAPI.get("/v1/user/get/verify-user-exist", {
        headers: {
          authorization: `BEARER ${token}`,
        },
      });
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const on_get_status_company = useCallback(async (field: string) => {
    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/company-field/${field}`
      );
      return data.data[field];
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const [open_site, set_open_site] = useState<null | number>(null);

  const [social, set_social] = useState<social_I>({
    instagram: null,
    linkedin: null,
    facebook: null,
    twitter: null,
  } as social_I);

  const loadingPage = useCallback(async (token?: string) => {
    try {
      // maintenance;
      // open;
      const maintenance = await on_get_status_company("maintenance");
      if (maintenance) {
        setErrorServer(true);
        return;
      }
      const open = await on_get_status_company("open");
      if (open === 0) {
        set_open_site(0);
        setLoading(1);
        return;
      }
      set_open_site(open);

      const instagram = await on_get_status_company("link_instagram");
      const linkedin = await on_get_status_company("link_linkedin");
      const facebook = await on_get_status_company("link_facebook");
      const twitter = await on_get_status_company("link_twitter");

      set_social(
        Object.assign({}, {
          instagram,
          linkedin,
          facebook,
          twitter,
        } as social_I)
      );

      if (token) {
        await on_verify_user_exist(token);
        const full_name = await on_get_one_field_user("full_name", token);
        const cpf = await on_get_one_field_user("cpf", token);
        const city = await on_get_city_user('city', token);
        const cep = await on_get_city_user('cep', token);
        const state = await on_get_city_user('state', token);
        // //
        // await on_get_amount_cart_sale(token);
        set_user_data({ cpf, full_name, city, cep, state });
      }
      setLoading(1);
    } catch (error: any) {
      console.log(error);
      setLoading(0);
      if (error.status === 401) return on_logout();
      if (error.status === 0) return setErrorServer(true);
      if (error.status === 500) return setErrorServer(true);
      return;
    }
  }, [
    on_get_status_company,
    on_verify_user_exist,
    on_get_one_field_user,
    on_get_city_user,
    set_user_data,
    on_logout
  ]);

  const [location_user_logout, set_location_user_logout] = useState<prop_location_user_logout | null>(null as prop_location_user_logout | null);

  useEffect(() => {
    const data = getCookie("SID-site-location");
    if (data) {
      const dat = JSON.parse(String(data));
      set_user_data({ cep: dat?.cep ?? null, city: dat?.city ?? null, state: dat?.state ?? null });
      set_location_user_logout({ cep: dat?.cep, city: dat?.city });
      set_city(dat ? `${dat?.state} - ${dat?.city}` : null);
    } else {
      set_user_data({ cep: null, city: null, state: null });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (auth_data !== 0 && auth_data?.token) {
      loadingPage(auth_data.token);
    } else {
      loadingPage();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth_data]);

  const valueData = useMemo(() => {
    return {
      city,
      set_city,
      header_Category,
      amount_cart_sale,
      setHeader_Category,
      open_modal_location,
      on_get_amount_cart_sale,
      set_open_modal_location,
      exist,
      social,
      set_exist,
      location_user_logout,
      set_load_click,
      load_click
    };
  }, [
    city,
    header_Category,
    amount_cart_sale,
    open_modal_location,
    on_get_amount_cart_sale,
    exist,
    social,
    location_user_logout,
    load_click
  ]);

  return (
    <TotalLoadingDataContext.Provider value={valueData}>
      {errorServer && (
        <DivLoading>
          <LogoComponent type="2" />
          <p style={{ marginTop: 20 }}>
            Estamos em <strong>manuntenção</strong>.
          </p>
          <small>Todas as entregas seguem normalmente.</small>
        </DivLoading>
      )}
      {open_site === 0 && (
        <DivLoading>
          <LogoComponent type="2" />
          <p style={{ marginTop: 20 }}>
            Fechada para <strong>manuntenção</strong>.
          </p>
          <small>Todas as entregas seguem normalmente.</small>
        </DivLoading>
      )}
      {loading === 0 && !errorServer && (
        <DivLoading>
          <LogoComponent type="2" />
        </DivLoading>
      )}
      {load_click && (
        <DivLoading>
          <LogoComponent type="2" />
        </DivLoading>
      )}
      {open_site === 1 && loading === 1 && <CookiesInfoComponent />}
      <LayoutComponent>{children}</LayoutComponent>
    </TotalLoadingDataContext.Provider>
  );
};
