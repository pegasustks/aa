import Head from "next/head";
import { useRouter } from "next/router";
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { ProviderAPI } from "services/api";
import { AuthContext } from "./AuthContext";
import { TotalLoadingDataContext } from "./TotalLoadingDataContextContext";

interface categories_prop {
  category: string;
  sub_category1?: string | null;
  sub_category2?: string | null;
  sub_category3?: string | null;
}

interface prop_feature {
  name: string;
  value: string;
}


interface data_request_prop {
  product_id: string;
  color_id?: string;
  amount: number;
  sizes_fashion_id?: string;
}

// TYPE PRODUTO

type feature_T = {
  name: string;
  value: string;
}

interface color_prop {
  id: string;
  title: string;
  value: string;
}


interface size_value {
  value: string;
  id: string;
  sizes_fashion_id: string;
}

interface sizes_prop {
  header: string[];
  body: string[][];
}

interface prices_prop {
  price_unit: string;
  price_unit2?: string;
  repository: number;
}

interface data_categories_prop {
  category: string;
  sub_category1: string;
  sub_category2?: string;
  sub_category3?: string;
}

interface frete_prop {
  service: string,
  descricao: string,
  price: {
    de: number;
    por: number;
  };
  referencia: string,
  prazoEnt: number,
  alertas: string[],
}

// END

export type first_info_product_T = {
  title: string;
  sales: number;
  id: string;
  resum?: string;
  types: string[];
  first_color_id: string;
  first_size_fashion_id: string;
  exist_in_cart_sale: boolean;
  data_categories: data_categories_prop;
  it?: string;
}

export type prop_product_T = {
  fashion_data: {
    features: feature_T[] | undefined;
    colors: color_prop[] | undefined;
    sizes_values: size_value[] | undefined;
    images: string[] | undefined;
    sizes: sizes_prop | undefined;
  };
  frete: frete_prop | undefined;
  price: prices_prop | undefined;
}

export type product_T = first_info_product_T & prop_product_T;

interface InfoProductProps_I {
  product: product_T | undefined;
  categories_of_product: categories_prop | undefined;
  data_features: prop_feature[] | undefined;
  colors: color_prop[] | undefined;
  sizes_values: size_value[] | undefined;
  sizes: sizes_prop | undefined;
  prices: prices_prop | undefined;
  data_request: data_request_prop | undefined;
  images: string[] | undefined;
  data_frete: frete_prop | undefined;

  colapse_feature: boolean;
  set_colapse_feature(e: boolean): void;
  colapse_measurements: boolean;
  set_colapse_measurements(e: boolean): void;
  handle_value_request(name: string, new_value: string | number): void;
  on_add_product_in_cart_sale(): Promise<void>;
  on_select_color_product(color_id: string): Promise<void>;
  load_change: boolean;
  load_change_size: boolean;
  on_select_size_product(size_value_id: string): Promise<void>;

  on_reload_frete_product(cep: string, quantidade?: string): Promise<void>;
}

export const InfoProductContext = createContext({} as InfoProductProps_I);

interface ProviderInfo_I {
  children: JSX.Element;
  product_data: product_T | undefined;
}

export const ProviderInfoProductContext: React.FC<ProviderInfo_I> = ({
  children,
  product_data,
}): JSX.Element => {
  const { auth_data, set_user_data, user_data } = useContext(AuthContext);
  const { on_get_amount_cart_sale, exist, location_user_logout, set_exist } = useContext(
    TotalLoadingDataContext
  );
  const { push } = useRouter();

  const [product, set_product] = useState<first_info_product_T | undefined>(product_data && {
    sales: product_data.sales,
    title: product_data.title,
    exist_in_cart_sale: product_data.exist_in_cart_sale,
    first_color_id: product_data.first_color_id,
    first_size_fashion_id: product_data.first_size_fashion_id,
    id: product_data.id,
    resum: product_data.resum,
    types: product_data.types,
    it: product_data.it
  } as first_info_product_T | undefined);

  const [images, set_images] = useState<string[] | undefined>(product_data && product_data.fashion_data.images as string[] | undefined);
  const [prices, set_prices] = useState<prices_prop | undefined>(product_data && product_data.price as prices_prop | undefined);
  const [colors, set_colors] = useState<color_prop[] | undefined>(product_data && product_data.fashion_data.colors as color_prop[] | undefined);
  const [sizes_values, set_sizes_values] = useState<size_value[] | undefined>(product_data && product_data.fashion_data.sizes_values as size_value[] | undefined);
  const [sizes, set_sizes] = useState<sizes_prop | undefined>(product_data && product_data.fashion_data.sizes as sizes_prop | undefined);
  const [data_features, set_data_features] = useState<prop_feature[] | undefined>(product_data && product_data.fashion_data.features as prop_feature[] | undefined);
  const [data_frete, set_data_frete] = useState<frete_prop | undefined>(product_data && product_data.frete as frete_prop | undefined);
  const [data_request, set_data_request] = useState<data_request_prop | undefined>(product_data && {
    amount: 1,
    product_id: product_data.id,
    color_id: product_data.first_color_id,
    sizes_fashion_id: product_data.first_size_fashion_id
  } as data_request_prop | undefined);
  const [categories_of_product] = useState<categories_prop>(product_data?.data_categories as categories_prop);

  useMemo(() => {
    if (product_data) set_exist(product_data.exist_in_cart_sale);
    return {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const [colapse_feature, set_colapse_feature] = useState<boolean>(false);
  const [colapse_measurements, set_colapse_measurements] =
    useState<boolean>(false);

  const on_get_images_of_color_product = useCallback(
    async (id: string, product_id: string) => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-8-images-on-color/${product_id}`
        );
        set_images(data.data);
      } catch (error: any) {
        return null;
      }
    },
    [set_images]
  );

  const on_get_prices_of_color_product = useCallback(
    async (id: string, color_id: string) => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-7-prices-color/${id}/${color_id}`
        );
        set_prices(data.data);
      } catch (error: any) {
        return null;
      }
    },
    []
  );

  const on_get_sizes_of_product = useCallback(
    async (product_id: string, color_id: string) => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-5-sizes/${product_id}/${color_id}`
        );
        set_sizes(data.data);
      } catch (error: any) {
        return null;
      }
    },
    []
  );

  const on_get_sizes_values_of_product = useCallback(
    async (
      id: string
    ): Promise<{
      size_value_id?: string;
      sizes_fashion_id?: string;
    }> => {
      try {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-4-sizes-values/${id}`
        );
        set_sizes_values(
          data.data.map((e: any, i: number) => {
            return {
              value: e.size_value,
              sizes_fashion_id: e.id,
            };
          })
        );
        return {
          size_value_id: data.data[0].id,
          sizes_fashion_id: data.data[0].id,
        };
      } catch (error: any) {
        return {
          size_value_id: undefined,
          sizes_fashion_id: undefined,
        };
      }
    },
    []
  );

  const on_reload_frete_product = useCallback(async (cep: string, quantidade?: string) => {
    const auth = auth_data !== 0 ?
      auth_data?.token ? {
        headers: { authorization: `BEARER ${auth_data?.token}` },
      } : undefined : undefined;

    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/product-2-step-9-prices-frete/${product_data!.id}/${product_data!.first_size_fashion_id}/${quantidade ?? 1}?cep_destination=${cep}`, auth
      ).then((e): any => e).catch(() => undefined);

      set_data_frete({
        service: data.data.service,
        descricao: data.data.response.descricao,
        price: data.data.response.vlrFrete,
        referencia: data.data.response.referencia,
        prazoEnt: data.data.response.prazoEnt,
        alertas: data.data.response.alertas,
      });
      set_user_data({ ...user_data, cep });
    } catch (error) {
      set_data_frete(undefined);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth_data]);

  const handle_value_request = useCallback(
    (name: string, new_value: string | number): void => {
      set_data_request({ ...data_request!, [name]: new_value });
    },
    [data_request]
  );

  const on_add_product_in_cart_sale = useCallback(async () => {
    try {
      const auth = auth_data !== 0 ? auth_data : undefined;

      await ProviderAPI.post(
        "/v1/user/create/cart-sale",
        {
          product2_id: data_request?.product_id,
          user_id: auth?.id,
          amount: data_request?.amount,
          color_product_id: data_request?.color_id,
          sizes_fashion_id: data_request?.sizes_fashion_id,
          influencer: product?.it,
        },
        {
          headers: {
            authorization: `BEARER ${auth?.token}`,
          },
        }
      );
      if (auth?.token) {
        await on_get_amount_cart_sale(auth.token ?? undefined);
      }
      set_exist(true);
    } catch (error: any) {
      if (error?.response?.status === 401)
        push(
          `/account/login?p=${data_request?.product_id}${data_request?.amount ? `&a=${data_request?.amount}` : ""
          }${data_request?.color_id ? `&c=${data_request?.color_id}` : ""}${data_request?.sizes_fashion_id
            ? `&s=${data_request?.sizes_fashion_id}`
            : ""
          }`
        );
    }
  }, [
    auth_data,
    data_request,
    product?.it,
    on_get_amount_cart_sale,
    push,
    set_exist,
  ]);
  // AÇÕES DE USUARIO SOBRE O PRODUTO

  const [load_change, set_load_change] = useState<boolean>(false);
  const on_select_color_product = useCallback(
    async (color_id: string) => {
      await set_load_change(true);
      const { sizes_fashion_id } = await on_get_sizes_values_of_product(
        color_id
      );
      set_data_request({
        ...data_request!,
        color_id,
        sizes_fashion_id,
      });
      await on_get_images_of_color_product(sizes_fashion_id ?? "", color_id);
      await on_get_sizes_of_product(data_request!.product_id, color_id);
      await on_get_prices_of_color_product(sizes_fashion_id ?? "", color_id);
      await set_load_change(false);
    },
    [
      data_request,
      on_get_images_of_color_product,
      on_get_prices_of_color_product,
      on_get_sizes_of_product,
      on_get_sizes_values_of_product,
    ]
  );

  const [load_change_size, set_load_change_size] = useState<boolean>(false);
  const on_select_size_product = useCallback(
    async (sizes_fashion_id: string) => {
      await set_load_change_size(true);
      await on_get_prices_of_color_product(
        sizes_fashion_id,
        data_request?.color_id ?? ""
      );
      set_data_request({
        ...data_request!,
        sizes_fashion_id,
      });
      await set_load_change_size(false);
    },
    [data_request, on_get_prices_of_color_product]
  );

  const dataValue = useMemo(() => {
    const obj_p: product_T | undefined = product && {
      ...product,
      fashion_data: {
        colors,
        features: data_features,
        images,
        sizes,
        sizes_values,
      },
      frete: data_frete,
      price: prices,
    }

    return {
      sizes,
      colors,
      prices,
      images,
      product: obj_p,
      load_change,
      sizes_values,
      data_request,
      data_features,
      colapse_feature,
      load_change_size,
      colapse_measurements,
      categories_of_product,
      set_colapse_feature,
      handle_value_request,
      on_select_size_product,
      on_select_color_product,
      set_colapse_measurements,
      on_add_product_in_cart_sale,
      data_frete,
      on_reload_frete_product
    };
  }, [
    sizes,
    colors,
    prices,
    images,
    load_change,
    sizes_values,
    data_request,
    data_features,
    colapse_feature,
    load_change_size,
    colapse_measurements,
    categories_of_product,
    set_colapse_feature,
    handle_value_request,
    on_select_size_product,
    on_select_color_product,
    set_colapse_measurements,
    on_add_product_in_cart_sale,
    product,
    data_frete,
    on_reload_frete_product
  ]);

  return (
    <>
      <Head>
        <title>{product_data?.title ?? "Não encontrado :("}</title>
      </Head>
      <InfoProductContext.Provider value={dataValue}>
        {children}
      </InfoProductContext.Provider>
    </>
  );
};
