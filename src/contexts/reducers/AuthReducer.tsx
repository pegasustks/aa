interface AuthProps {
  user_data?: user_prop;
}

interface user_prop {
  full_name?: string;
  cpf?: string;
  city?: string;
  cep?: string;
}

type AuthActions = {
  type: 'SET_USER';
  data: number;
};

type AuthD = AuthProps;

export const AuthDefaultValues: AuthD = {
  user_data: {},
};

export const AuthReducer = (
  state: AuthProps,
  action: AuthActions
): AuthProps => {
  const newState = { ...state };

  switch (action.type) {
    case 'SET_USER':
      break;

    default:
      break;
  }

  return newState;
};
