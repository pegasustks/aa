import axios from 'axios';

export const ProviderAPI = axios.create({
  baseURL: 'https://c68b-177-128-192-93.ngrok-free.app',
  // baseURL: 'https://1e67-177-128-192-93.ngrok-free.app',
});
