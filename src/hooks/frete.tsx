import { AuthContext } from "contexts/AuthContext";
import { memo, useCallback, useContext, useMemo, useState } from "react";
import { ProviderAPI } from '../services/api';

type prop_frete_T = {
  quantidade: number;
  product_id: string;
  cepDest?: string;
}

type return_data_use_T = {
  getPriceFrete: ({ quantidade, ...prop }: prop_frete_T) => Promise<any>
}

export const useFrete = (): return_data_use_T => {
  const { auth_data } = useContext(AuthContext);

  const memory = useCallback(async ({ quantidade = 1, ...prop }: prop_frete_T) => {
    const url = `/v1/public/get/product-2-step-9-prices-frete/${prop.product_id}/${quantidade}?cep_destination=${prop.cepDest}`;
    const { data } = await ProviderAPI.get(url, {
      headers: {
        authorization: `BEARER ${auth_data?.token}`,
      },
    });
    return data?.data;
  }, [auth_data?.token]);

  return { getPriceFrete: memory };
}