import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { InfoProductComponent } from "@components/PageComponents/InfoProduct";
import { CEP } from "cep-promise";
import { AuthContext, auth_prop } from "contexts/AuthContext";
import { product_T, ProviderInfoProductContext } from "contexts/InfoProductContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

interface prop_server_side_T {
  data_product: product_T | undefined;
  data_user: auth_prop | 0;
}

export default function ProductPage({ data_product, data_user }: prop_server_side_T): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);

  useEffect(() => {
    set_auth_data(data_user);
  }, [data_user, set_auth_data]);

  return (
    <ProviderInfoProductContext product_data={data_product}>
      <MaxContainerComponent>
        <InfoProductComponent />
      </MaxContainerComponent>
    </ProviderInfoProductContext>
  );
}

export async function getServerSideProps(context: any): Promise<any> {
  const auth = getCookie("auth", { req: context.req, res: context.res });

  const product_id: string = context.params?.produto;
  const it: string | null = context.query?.it ?? null;
  const auth_token: string | undefined = context.req.cookies?.auth;
  const cep_cookie: string | undefined = context.req.cookies['SID-site-location'];
  const json_cep: undefined | CEP = cep_cookie && JSON.parse(cep_cookie);

  try {
    const dataAuth = await ProviderAPI.get(`/v1/verify-token/${auth}/2`)
      .catch(() => { return undefined })
      .then((e): any => { return e });

    const prop_product = {};

    const { data: response_product } = await ProviderAPI.get(`/v1/public/get/product2/${product_id}`)
      .catch((e) => {
        return false;
      })
      .then((e): any => { return e });

    if (!response_product) {
      return {
        props: {
          data_product: null,
          data_user: dataAuth ? {
            id: dataAuth.data.data.idUser,
            token: auth_token
          } : 0,
        }
      }
    }

    const category_id = response_product.data.categories.category_id;
    const sub_category_id = response_product.data.categories.sub_category_id;
    const sub_category2_id = response_product.data.categories?.sub_category2_id;
    const sub_category3_id = response_product.data.categories?.sub_category3_id;

    const { data: response_categories } = await ProviderAPI.get(`/v1/public/get/categories-of-product2/${category_id}/${sub_category_id}${sub_category2_id ? `?sub_category2_id=${sub_category2_id}` : ''}${sub_category3_id ? `?sub_category3_id=${sub_category3_id}` : ''}`);

    Object.assign(prop_product, {
      it,
      title: response_product.data.title,
      sales: response_product.data.sales,
      id: product_id,
      resum: response_product.data.resum,
      types: response_product.data.types,
      data_categories: {
        category: response_categories.data.category.name,
        sub_category1: response_categories.data.sub_category.name,
        sub_category2: response_categories.data.sub_category2?.name ?? null,
        sub_category3: response_categories.data.sub_category3?.name ?? null,
      }
    });

    // USUARIO LOGADO
    if (auth_token) {
      const { data: response_exist_in_cart_sale } = await ProviderAPI.get(`/v1/user/get/this-exist-product-cart-sale/${product_id}`,
        {
          headers: {
            authorization: `BEARER ${auth_token}`,
          },
        }
      );
      Object.assign(prop_product, {
        exist_in_cart_sale: response_exist_in_cart_sale.data,
      });
    }

    if (response_product.data.types.some((e: string) => /(moda|roupa)s?/i.test(e))) {
      const [
        { data: response_step_2_fashion_features },
        { data: response_step_3_colors }
      ] = await Promise.all(
        [
          await ProviderAPI.get(`/v1/public/get/product2-step-2-fashion-features/${product_id}`),
          await ProviderAPI.get(`/v1/public/get/product2-step-3-colors/${product_id}`)
        ]
      );

      const first_color_id = response_step_3_colors.data[0].id

      Object.assign(prop_product, {
        first_color_id,
      })

      const { data: response_step_4_sizes_values } = await ProviderAPI.get(
        `/v1/public/get/product-2-step-4-sizes-values/${first_color_id}`
      );

      const first_size_fashion_id = response_step_4_sizes_values.data[0].id;

      Object.assign(prop_product, {
        first_size_fashion_id,
      })

      const { data: response_set_8_images_of_color } = await ProviderAPI.get(
        `/v1/public/get/product-2-step-8-images-on-color/${first_color_id}`
      );

      const { data: response_step_5_sizes } = await ProviderAPI.get(
        `/v1/public/get/product-2-step-5-sizes/${product_id}/${first_color_id}`
      );

      Object.assign(prop_product, {
        fashion_data: {
          features: response_step_2_fashion_features.data,
          colors: response_step_3_colors.data,
          sizes_values: response_step_4_sizes_values.data
            .map((e: any) => {
              return {
                value: e.size_value,
                sizes_fashion_id: e.id,
              };
            }),
          images: response_set_8_images_of_color.data,
          sizes: response_step_5_sizes.data,
        },
      });

      const { data: response_step_7_price_color } = await ProviderAPI.get(
        `/v1/public/get/product-2-step-7-prices-color/${first_size_fashion_id}/${first_color_id}`
      );

      Object.assign(prop_product, {
        price: response_step_7_price_color.data
      });

      if (json_cep?.cep) {
        const { data: response_step_9_price_frete } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-9-prices-frete/${product_id}/${first_size_fashion_id}/1?cep_destination=${json_cep?.cep}`,
          {
            headers: {
              authorization: `BEARER ${auth_token}`,
            },
          }
        ).then((e): any => e).catch((err) => undefined);
        Object.assign(prop_product, {
          frete: response_step_9_price_frete?.data && {
            service: response_step_9_price_frete.data.service,
            descricao: response_step_9_price_frete.data.response.descricao,
            price: response_step_9_price_frete.data.response.vlrFrete,
            referencia: response_step_9_price_frete.data.response.referencia,
            prazoEnt: response_step_9_price_frete.data.response.prazoEnt,
            alertas: response_step_9_price_frete.data.response.alertas,
          }
        });
      }
    }

    return {
      props: {
        data_product: prop_product ?? null,
        data_user: dataAuth ? {
          id: dataAuth.data.data.idUser,
          token: auth_token
        } : 0,
      }
    }
  } catch (error) {
    deleteCookie("auth");
    return {
      props: {}
    };
  }
}
