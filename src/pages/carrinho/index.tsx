import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { BagComponent } from "@components/PageComponents/Bag";
import { AuthContext } from "contexts/AuthContext";
import { cart_prop, ProviderCartSaleContext } from "contexts/CartSaleContext";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

interface prop_serv_I {
  data_user: any;
  data_cart: cart_prop[];
}

export default function CartPage({ data_user, data_cart }: prop_serv_I): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);
  const { set_load_click } = useContext(TotalLoadingDataContext);

  useEffect(() => {
    set_load_click(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    set_auth_data(data_user);
  }, [set_auth_data, data_user]);

  return (
    <ProviderCartSaleContext data_cart_server={data_cart}>
      <MaxContainerComponent>
        <BagComponent />
      </MaxContainerComponent>
    </ProviderCartSaleContext>
  );
}

export async function getServerSideProps(context: any): Promise<any> {
  const auth = getCookie("auth", { req: context.req, res: context.res });

  if (!auth) {
    return {
      redirect: { destination: "/account/login", permanent: true },
    };
  }

  const objt_auth = auth ? { headers: { authorization: `BEARER ${auth}` } } : undefined;

  try {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);

    const { data: response_exist_address_user } = await ProviderAPI.get(`/v1/user/get/address-user`, objt_auth);
    const exist_address_user = !!response_exist_address_user.data;

    if (!exist_address_user) {
      return {
        redirect: {
          destination: "/account/user?address=1",
          permanent: true,
        },
      }
    }

    const { data: response_cart_sale } = await ProviderAPI.get("/v1/user/get/cart-sale", objt_auth);

    const next_state = await Promise.all(response_cart_sale.data.map(async (e: cart_prop) => {
      try {
        const { data: response_step_9_price_frete } = await ProviderAPI.get(
          `/v1/public/get/product-2-step-9-prices-frete/${e.product_id}/${e.color.size?.sizes_fashion_id}/${e.amount}`, objt_auth
        );
        e.frete = response_step_9_price_frete.data.response.vlrFrete
        if (e?.color?.id) {
          const { data: response_step_4_sizes_value } = await ProviderAPI.get(`/v1/public/get/product-2-step-4-sizes-values/${e.color.id}`);
          const { data: response_step_3_colors } = await ProviderAPI.get(`/v1/public/get/product2-step-3-colors/${e.product_id}`);

          const data_sizes_values = response_step_4_sizes_value.data.map((e: any) => {
            return {
              value: e.size_value,
              sizes_fashion_id: e.id,
            };
          });
          e.main_image = response_step_3_colors.data[0].images.main;
          const data_colors = response_step_3_colors.data.map((e: any) => {
            return {
              id: e.id,
              color_value: e.value,
              color_name: e.title,
              main_image: e.images.main,
            };
          });
          e.options = {
            colors: data_colors.filter((es: any) => es.id !== e.color.id),
            sizes: data_sizes_values.filter(
              (es: any) =>
                es.sizes_fashion_id !== e.color?.size?.sizes_fashion_id
            ),
          };
          return e;
        }
        return e;
      } catch (error) {
        console.log(error);
      }
    }));

    return {
      props: {
        data_user: {
          token: data.data.token,
          id: data.data.idUser,
        },
        data_cart: next_state
      },
    };
  } catch (error: any) {
    if (error?.response?.status === 401) {
      deleteCookie("auth");
      return {
        redirect: {
          destination: "/account/login",
          permanent: true,
        },
      };
    }
    return {
      redirect: {
        destination: "/account/login",
        permanent: true,
      },
    };
  }
}
