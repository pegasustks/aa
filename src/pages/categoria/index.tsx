import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { SubCategoryComponent } from "@components/PageComponents/SubCategory";
import { AuthContext } from "contexts/AuthContext";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useState } from "react";
import { ProviderAPI } from "services/api";
import styled from "styled-components";

interface Names_I {
  category: string | null;
  subcategory: string | null;
  subcategory2?: string | null;
  subcategory3?: string | null;
}

export const Container = styled.div`
  width: 100%;
  height: calc(100vh - 312.99px);
`;

export interface product_I {
  id: string;
  main_image: string;
  title: string;
  main_price1: string;
  main_price2: string | null;
  city?: string;
  sales: number | null;
}

export default function SubCategoryPage({ data_user }: any): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);
  const { query } = useRouter();

  const [names, setNames] = useState<Names_I | null>({} as Names_I | null);
  const [loadPage, setLoadPage] = useState(false);
  const [products, setProducts] = useState<product_I[]>([] as product_I[]);

  const getNamesCategories = useCallback(async () => {
    try {
      await setLoadPage(true);

      const next_values = {
        category: null,
        subcategory: null,
        subcategory2: null,
        subcategory3: null,
      };

      if (query.category) {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/category-name/category/${query.category}`
        );
        next_values.category = data.data;
      }
      if (query.subcategory) {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/category-name/sub_category/${query.subcategory}`
        );
        next_values.subcategory = data.data;
      }
      if (query.subcategory2) {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/category-name/sub_category2/${query.subcategory2}`
        );
        next_values.subcategory2 = data.data;
      }
      if (query.subcategory3) {
        const { data } = await ProviderAPI.get(
          `/v1/public/get/category-name/sub_category3/${query.subcategory3}`
        );
        next_values.subcategory3 = data.data;
      }

      const dataJ = JSON.parse(
        String(window.localStorage.getItem("Lariizana-site-location"))
      );

      const { data } = await ProviderAPI.get(
        `/v1/public/get/products2/0/cat?category=${query.category
        }&subcategory=${query.subcategory}${query?.subcategory2 ? `&subcategory2=${query.subcategory2}` : ""
        }${query?.subcategory3 ? `&subcategory3=${query.subcategory3}` : ""}${dataJ?.city ? `&city=${dataJ?.city}` : ""
        }`
      );

      await setNames(next_values);
      await setProducts(data.data);
      await setLoadPage(false);
    } catch (error) {
      setLoadPage(false);
    }
  }, [query]);

  useEffect(() => {
    if (getNamesCategories) getNamesCategories();
  }, [getNamesCategories]);

  useEffect(() => {
    set_auth_data(data_user);
  }, [data_user, set_auth_data]);

  return (
    <MaxContainerComponent>
      <SubCategoryComponent products={products} trail={names} />
    </MaxContainerComponent>
  );
}

export async function getServerSideProps({ req, res }: any): Promise<any> {
  const auth = getCookie("auth", { req, res });

  if (!auth) return { props: {} };
  try {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    return {
      props: {
        data_user: {
          token: data.data.token,
          id: data.data.idUser,
        },
      },
    };
  } catch (error) {
    deleteCookie("auth");
    return {
      props: {
        data_user: 0
      }
    };
  }
}
