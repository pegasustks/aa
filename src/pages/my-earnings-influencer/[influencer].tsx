import { InfluencerPage } from "@components/PageComponents/Influencer";

export default function IndexInfluencerPage(): JSX.Element {
  return <InfluencerPage />;
}