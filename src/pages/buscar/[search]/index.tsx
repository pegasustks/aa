import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { SearchComponent } from "@components/PageComponents/Search";
import { AuthContext } from "contexts/AuthContext";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

export default function SearchPage({ data_user }: any): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);
  const { set_load_click } = useContext(TotalLoadingDataContext);

  useEffect(() => {
    set_load_click(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    set_auth_data(data_user);
  }, [data_user, set_auth_data]);

  return (
    <MaxContainerComponent>
      <SearchComponent />
    </MaxContainerComponent>
  );
}

export async function getServerSideProps({ req, res }: any): Promise<any> {
  const auth = getCookie("auth", { req, res });
  if (!auth) return { props: {} };

  try {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    return {
      props: {
        data_user: {
          token: data.data.token,
          id: data.data.idUser,
        },
      },
    };
  } catch (error) {
    deleteCookie("auth");
    return {
      props: {
        data_user: 0
      }
    };
  }
}
