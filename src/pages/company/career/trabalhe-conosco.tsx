import { LayoutComponent } from '@components/Global/Layout';
import { MaxContainerComponent } from '@components/Global/MaxContainer';
import { WorkWithUsComponent } from '@components/PageComponents/WorkWithUs';

export default function WorkWithUsPage(): JSX.Element {
  return (
    <LayoutComponent>
      <MaxContainerComponent>
        <WorkWithUsComponent />
      </MaxContainerComponent>
    </LayoutComponent>
  );
}
