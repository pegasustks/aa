import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { TermUseComponent } from "@components/PageComponents/TermUse";
import { AuthContext } from "contexts/AuthContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

export default function TermUsePage({ data_user }: any): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);

  useEffect(() => {
    set_auth_data(data_user);
  }, [data_user, set_auth_data]);

  return (
    <MaxContainerComponent>
      <TermUseComponent />
    </MaxContainerComponent>
  );
}

export async function getServerSideProps({ req, res }: any): Promise<any> {
  const auth = getCookie("auth", { req, res });

  if (!auth) return { props: {} };
  try {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    return {
      props: {
        data_user: {
          token: data.data.token,
          id: data.data.idUser,
        },
      },
    };
  } catch (error) {
    deleteCookie("auth");
    return {
      props: {
        data_user: 0
      }
    };
  }
}
