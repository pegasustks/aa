import { LoginComponent } from "@components/PageComponents/Login";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie, deleteCookie } from "cookies-next";
import Head from "next/head";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

export default function LoginPage(): JSX.Element {
  const { set_load_click } = useContext(TotalLoadingDataContext);

  useEffect(() => {
    set_load_click(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Head>
        <title>Acessar conta</title>
      </Head>
      <LoginComponent />
    </>
  );
}

export async function getServerSideProps({ req, res }: any): Promise<any> {
  const auth = getCookie("auth", { req, res });

  if (!auth) return { props: {} };

  try {
    await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    return {
      redirect: {
        destination: "/",
        permanent: true,
      },
    };
  } catch (error) {
    deleteCookie("auth");
    return {
      props: {
        data_user: undefined
      }
    };
  }
}
