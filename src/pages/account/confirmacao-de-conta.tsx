import { ConfirmOfAccountComponent } from "@components/PageComponents/ConfirmAccount";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie } from "cookies-next";
import { Jwt, verify } from "jsonwebtoken";
import Head from "next/head";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

export default function ConfirmOfAccountPage({ whatsapp }: any): JSX.Element {
  const { set_load_click } = useContext(TotalLoadingDataContext);

  useEffect(() => {
    set_load_click(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Head>
        <title>Confirmar conta</title>
      </Head>
      <ConfirmOfAccountComponent whatsapp={whatsapp} />
    </>
  );
}

export async function getServerSideProps({
  req,
  res,
  query,
}: any): Promise<any> {
  const auth = getCookie("auth", { req, res });

  let whatsapp: string | null = null;

  if (!query.e) {
    return {
      redirect: {
        destination: "/account/login",
        permanent: true,
      },
    };
  }

  try {
    const decoded: any = verify(query.e, "whatsapp_Lariizana");
    whatsapp = decoded.e;
  } catch (error) {
    console.log(error);
    return {
      redirect: {
        destination: "/account/login",
        permanent: true,
      },
    };
  }

  if (auth) {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    if (data.data?.token) {
      return {
        redirect: {
          destination: "/",
          permanent: true,
        },
      };
    }
  }

  return {
    props: { whatsapp },
  };
}
