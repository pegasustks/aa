import { ThemeProvider } from "styled-components";
import GlobalStyles from "@styles/global";
import { theme } from "@styles/theme/primary";
import { ProviderTotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { ProviderAuthContext } from "contexts/AuthContext";
import "react-multi-carousel/lib/styles.css";
import { ProviderListenSize } from "contexts/ListenSize";
import Head from "next/head";
import NextNprogress from "nextjs-progressbar";

export default function MyApp({ Component, pageProps }: any): JSX.Element {
  return (
    <>
      <Head>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <title>Lariizana</title>
      </Head>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <NextNprogress options={{
          showSpinner: false,
          trickle: false,
        }} color="#e2380e" />
        <ProviderAuthContext>
          <ProviderListenSize>
            <ProviderTotalLoadingDataContext>

              <Component {...pageProps} />
            </ProviderTotalLoadingDataContext>
          </ProviderListenSize>
        </ProviderAuthContext>
      </ThemeProvider>
    </>
  );
}
