import { PayComponent } from "@components/PageComponents/Pay";
import { AuthContext } from "contexts/AuthContext";
import { ProviderPayAddressContext } from "contexts/PayAddressContext";
import { ProviderPayContext } from "contexts/PayContext";
import { getCookie, deleteCookie } from "cookies-next";
import { useContext, useEffect } from "react";
import { ProviderAPI } from "services/api";

export default function PayPage({ data_user }: any): JSX.Element {
  const { set_auth_data } = useContext(AuthContext);

  useEffect(() => {
    set_auth_data(data_user ?? { id: null, token: null });
  }, [set_auth_data, data_user]);

  return (
    <ProviderPayContext>
      <ProviderPayAddressContext data_user={data_user}>
        <PayComponent />
      </ProviderPayAddressContext>
    </ProviderPayContext>
  );
}

export async function getServerSideProps({ req, res }: any): Promise<any> {
  const auth = getCookie("auth", { req, res });

  if (!auth) {
    return {
      redirect: { destination: "/account/login", permanent: true },
    };
  }

  try {
    const { data } = await ProviderAPI.get(`/v1/verify-token/${auth}/2`);
    return {
      props: {
        data_user: {
          token: data.data.token,
          id: data.data.idUser,
        },
      },
    };
  } catch (error) {
    deleteCookie("auth");
    return {
      redirect: {
        destination: "/account/login",
        permanent: true,
      },
    };
  }
}
