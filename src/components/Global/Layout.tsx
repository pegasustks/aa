import { FooterComponent } from "@components/Footer";
import { HeaderComponent } from "@components/Header";
import { ProviderCategoriesContext } from "contexts/CategoriesContext";
import { ProviderHeaderContext } from "contexts/HeaderContext";
import { ListenSizeContext } from "contexts/ListenSize";
import { useContext, useEffect } from "react";
import { useResizeDetector } from "react-resize-detector";
import { SuperContainer } from "./styles";

interface LayoutProps {
  children: JSX.Element;
}

export const LayoutComponent: React.FC<LayoutProps> = ({
  children,
}): JSX.Element => {
  const { setSize } = useContext(ListenSizeContext);
  const { width, ref } = useResizeDetector();

  useEffect(() => {
    setSize(width ?? 0);
  }, [width, setSize]);

  return (
    <>
      <ProviderCategoriesContext>
        <ProviderHeaderContext>
          <HeaderComponent />
        </ProviderHeaderContext>
      </ProviderCategoriesContext>
      <SuperContainer ref={ref}>{children}</SuperContainer>
      <FooterComponent />
    </>
  );
};
