import styled from "styled-components";

export const SuperContainer = styled.div`
  width: 100%;
  min-height: calc(100vh - 195px);
`;

export const GridProducts = styled.div<{ n?: boolean }>`
  display: grid;
  grid-column-gap: 7px;
  grid-row-gap: 7px;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;

  #prod {
    max-width: 100%;
  }

  @media screen and (max-width: 998px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }

  @media screen and (max-width: 880px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }

  @media screen and (max-width: 640px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media screen and (max-width: 490px) {
    grid-template-columns: 1fr 1fr;
  }

  @media screen and (max-width: 490px) {
    grid-template-columns: 1fr 1fr;
  }
`;
