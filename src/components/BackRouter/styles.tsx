import styled from 'styled-components';

export const WapperInfoRoute = styled.div`
  display: flex;
  align-items: start;

  color: #000;

  #back_to {
    cursor: pointer;
    :hover {
      background: #f5f5f5;
    }
    transition: 0.3s ease;
    padding: 8px 20px;
    color: #000;
    font-family: ${({ theme }) => theme.font.primary};
    font-size: 14px;
  }
`;

export const ValueInfoRoute = styled.p`
  a {
    color: #696969;
    font-weight: 400;
    margin: 0 5px;
    font-size: 14px;

    @media screen and (max-width: 555px) {
      font-size: 12px;
    }

    :first-child {
      margin-left: 0;
    }
    :last-child {
      @media screen and (max-width: 555px) {
        font-size: 14px;
      }
      font-size: 16px;
      margin-right: 0;
      font-weight: 500;
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`;
