/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { useRouter } from 'next/router';
import { ValueInfoRoute, WapperInfoRoute } from './styles';

interface BackRouterProps {
  value?: Array<{
    value?: string | null;
    id?: string;
  }>;
}

export const BackRouterComponent: React.FC<BackRouterProps> = ({
  value,
}): JSX.Element => {
  const { back } = useRouter();

  return (
    <WapperInfoRoute>
      <ValueInfoRoute>
        {value?.map(
          (item, index) =>
            item.value && (
              <a key={item?.id}>
                {item?.value}
                {value.length - 1 === index ? '' : ' /'}
              </a>
            )
        )}
      </ValueInfoRoute>
    </WapperInfoRoute>
  );
};
