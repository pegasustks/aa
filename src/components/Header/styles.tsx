import styled from "styled-components";

export const DivContainer = styled.div`
  position: sticky;
  top: 0;
  z-index: 99;
  height: 137px;
  background: ${({ theme }) => theme.background.primary};
  box-shadow: ${({ theme }) => theme["box-shadow"].primary};


  @media screen and (max-width: 510px) {
    height: auto;
  }
`;

export const Header = styled.header`
  padding: 5px 0 10px;
  display: flex;
  flex-direction: column;
  row-gap: 10px;
`;

export const DivTop = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  column-gap: 10px;
  row-gap: 5px;
`;

export const DivInfoCompany = styled.div`
  display: flex;
  column-gap: 10px;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.white} !important;
  align-items: center;
  font-size: 14px;
  span {
  cursor: default;
  }
  a {
    color: ${({ theme }) => theme.colors.white} !important;
    :hover {
      color: ${({ theme }) => theme.colors.hover.white} !important;
    }
  }
`;

export const DivItemIC = styled.div`
  display: flex;
  column-gap: 10px;
  align-items: center;
`;

export const DivItensRedes = styled.div`
  column-gap: 7px;
  align-items: center;
  display: flex;
`;

export const DivAccoutUser = styled.div`
  display: flex;
  align-items: center;
  column-gap: 10px;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.white} !important;
  font-size: 14px;
  a {
    color: ${({ theme }) => theme.colors.white} !important;
    :hover {
      color: ${({ theme }) => theme.colors.hover.white} !important;
    }
  }
`;


// main

export const DivMain = styled.div`
  display: grid;
  grid-template-columns: minmax(50px,67px) minmax(180px, 1fr) minmax(50px,60px);
  column-gap: 20px;

  @media screen and (max-width: 575px) {
    column-gap: 10px;
  }

  @media screen and (max-width: 405px) {
    grid-template-columns: minmax(180px, 1fr) minmax(50px,60px);
    #logo_ {
      display: none;
    }
  }
`;

export const StepSearchHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: 12px;

  @media screen and (max-width: 510px) {
    justify-content: space-between;
  }
`;

export const FormSearch = styled.form``;

export const LabelInput = styled.label``;

export const DivInput = styled.div`
  background-color: ${({ theme }) => theme.background.white};
  height: 58px;
  display: flex;
  padding: 7px;
`;

export const InputSearch = styled.input`
  height: 100%;
  border: 0;
  outline: none;
  padding-left: 10px;
  font-size: 16px;
  width: 100%;

  ::-webkit-search-cancel-button {
    opacity: 0.5;
    cursor: pointer;
  }

  ::-moz-search-cancel {
    opacity: 0.5;
    cursor: pointer;
  }

  ::-ms-clear {
    display: none; /* Oculta o botão de limpar padrão do IE */
  }
`;

export const ButtonSearch = styled.button`
  width: 68px;
  height: 44px;
  background: ${({ theme }) => theme.background.buttons.primary};
  border-radius: 4px;
  border: 0;
  cursor: pointer;
  
  :hover {
    background: ${({ theme }) => theme.background.buttons.hover.primary};
  }
`;

export const NavPreSearch = styled.nav`
  @media screen and (max-width: 510px) {
    display: none;
  }
`;

export const ListPS = styled.ul`
  column-gap: 10px;
  align-items: center;
  display: flex;
  list-style: none;
`;

export const ItemPS = styled.li`
  font-weight: 300;
  a {
    color: ${({ theme }) => theme.colors.white} !important;
    :hover {
      color: ${({ theme }) => theme.colors.hover.white} !important;
    }
  }
`;

export const DivCartSale = styled.button`
  border: 0;
  outline: none;
  background: transparent;
  width: 100%;
  height: 100%;
  display: grid;
  place-items: center;
`;

export const DivIconCart = styled.div`
  position: relative;
`;

export const Amount = styled.small`
  position: absolute;
  bottom: -3px;
  right: -5px;
  min-width: 16px;
  height: 22px;
  padding: 0 4px;
  color: ${({ theme }) => theme.colors.primary};
  font-weight: 600;
  font-size: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 3px solid ${({ theme }) => theme.colors.primary};
  background: ${({ theme }) => theme.background.white};
`;

export const DivLocation = styled.div`
  #location_b {
    cursor: pointer;
    border: 0;
    background: transparent;
    outline: none;
    align-items: center;
    display: flex;
    font-size: 14px;
    column-gap: 5px;
  }

  position: relative;
  width: 100%;
  padding: 4px;
    
  span {
    text-align: start;
    color: ${({ theme }) => theme.colors.white};
  }
`;
