import { useContext, useEffect, useState } from "react";
import { HeaderContext } from "contexts/HeaderContext";
import {
  Amount,
  ButtonSearch,
  DivAccoutUser,
  DivCartSale,
  DivContainer,
  DivIconCart,
  DivInfoCompany,
  DivInput,
  DivItemIC,
  DivItensRedes,
  DivLocation,
  DivMain,
  DivTop,
  FormSearch,
  Header,
  InputSearch,
  ItemPS,
  LabelInput,
  ListPS,
  NavPreSearch,
  StepSearchHeader
} from "./styles";
import { MaxContainerComponent } from "@components/Global/MaxContainer";
import Link from "next/link";
import { FiTwitter } from "react-icons/fi";
import { BsInstagram } from "react-icons/bs";
import { RiFacebookBoxLine } from "react-icons/ri";
import { LogoComponent } from "@components/Logo";
import { HiOutlineSearch } from 'react-icons/hi'
import { MdOutlineLocationOn, MdOutlineShoppingBag } from "react-icons/md";
import { AuthContext } from "contexts/AuthContext";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { useRouter } from "next/router";
import { useTheme } from "styled-components";

export const HeaderComponent: React.FC = (): JSX.Element => {
  const { open, setOpen } = useContext(HeaderContext);
  const { user_data, on_logout, auth_data } = useContext(AuthContext);
  const [open_modal_location, set_open_modal_location] = useState<boolean>(false as boolean);
  const { on_get_amount_cart_sale, social, amount_cart_sale, set_load_click } = useContext(TotalLoadingDataContext);
  const { push } = useRouter();
  const { colors } = useTheme();

  const { asPath } = useRouter();

  const [value_search, set_value_search] = useState<string>('' as string);

  useEffect(() => {
    if (auth_data !== 0 && auth_data?.token) {
      on_get_amount_cart_sale(auth_data.token);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth_data]);

  useEffect(() => {
    if (user_data?.cep === null) {
      set_open_modal_location(true);
    }
  }, [user_data?.cep]);

  console.log(asPath);

  return (
    <DivContainer>
      <MaxContainerComponent>
        <Header>
          <DivTop>
            <DivInfoCompany>
              <DivItemIC>
                <a href="https://wa.me/5571986751101?text=Quero%20saber%20mais%20sobre%20como%20vender%20na%20Lariizana" target='_blank' rel="noreferrer">
                  Vender na Lariizana
                </a>
              </DivItemIC>


              {(social?.facebook || social?.instagram || social?.linkedin || social?.twitter) && (
                <>
                  <span>|</span>
                  <DivItemIC>
                    <span>Siga-nos no</span>
                    <DivItensRedes>
                      {social?.instagram && (
                        <a
                          href={social.instagram}
                          title={"Instagram"}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <BsInstagram size={20} color={colors.hover.white} />
                        </a>
                      )}
                      {social?.twitter && (
                        <a
                          href={social.twitter}
                          title={"Twitter"}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <FiTwitter size={23} color={colors.hover.white} />
                        </a>
                      )}
                      {social?.facebook && (
                        <a
                          href={social.facebook}
                          title={"Facebook"}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <RiFacebookBoxLine size={28} color={colors.hover.white} />
                        </a>
                      )}
                    </DivItensRedes>
                  </DivItemIC>
                </>
              )}
            </DivInfoCompany>
            <DivAccoutUser>
              {!user_data?.cpf && (
                <>
                  <Link onClick={() => set_load_click(true)} prefetch={false} href={'/account/register'}>
                    Registrar
                  </Link>
                  <span>|</span>
                  <Link onClick={() => set_load_click(true)} prefetch={false} href={'/account/login'}>
                    Entrar
                  </Link>
                </>
              )}
              {user_data?.cpf && (
                <>
                  <Link onClick={() => set_load_click(true)} prefetch={false} href={'/account/user'}>Conta</Link>
                  <span>|</span>
                  <a style={{ cursor: 'pointer' }} onClick={on_logout}>Sair</a>
                </>
              )}
            </DivAccoutUser>
          </DivTop>

          <DivMain>
            <div id="logo_">
              <LogoComponent link={asPath !== '/'} />
            </div>

            <StepSearchHeader>
              <FormSearch
                onSubmit={e => {
                  e.preventDefault();
                  if (value_search.length > 0) {
                    set_load_click(true)
                    push(`/buscar/${value_search}`)
                  }
                }}
              >
                <LabelInput>
                  <DivInput>
                    <InputSearch onChange={(e) => set_value_search(e.target.value)} placeholder="Buscar na Lariizana" type="search"></InputSearch>
                    <ButtonSearch>
                      <HiOutlineSearch size={20} color={colors.hover.white} />
                    </ButtonSearch>
                  </DivInput>
                </LabelInput>
              </FormSearch>
              <NavPreSearch>
                <ListPS>
                  <ItemPS>
                    <Link onClick={() => !/\/buscar/i.test(asPath) ? set_load_click(true) : undefined} prefetch={false} href={`/buscar/camisetas`}>Camisetas</Link>
                  </ItemPS>
                  <ItemPS>
                    <Link onClick={() => !/\/buscar/i.test(asPath) ? set_load_click(true) : undefined} prefetch={false} href={`/buscar/blusas`}>Blusas</Link>
                  </ItemPS>
                  <ItemPS>
                    <Link onClick={() => !/\/buscar/i.test(asPath) ? set_load_click(true) : undefined} prefetch={false} href={`/buscar/vestidos`}>Vestidos</Link>
                  </ItemPS>
                </ListPS>
              </NavPreSearch>
            </StepSearchHeader>

            <DivCartSale>
              <Link onClick={() => set_load_click(true)} prefetch={false} href={'/carrinho'}>
                <DivIconCart>
                  <MdOutlineShoppingBag color="#fff" size={40} />
                  <Amount>{amount_cart_sale}</Amount>
                </DivIconCart>
              </Link>
            </DivCartSale>
          </DivMain>
        </Header>
      </MaxContainerComponent>
    </DivContainer >
  );
};
