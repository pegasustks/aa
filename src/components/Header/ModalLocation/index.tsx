import cep from "cep-promise";
import { setCookie } from "cookies-next";
import Image from "next/image";
import { useCallback, useContext, useState } from "react";
import { FaSpinner } from "react-icons/fa";
import { IoMdClose } from "react-icons/io";
import { mask_input_cep } from "utils/mask-cep";
import {
  Modal,
  MessageError,
  Content,
  Right,
  Close,
  DivInput,
  Load,
  Exit,
} from "./styles";
import { AuthContext } from "contexts/AuthContext";

interface modal_prop {
  set_open_modal_location(e: boolean): void;
  set_city(e: string | null): void;
}

export const RenderModalLocation: React.FC<modal_prop> = ({
  set_open_modal_location,
  set_city,
}: modal_prop): JSX.Element => {
  const [cep_value, set_cep_value] = useState<string>("");
  const [message_error, set_message_error] = useState<string | null>(null);
  const [load, set_load] = useState<boolean>(false);
  const { set_user_data, user_data } = useContext(AuthContext);

  const handle_mask_input_cep = useCallback((value: string) => {
    set_cep_value(mask_input_cep(value));
  }, []);

  const get_info_cep = useCallback(async () => {
    await set_load(true);
    if (cep_value.length !== 9) {
      set_message_error("CEP incorreto!");
      setTimeout(() => {
        set_message_error(null);
      }, 1200);
      set_load(false);
      return;
    }
    const info_cep = await cep(cep_value)
      .catch(() => {
        set_message_error("CEP incorreto!");
        setTimeout(() => {
          set_message_error(null);
        }, 1200);
        set_load(false);
      })
      .then((e) => e);

    if (info_cep) {
      const data = JSON.stringify(info_cep);
      setCookie("SID-site-location", data, {
        secure: true,
        path: "/",
        sameSite: "strict",
        maxAge: 2147483647,
      });
      // set_city(`${info_cep.state} - ${info_cep.city}`);
      set_user_data({ ...user_data, cep: info_cep.cep, city: info_cep.city, state: info_cep.state });
      set_open_modal_location(false);
    }
    await set_load(false);
  }, [cep_value, set_open_modal_location, set_user_data, user_data]);

  return (
    <Modal>
      <MessageError open={!!message_error}>
        <p>{message_error}</p>
      </MessageError>
      <Content>
        <Right>
          <div>
            <Close>
              <button
                onClick={() => set_open_modal_location(false)}
                type="button"
                title="fechar"
              >
                <IoMdClose size={25} color="#ffffff" />
              </button>
            </Close>
            <div>
              <h2>Qual a sua localização?</h2>
              <p>Adicione uma localização para vizualizar os valores de frete para sua região.</p>
              <DivInput>
                <input
                  onChange={(e) => handle_mask_input_cep(e.currentTarget.value)}
                  value={cep_value}
                  type="text"
                  placeholder="Digite seu CEP"
                />
                <Load load={load}>
                  <FaSpinner />
                </Load>
              </DivInput>
            </div>
          </div>
          <button onClick={get_info_cep} type="button" className="next">
            Continuar
          </button>
        </Right>
      </Content>
    </Modal>
  );
};
