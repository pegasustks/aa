import styled, { css, keyframes } from 'styled-components';

// const animedrop = keyframes`
//   0% {
//     opacity: 0;
//     transform: scale(0);
//   }
//   100% {
//     opacity: 1;
//     transform: scale(1);
//   }
// `;

export const Modal = styled.div`
  position: absolute;
  left: 0;
  z-index: 999999;
  top: 50px;

  display: flex;
  justify-content: center;
  align-items: center;
  // animation:  0.4s ease forwards;
  // transform-origin: top left;
`;
export const Right = styled.div`
  padding: 25px;
  background-color: #032e47;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  height: 100%;
  width: 100%;
  position: relative;

  ::before {
    position: absolute;
    content: '';
    background: #032e47;
    height: 20px;
    width: 20px;
    top: -10px;
    transform: rotateZ(45deg);
    left: 25px;
  }

  h2 {
    font-family: sans-serif;
    font-size: 20px;
    color: #f8f8f8;
    margin-bottom: 20px;
    text-align: start;
  }

  p {
    text-align: start;
    font-size: 15px;
    color: #ffffff;
    font-family: sans-serif;
  }

  input {
    margin-top: 10px;
    width: 100%;
    height: 40px;
    padding-left: 10px;
    font-size: 15px;
  }

  .next {
    width: 100%;
    height: 45px;
    background: #fff;
    color: #032e47;
    font-weight: 600;
    letter-spacing: 1px;
    border: none;
    outline: none;
    border-radius: 3px;
    cursor: pointer;
    
    :hover {
      background: #f5f5f5;
    }
  }
`;

export const Content = styled.div`
  max-width: 350px;
  width: 100%;
  height: 300px;
  border-radius: 5px;
  box-shadow: -1px 14px 12px -15px rgba(117, 117, 117, 0.349);
  z-index: 999;
`;

export const Close = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-bottom: 10px;

  button {
    outline: none;
    background: transparent;
    cursor: pointer;
    border: 0;

    svg {
      :hover {
        fill: #e73636;
        transition: 0.3s ease;
      }
    }
  }
`;

export const Exit = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background: #00000032;
`;

export const MessageError = styled.div<{ open: boolean }>`
  bottom: ${({ open }) => (open ? 0 : '-50px')};
  opacity: ${({ open }) => (open ? 1 : 0)};
  transition: 0.3s ease;
  max-width: 240px;
  width: 100%;
  height: 50px;
  background: ${({ theme }) => theme.colors.primary};
  position: fixed;
  cursor: default;
  z-index: 999;

  display: flex;
  justify-content: center;
  align-items: center;

  p {
    font-size: 13px;
    color: #fff;
  }
`;

export const DivInput = styled.div`
  position: relative;
  input {
    outline-color: #032e47;
    background: #f1f1f1;
    border: 0;

    :focus {
      background: #ffffff;
    }
  }

`;

export const animation = keyframes`
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(360deg);
  }
`;

export const Load = styled.div<{ load: boolean }>`
  position: absolute;
  display: none;
  ${({ load }) =>
    load &&
    css`
      animation: ${animation} linear infinite 1s;
      display: block;
    `}
  top: calc(50% - 3px);
  transform: rotateZ(0deg);
  right: 10px;
  width: 16px;
  height: 16px;
`;
