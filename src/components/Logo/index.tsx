/* eslint-disable @next/next/no-img-element */
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import Link from "next/link";
import { useContext } from "react";
import styled from "styled-components";

type LogoProps = {
  type?: "1" | "2";
  link?: boolean;
};

export const Container = styled.div`
  max-width: 80px;
`;

export const LogoComponent: React.FC<LogoProps> = ({
  type = "1",
  link,
}): JSX.Element => {
  const { set_load_click } = useContext(TotalLoadingDataContext);
  return (
    <Container>
      {link ? (
        <Link href={'/'}>
          {type === '1' && (
            <img style={{ maxWidth: '100%', height: 'auto' }} src="/images/logo.png" alt="logo-marca" />
          )}
          {type === '2' && (
            <img style={{ maxWidth: '100%', height: 'auto' }} src="/images/logo-2.png" alt="logo-marca" />
          )}
        </Link>
      ) : (
        <>
          {type === '1' && (
            <img style={{ maxWidth: '100%', height: 'auto' }} src="/images/logo.png" alt="logo-marca" />
          )}
          {type === '2' && (
            <img style={{ maxWidth: '100%', height: 'auto' }} src="/images/logo-2.png" alt="logo-marca" />
          )}
        </>
      )}
    </Container>
  );
};
