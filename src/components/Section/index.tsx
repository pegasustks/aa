import Link from "next/link";
import { Section, TitleSection, WapperContainer, WapperTop } from "./style";

interface Section_I {
  title?: {
    value?: string;
    children?: JSX.Element;
    style?: {
      size?: 26 | 30 | 34;
    };
  };
  subTitle?: string;
  link?: string;
  children: JSX.Element;
}

export const SectionComponent: React.FC<Section_I> = ({
  title,
  subTitle,
  children,
  link,
}): JSX.Element => {
  return (
    <WapperContainer>
      <Section>
        <WapperTop size={title?.style?.size ?? 34}>
          {title?.value && (
            <TitleSection size={title.style?.size ?? 34}>
              {title.value}
            </TitleSection>
          )}
          {title?.children && title.children}
          {link && <Link href={link}>Ver mais</Link>}
        </WapperTop>
        {children}
      </Section>
    </WapperContainer>
  );
};
