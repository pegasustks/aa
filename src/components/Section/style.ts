import styled from 'styled-components';

export const WapperContainer = styled.div`
  margin: 20px 0;
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;

export const Section = styled.div``;

export const WapperTop = styled.div<{ size?: number }>`
  display: flex;
  align-items: center;
  margin-bottom: 15px;
  :first-letter {
    text-transform: capitalize;
  }

  a {
    transform: translateY(3px);
    text-decoration: none;
    font-family: sans-serif;
    font-size: 19px;
    color: ${({ theme }) => theme.colors.hover.white};
  }

  h2 {
    :first-letter {
      text-transform: capitalize;
    }
    font-size: ${({ size }) => `${size}px`};
    strong {
      :first-letter {
        text-transform: capitalize;
      }
    }
  }
`;

export const TitleSection = styled.h3<{ size: number }>`
  margin-right: 12px;

  font-weight: 500;
  letter-spacing: 0.4px;
  font-family: ${({ theme }) => theme.font.primary};
  color: #010101;
  font-size: ${({ size }) => `${size}px`};
`;
