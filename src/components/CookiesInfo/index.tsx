import { getCookie, setCookie } from "cookies-next";
import Link from "next/link"
import { useCallback, useEffect, useState } from "react"
import { Button, WapperContainer, WapperInt } from "./styles"
import { useTheme } from "styled-components";

export const CookiesInfoComponent: React.FC = (): JSX.Element | null => {
  const { colors } = useTheme();
  const [check, set_check] = useState<0 | 1 | 2>(0);

  useEffect(() => {
    const get_cookie = getCookie('accepted_cookie');
    if (!get_cookie) {
      set_check(1);
    }
  }, []);

  const handleCookie = useCallback(() => {
    setCookie('accepted_cookie', 'accepted');
    set_check(2);
  }, []);

  return check === 1 ? (
    <WapperContainer>
      <WapperInt>
        <p>
          <strong style={{ color: "#3C3C3C" }}>Lariizana e os cookies:</strong> não utilizamos seus cookies de terceiros, usamos os
          cookies somente para personalizar sua experiencia em nossa plataforma.
          Se continuar navegando, você concorda com a nossa{" "}
          <Link href="/company/information/privacy_terms">
            <strong style={{ color: colors.primary }}>Politica de Privacidade</strong>
          </Link>.
        </p>
        <Button onClick={handleCookie}>Confirmar e fechar</Button>
      </WapperInt>
    </WapperContainer>
  ) : null;
} 