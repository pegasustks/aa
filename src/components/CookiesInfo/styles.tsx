import styled from "styled-components";

export const WapperContainer = styled.div`
  max-width: 1150px;
  width: 100%;
  position: fixed;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 99999;
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;

export const WapperInt = styled.div`
  width: 100%;
  padding: 15px;
  background: #fff;
  border-radius: 20px;
  box-shadow: 0px 3px 7px rgba(0, 0, 0, 0.15);
  display: flex;
  column-gap: 20px;
  align-items: center;
  p {
    font-size: 14px;
  }

  @media screen and (max-width: 630px) {
    flex-direction: column;
    column-gap: 0;
    row-gap: 10px;
    align-items: flex-end;
  }
`;

export const Button = styled.button`
  border: 0;
  outline: none;
  min-width: 170px;
  user-select: none;
  background: transparent;
  border: 2px solid ${({ theme }) => theme.background.primary};
  border-radius: 20px;
  padding: 0 19px;
  height: 50px;
  color:  ${({ theme }) => theme.background.primary};
  cursor: pointer;
  font-weight: 600;

  :hover {
    border: 2px solid ${({ theme }) => theme.background.hover.primary};
    color: ${({ theme }) => theme.colors.hover.primary};
  }
`;