import { useState } from 'react';
import { BiChevronDown, BiChevronUp } from 'react-icons/bi';
import { useTheme } from 'styled-components';
import {
  ButtonWapperSelect,
  Items,
  ListOptions,
  Value,
  ValueInput,
  WapperContainer,
  WapperIcons,
  WapperValueSelected,
} from './styles';

interface SelectNormalProps {
  value: string;
  data: Array<string>;
  not_margin_horizontal?: boolean;
  placeholder?: string;
  on_select?(e: number): void;
}

export const SelectNormalComponent: React.FC<SelectNormalProps> = ({
  value: Valuetitle,
  data,
  not_margin_horizontal,
  placeholder,
  on_select,
}): JSX.Element => {
  const [value, setValue] = useState<string>(data[0]);
  const [active, setActive] = useState(false);

  const pIcon = {
    size: 24,
    color: '#000',
  };

  const handleButton = {
    onClick: () => setActive(!active),
    onBlur: () => setActive(false),
  };

  return (
    <WapperContainer not_margin_horizontal={not_margin_horizontal}>
      <ValueInput>{Valuetitle}</ValueInput>
      <ButtonWapperSelect {...handleButton} type="button">
        <WapperValueSelected>
          <Value>{placeholder ?? value}</Value>

          <WapperIcons>
            {active ? <BiChevronUp {...pIcon} /> : <BiChevronDown {...pIcon} />}
          </WapperIcons>
        </WapperValueSelected>
        {active && (
          <ListOptions>
            {data.map(item => (
              <Items
                key={item}
                onClick={() => {
                  setValue(item);
                  on_select(item === 'Femenino' ? 1 : 0);
                }}
              >
                {item}
              </Items>
            ))}
          </ListOptions>
        )}
      </ButtonWapperSelect>
    </WapperContainer>
  );
};
