import styled from 'styled-components';

export const WapperContainer = styled.div<{ not_margin_horizontal?: boolean }>`
  max-width: 600px;
  width: 100%;
  margin: ${({ not_margin_horizontal }) => !not_margin_horizontal && '0 10px'};
`;
export const ValueInput = styled.p`
  font-size: 17px;
  font-family: ${({ theme }) => theme.font.primary};
  margin-bottom: 5px;
`;
export const ButtonWapperSelect = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border: none;
  outline: none;
  width: 100%;

  cursor: pointer;
  position: relative;
`;

export const WapperValueSelected = styled.div`
  width: 100%;
  height: 45px;
  background: #fff;
  border: 1px solid #d2d2d2;
  box-shadow: 0 0 4px #8b8b8b24;
  padding-left: 10px;
  font-size: 15px;

  position: relative;

  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

export const WapperIcons = styled.div`
  position: absolute;
  right: 5px;
  bottom: 50%;
  transform: translateY(50%);
`;

export const Value = styled.p`
  font-size: 17px;
`;

export const ListOptions = styled.ul`
  position: absolute;
  top: 47px;

  background: #ececec;
  width: 100%;
  height: auto;
  list-style: none;
  box-shadow: 2px 2px 2px #3e3e3e2d;

  z-index: 99;
`;

export const Items = styled.li`
  text-align: start;
  padding: 10px 8px;

  font-size: 16px;

  :hover {
    background: #d8d8d8;
  }
`;
