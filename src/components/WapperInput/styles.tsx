import styled from "styled-components";

export const WapperContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const WapperContent = styled.div`
  background: #fff;
  padding: 45px 10px 20px;
`;

export const TitlePage = styled.h1`
  font-family: ${({ theme }) => theme.font.primary};
  margin: 0 auto 25px auto;
  font-size: 19px;
  color: #000;
  text-align: center;
  max-width: 410px;
  line-height: 28px;
`;

export const WapperLogo = styled.div`
  margin-bottom: 30px;
  display: flex;
  justify-content: center;
`;

export const WapperInterContent = styled.div``;
export const Form = styled.form``;

export const WapperInfoLogin = styled.div`
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 5px;
`;

export const ValueInfo = styled.p`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 14px !important;
  color: #5c5c5c;

  a {
    margin-left: 5px;
    color: #000000 !important;
    font-weight: 600;

    :hover {
      color: #222222 !important;
    }
  }
`;

export const ButtonSee = styled.button`
  min-width: 30px;
  border: none;
  outline: none;
  cursor: pointer;
  background: transparent;
  padding-left: 5px;
`;

export const WapperInput = styled.label<{ not_margin_horizontal?: boolean }>`
  display: flex;
  flex-direction: column;
  max-width: 600px;
  width: 100%;
  margin: ${({ not_margin_horizontal }) => !not_margin_horizontal && "0 10px"};
`;

export const Wapper2Input = styled.div<{ lette?: boolean }>`
  width: 100%;
  display: flex;

  input {
    height: 45px;
    width: 100%;
    border: none;
    outline: none;
    background: #fff;
    border: 1px solid #d2d2d2;
    box-shadow: 0 0 4px #8b8b8b24;
    padding-left: 10px;
    font-size: 15px;

    letter-spacing: ${({ lette }) => lette && "1.4px"};
  }
`;

export const ValueInput = styled.span`
  font-size: 17px;
  font-family: ${({ theme }) => theme.font.primary};
  margin-bottom: 5px;
  strong {
    color: #f71313;
    position: absolute;
    transform: translate(2px, -5px);
  }
`;

export const Textarea = styled.textarea`
  width: 100%;
  border: none;
  outline: none;
  background: #fff;
  border: 1px solid #d2d2d2;
  box-shadow: 0 0 4px #8b8b8b24;
  padding: 4px;

  overflow: auto;

  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;

  resize: none;
`;
