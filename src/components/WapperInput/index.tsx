import { LogoComponent } from '@components/Logo';
import Link from 'next/link';
import { useState } from 'react';
import { BsFillEyeFill, BsFillEyeSlashFill } from 'react-icons/bs';
import {
  ButtonSee,
  Form,
  Textarea,
  TitlePage,
  ValueInfo,
  ValueInput,
  Wapper2Input,
  WapperContainer,
  WapperContent,
  WapperInfoLogin,
  WapperInput,
  WapperInterContent,
  WapperLogo,
} from './styles';

interface LoginRegisterProps {
  children: JSX.Element;
  page: 'register' | 'login' | 'confirm_email' | 'add_address';
  title?: string;
}

export const LoginRegisterLayout: React.FC<LoginRegisterProps> = ({
  children,
  page,
  title,
}): JSX.Element => (
  <WapperContainer>
    <WapperContent style={{ padding: '45px 25px 20px' }}>
      {title && <TitlePage>{title}</TitlePage>}
      <WapperInterContent>
        <Form>{children}</Form>
        {page !== 'confirm_email' && page !== 'add_address' && (
          <WapperInfoLogin>
            <ValueInfo>
              {page === 'register' ? 'Já tem conta?' : 'Ainda não tem conta?'}

              <Link
                href={
                  page === 'register' ? '/account/login' : '/account/register'
                }
              >
                <a>{page === 'register' ? 'Entrar' : 'Criar conta'}</a>
              </Link>
            </ValueInfo>
          </WapperInfoLogin>
        )}
      </WapperInterContent>
    </WapperContent>
  </WapperContainer>
);
