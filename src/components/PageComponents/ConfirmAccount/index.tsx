import { RegisterContext } from "contexts/RegisterContext";
import { setCookie } from "cookies-next";
import Link from "next/link";
import { useCallback, useContext, useEffect, useState } from "react";
import { MdOutlineError } from "react-icons/md";
import { ProviderAPI } from "services/api";
import {
  Content,
  Error,
  FormOutherEmail,
  InfoTop,
  Input,
  LabelText,
  MessageError,
  Title,
  WapperContainer,
  WapperOuther,
} from "./styles";

export interface stepProps {
  setStepRegister: (value: number) => void;
}

interface input_prop {
  title: string;
  value?: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
  code?: boolean;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <p>{title}</p>
      <Input
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
        maxLength={props?.code ? 5 : undefined}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

interface data_errors_prop {
  field: string;
  message: string;
}

export const ConfirmOfAccountComponent: React.FC<{
  whatsapp: string;
}> = ({ whatsapp }): JSX.Element => {
  const { response_error } = useContext(RegisterContext);

  const [outher_whatsapp, set_outher_whatsapp] = useState<boolean>(false);
  const [value_whatsapp, set_value_whatsapp] = useState<string>("");

  const [first_name, set_first_name] = useState<string | null>(null);
  const [data_errors, set_data_errors] = useState<data_errors_prop[]>(
    [] as data_errors_prop[]
  );

  const [send_whatsapp, set_send_whatsapp] = useState<boolean>(false);

  const on_send_new_code = useCallback(async (whatsapp: string) => {
    try {
      await ProviderAPI.put(`/v1/public/update/send-new-code/${whatsapp}`);
    } catch (error: any) {
      return null;
    }
  }, []);

  const on_get_name = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/full-name-user/${whatsapp}`
      );
      set_first_name(data.data.first);
    } catch (error: any) {
      return null;
    }
  }, [whatsapp]);

  const on_change_whatsapp = useCallback(
    async (e?: any) => {
      try {
        e?.preventDefault();
        await ProviderAPI.put(
          `/v1/public/update/change-whatsapp/${whatsapp}/${value_whatsapp}`
        );
        await on_send_new_code(value_whatsapp);
        set_send_whatsapp(true);
      } catch (error: any) {
        if (error?.response?.status === 422) {
          return set_data_errors(error.response.data.data);
        }
      }
    },
    [whatsapp, on_send_new_code, value_whatsapp]
  );

  const on_confirm_code = useCallback(async (code: string, whatsapp: string) => {
    try {
      const { data: data_ } = await ProviderAPI.put(
        `/v1/public/update/confirm-code-user/${code}?whatsapp=${whatsapp}`
      );
      setCookie("auth", data_.data, {
        secure: true,
        path: "/",
        sameSite: "strict",
        maxAge: 60 * 60 * 12,
      });
      window.location.href = "/";
    } catch (error: any) {
      if (error?.response?.status === 422) {
        return set_data_errors(error.response.data.data);
      }
    }
  }, []);

  const on_send_whatsapp_int = useCallback(async () => {
    try {
      await on_send_new_code(whatsapp);
      set_send_whatsapp(true);
    } catch (error: any) {
      return null;
      return null;
    }
  }, [whatsapp, on_send_new_code]);

  useEffect(() => {
    if (on_get_name) on_get_name();
  }, [on_get_name]);

  return (
    <WapperContainer>
      <MessageError open={!!response_error}>
        <MdOutlineError size={35} color="#ffbebe" />
        <p>{response_error}</p>
      </MessageError>
      <Content>
        <div>
          <InfoTop>
            <Title>Confirmar conta</Title>
            {send_whatsapp && (
              <p>
                Enviamos o código de confirmação de conta para o WhatsApp:{" "}
                <strong style={{ letterSpacing: 0 }}>
                  {value_whatsapp === "" ? whatsapp : value_whatsapp}
                </strong>
                , insira-o a baixo para finalizar seu cadastro
              </p>
            )}
            {!send_whatsapp && (
              <p>
                {`Ola ${first_name}` ?? "Oi"}, sua conta ainda não foi
                confirmada. Para confirmar sua conta basta inserir o codigo de 5
                digitos que será enviado para o seu whatsApp
              </p>
            )}
          </InfoTop>
          <WapperOuther outher={outher_whatsapp && !send_whatsapp} load={false}>
            {!outher_whatsapp && !send_whatsapp && (
              <>
                <button type="button" onClick={() => set_outher_whatsapp(true)}>
                  Usar outro WhatsApp
                </button>
                <button onClick={on_send_whatsapp_int} type="button">
                  Enviar codigo: {whatsapp.slice(0, 6)}...
                </button>
              </>
            )}
            {send_whatsapp &&
              renderInput({
                title: "Codigo",
                code: true,
                onChange: (e) => {
                  if (e.target.value.length === 5) {
                    on_confirm_code(
                      e.target.value,
                      value_whatsapp === "" ? whatsapp : value_whatsapp
                    );
                  }
                },
                message_error: data_errors.some((e) => e.field === "code")
                  ? data_errors?.find((e) => e.field === "code")?.message
                  : undefined,
              })}
          </WapperOuther>
          {!send_whatsapp && outher_whatsapp && (
            <FormOutherEmail>
              {renderInput({
                title: "Digite seu WhatsApp",
                onChange: (e) => set_value_whatsapp(e.target.value),
                value: value_whatsapp,
                message_error: data_errors.some((e) => e.field === "code")
                  ? data_errors?.find((e) => e.field === "code")?.message
                  : undefined,
              })}
              <button onClick={on_change_whatsapp} className="b_c" type="submit">
                Enviar código
              </button>
            </FormOutherEmail>
          )}
          <InfoTop>
            <p>
              Ao inserir o codigo de confirmação você aceita com nossa{" "}
              <strong>
                <Link href="/">politica de dados</Link>
              </strong>
              {" e "}
              <strong>
                <Link href="/">termos de uso</Link>
              </strong>
            </p>
          </InfoTop>
        </div>
      </Content>
    </WapperContainer>
  );
};
