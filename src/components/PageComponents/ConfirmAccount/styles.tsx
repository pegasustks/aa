import styled from 'styled-components';

export const WapperContainer = styled.div`
  width: 100%;
  min-height: calc(100vh - 285px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const MessageError = styled.div<{ open: boolean }>`
  max-width: 300px;
  width: 100%;
  height: 70px;
  background: red;

  position: fixed;
  top: 161px;
  right: ${({ open }) => (open ? '16px' : '-305px')};
  transition: 0.3s ease;
  padding: 20px 10px;
  display: flex;
  align-items: center;

  p {
    margin-left: 10px;
    color: #ffd7d7;
    font-weight: 600;
  }
`;

export const Content = styled.div`
  max-width: 400px;
  width: 100%;

  padding: 18px;
  min-height: 470px;

  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export const WapperOuther = styled.div<{ load: boolean; outher: boolean }>`
  display: flex;
  width: 100%;
  margin: 25px 0;

  display: ${({ outher }) => (outher ? 'none' : 'flex')};

  opacity: ${({ load }) => (load ? 0.5 : 1)};

  #email {
    width: 100% !important;
    max-width: 100% !important;
  }

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }

  button {
    width: 100%;
    margin: 0 5px;
    height: 45px;
    outline: none;
    border: none;
    background: #1a1a1a;
    color: #fff;
    :hover {
      background: #0b0b0b;
    }
    cursor: pointer;
    :first-child {
      margin-left: 0;
    }
    :last-child {
      margin-right: 0;
    }
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;

  p {
    font-size: 14px;
  }
`;

export const FormOutherEmail = styled.div`
  width: 100%;
  margin: 10px 0 5px;

  .b_c {
    color: #fff;
    outline: none;
    border: none;
    background: #1a1a1a;
    height: 45px;
    width: 100%;
    margin: 20px 0 15px;
    cursor: pointer;
    font-family: sans-serif !important;
    letter-spacing: 1px;
    :hover {
      background: #0b0b0b;
    }
  }
`;

export const Input = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const InfoTop = styled.div`
  margin-bottom: 20px;

  p {
    margin-top: 5px;
    color: #5a5a5a;
    font-size: 15px;
    font-family: sans-serif;
    letter-spacing: 1px;
  }
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;

export const Title = styled.h3``;

export const WapperInfoEmail = styled.div``;
export const ValueInfo = styled.p``;
