import { CartSaleContext } from 'contexts/CartSaleContext';
import { useContext, useState } from 'react';
import { BiChevronDown, BiChevronUp } from 'react-icons/bi';
import {
  ItemWidth,
  ValueWidth,
  ButtonWapperWidth,
  WapperVisible,
  WapperIcon,
  WapperModalWidth,
} from './styles';

interface prop {
  size: string;
  id: string;
  options?: any[];
  product_id: string;
  item_id: string;
}

export const WidthComponent: React.FC<prop> = ({
  id,
  size,
  options,
  product_id,
  item_id,
}): JSX.Element => {
  const [open, setOpen] = useState(false);
  const { on_change_size_item } = useContext(CartSaleContext);

  return (
    <ButtonWapperWidth onBlur={() => setOpen(false)}>
      <WapperVisible onClick={() => setOpen(true)}>
        <ValueWidth>{size}</ValueWidth>
        <WapperIcon>
          {open ? <BiChevronUp size={19} /> : <BiChevronDown size={19} />}
        </WapperIcon>
      </WapperVisible>
      {open && (
        <WapperModalWidth>
          {options?.map(e => (
            <ItemWidth
              s={e.sizes_fashion_id === id}
              key={e.sizes_fashion_id}
              onClick={async () => {
                await on_change_size_item({
                  item_id,
                  product_id,
                  sizes_fashion_id: e.sizes_fashion_id,
                });
                setOpen(false);
              }}
            >
              {e.value}
            </ItemWidth>
          ))}
        </WapperModalWidth>
      )}
    </ButtonWapperWidth>
  );
};
