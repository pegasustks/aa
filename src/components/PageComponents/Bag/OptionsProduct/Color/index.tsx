import { CartSaleContext } from "contexts/CartSaleContext";
import { useContext, useState } from "react";
import { BiChevronDown, BiChevronUp } from "react-icons/bi";
import {
  ColorSelected,
  Item,
  WapperButtonContainer,
  WapperIcon,
  WapperItem,
  WapperModalListColors,
  WapperVisible,
} from "./styles";

interface prop {
  name_value: string;
  value_color: string;
  color_actual_id: string;
  options?: any[];
  product_id: string;
  item_id: string;
}

export const ColorComponent: React.FC<prop> = ({
  color_actual_id,
  name_value,
  options,
  ...prop
}): JSX.Element => {
  const { load_color, on_change_color_item, data_cart } =
    useContext(CartSaleContext);
  const [open, setOpen] = useState(false);

  return (
    <WapperButtonContainer onBlur={() => setOpen(false)}>
      <WapperVisible
        style={{ opacity: load_color ? 0.5 : 1 }}
        onClick={() => setOpen(true)}
      >
        <ColorSelected title={name_value} background={prop.value_color} />
        <WapperIcon>
          {open ? <BiChevronUp size={19} /> : <BiChevronDown size={19} />}
        </WapperIcon>
      </WapperVisible>
      {open && (
        <WapperModalListColors>
          {data_cart &&
            data_cart
              ?.find((e) => e.id === prop.item_id)
              ?.options?.colors?.map((e) => {
                return (
                  <WapperItem
                    title={e.color_name}
                    key={e.id}
                    onClick={async () => {
                      await on_change_color_item({
                        item_id: prop.item_id,
                        product_id: prop.product_id,
                        color_id: e.id,
                      });
                      setOpen(false);
                    }}
                  >
                    <Item background={e.color_value} />
                  </WapperItem>
                );
              })}
        </WapperModalListColors>
      )}
    </WapperButtonContainer>
  );
};
