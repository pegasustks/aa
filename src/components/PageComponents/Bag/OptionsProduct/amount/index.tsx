import { CartSaleContext } from 'contexts/CartSaleContext';
import { useContext } from 'react';
import { IoIosRemove, IoIosAdd } from 'react-icons/io';
import { ButtonWapperSet, ValueAmount, WapperContainer } from './styles';

interface amout_prop {
  amount: number;
  id: string;
  product_id: string;
  repository: number;
  size_fashion_id: string;
}

export const AmountComponent: React.FC<amout_prop> = ({
  id,
  amount,
  ...prop
}): JSX.Element => {
  const {
    on_change_amount_item,
    on_delete_item_cart_sale,
    load_amount,
    load_delete,
  } = useContext(CartSaleContext);

  return (
    <WapperContainer>
      <ButtonWapperSet
        load={load_amount || load_delete === id}
        onClick={
          load_amount || load_delete === id
            ? undefined
            : () =>
              amount > 1
                ? on_change_amount_item({
                  item_id: id,
                  new_amount: Number(amount) - 1,
                  product_id: prop.product_id,
                  size_fashion_id: prop.size_fashion_id
                })
                : on_delete_item_cart_sale(prop.product_id, id)
        }
      >
        <IoIosRemove size={18} />
      </ButtonWapperSet>
      <ValueAmount>{amount}</ValueAmount>
      <ButtonWapperSet
        load={load_amount || load_delete === id}
        onClick={
          load_amount || load_delete === id
            ? undefined
            : () =>
              on_change_amount_item({
                item_id: id,
                new_amount: Number(amount) + 1,
                product_id: prop.product_id,
                size_fashion_id: prop.size_fashion_id
              })
        }
      >
        <IoIosAdd size={18} />
      </ButtonWapperSet>
    </WapperContainer>
  );
};
