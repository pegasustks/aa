import { AdvertisingSimpleComponent } from "@components/Advertising/Simple";
import { CartSaleContext } from "contexts/CartSaleContext";
import { ListenSizeContext } from "contexts/ListenSize";
import Image from "next/image";
import Link from "next/link";
import { useContext } from "react";
import { ProviderAPI } from "services/api";
import { AmountComponent } from "./OptionsProduct/amount";
import { ColorComponent } from "./OptionsProduct/Color";
import { WidthComponent } from "./OptionsProduct/Width";
import {
  ButtonFinishSale,
  ButtonSale,
  CardItemFormPay,
  DivValuesInfo,
  ImageProduct,
  Item,
  Name,
  OfValues,
  QuestionSale,
  ResponseSale,
  SmallV,
  Table,
  Td,
  Th,
  TitleLink,
  Tr,
  ValueTitileSale,
  ValueTitlePage,
  WapperActionsProduct,
  WapperAmountItem,
  WapperBottomItem,
  WapperButtonPay,
  WapperContainer,
  WapperContentPage,
  WapperContentSale,
  WapperContentSaleContainer,
  WapperFeaturesItem,
  WapperInfomartionTotalSale,
  WapperInfoProduct,
  WapperInformationSale,
  WapperItemCartMobile,
  WapperItemsCart,
  WapperItemsCartMobile,
  WapperItemTopInfo,
  WapperItensFormPay,
  WapperOverflow,
  WapperProduct,
  WapperQuestions,
  WapperRe,
  WapperResumCart,
  WapperSubTotalItem,
  WapperTitleSale,
  WapperTopItem,
  WapperValuesPrice,
  WapperValueTitlePage,
} from "./styles";
import { useTheme } from "styled-components";

export const BagComponent: React.FC = (): JSX.Element => {
  const {
    data_cart,
    load_delete,
    load_size_change,
    load_image_get,
    on_delete_item_cart_sale
  } = useContext(CartSaleContext);

  const { colors } = useTheme();

  const { size } = useContext(ListenSizeContext);

  return (
    <WapperContainer>
      <WapperValueTitlePage>
        <ValueTitlePage>Meu carrinho de compras</ValueTitlePage>
      </WapperValueTitlePage>
      {data_cart.length < 1 ? <p>Está vazio. <Link href={"/"}>Ir as compras</Link></p> : undefined}

      {data_cart.length ? (
        <>
          <WapperInformationSale>
            <WapperContentPage>
              {size <= 899 ? (
                <>
                  <WapperItemsCartMobile>
                    {data_cart.map(e => (
                      <WapperItemCartMobile key={e.id}>
                        <WapperTopItem>
                          <ImageProduct
                            style={{ opacity: load_image_get ? 0.5 : 1 }}
                          >
                            <Image
                              src={`${ProviderAPI.defaults.baseURL}/v1/images/products/${e.main_image}`}
                              width={250}
                              height={278.75}
                              alt="Imagem"
                              style={{ height: "auto", maxWidth: "100%" }}
                            />
                          </ImageProduct>
                          <WapperItemTopInfo>
                            <Link href={`/item/${e.product_id}`}>
                              <TitleLink>{e.title}</TitleLink>
                            </Link>
                            <WapperFeaturesItem>
                              <WidthComponent
                                product_id={e.product_id}
                                size={e?.color?.size?.value ?? ""}
                                id={
                                  e?.color?.size?.sizes_fashion_id ?? ""
                                }
                                options={e.options?.sizes}
                                item_id={e.id}
                              />
                              <ColorComponent
                                item_id={e.id}
                                product_id={e.product_id}
                                color_actual_id={e.color.id}
                                name_value={e.color.color_name}
                                value_color={e.color.color_value}
                                options={e.options?.colors}
                              />
                            </WapperFeaturesItem>
                          </WapperItemTopInfo>
                        </WapperTopItem>
                        <WapperBottomItem>
                          <WapperValuesPrice>
                            <p>Preço <strong>R$ {Number(e.price1).toFixed(2)}</strong></p>
                            <p>Frete <strong>R$ {Number(e?.frete.por).toFixed(2)}</strong></p>
                          </WapperValuesPrice>
                          <WapperAmountItem>
                            <AmountComponent
                              product_id={e.product_id}
                              amount={e.amount}
                              id={e.id}
                              repository={5}
                              size_fashion_id={e.color.size?.sizes_fashion_id!}
                            />
                            <SmallV onClick={() => on_delete_item_cart_sale(e.product_id, e.id)}>remover</SmallV>
                          </WapperAmountItem>
                          <WapperSubTotalItem>
                            <SmallV id='sub'>Subtotal</SmallV>
                            <strong>R$ {Number(
                              Number(Number(e?.price1).toFixed(2)) *
                              Number(e?.amount) +
                              Number(Number(e?.frete.por).toFixed(2))
                            ).toFixed(2)}</strong>
                          </WapperSubTotalItem>
                        </WapperBottomItem>
                      </WapperItemCartMobile>
                    ))}
                  </WapperItemsCartMobile>
                  {data_cart.length > 0 ? (
                    <WapperOverflow>
                      <WapperResumCart>
                        <DivValuesInfo>
                          <OfValues>
                            <p>Total Frete</p>
                            <strong>R$ {data_cart
                              .reduce((ac, ob) => ac + Number(ob.frete.por), 0)
                              .toFixed(2)}</strong>
                          </OfValues>
                          <OfValues>
                            <p>Total a Pagar</p>
                            <strong>R$ {data_cart
                              .reduce(
                                (ac, ob) =>
                                  ac +
                                  Number(Number(ob?.price1).toFixed(2)) * ob.amount +
                                  Number(Number(ob?.frete.por).toFixed(2)),
                                0
                              )
                              .toFixed(2)}</strong>
                          </OfValues>
                        </DivValuesInfo>
                        <ButtonFinishSale>Finalizar Compra</ButtonFinishSale>
                      </WapperResumCart>
                    </WapperOverflow>
                  ) : undefined}
                </>
              ) : undefined}
              {size > 899 ? (
                <WapperItemsCart>
                  {data_cart.length > 0 ? (
                    <Table>
                      <thead>
                        <Tr load={false}>
                          <Th>Produto</Th>
                          <Th>Preço</Th>
                          <Th>Quantidade</Th>
                          <Th>Frete</Th>
                          <Th>Subtotal</Th>
                        </Tr>
                      </thead>
                      <tbody>
                        {data_cart.map((e) => (
                          <Tr load={load_delete === e.id} key={e.id}>
                            <Td>
                              <WapperProduct>
                                <ImageProduct
                                  style={{ opacity: load_image_get ? 0.5 : 1 }}
                                >
                                  <Image
                                    src={`${ProviderAPI.defaults.baseURL}/v1/images/products/${e.main_image}`}
                                    width={250}
                                    height={278.75}
                                    alt="Imagem"
                                    style={{ height: "auto", maxWidth: "100%" }}
                                  />
                                </ImageProduct>
                                <WapperInfoProduct>
                                  <div style={{ marginBottom: 10 }}>
                                    <Link href={`/${e.product_id}`}>
                                      <Name>{e.title}</Name>
                                    </Link>
                                    <WapperActionsProduct>
                                      <WidthComponent
                                        product_id={e.product_id}
                                        size={e?.color?.size?.value ?? ""}
                                        id={
                                          e?.color?.size?.sizes_fashion_id ?? ""
                                        }
                                        options={e.options?.sizes}
                                        item_id={e.id}
                                      />
                                      <ColorComponent
                                        item_id={e.id}
                                        product_id={e.product_id}
                                        color_actual_id={e.color.id}
                                        name_value={e.color.color_name}
                                        value_color={e.color.color_value}
                                        options={e.options?.colors}
                                      />
                                    </WapperActionsProduct>
                                  </div>
                                  <span style={{ fontSize: 13, color: '#4f4f4f' }}>
                                    Vendido por:{' '}
                                    <strong style={{ fontSize: 13, color: '#292929' }}>
                                      {e.name_company}
                                    </strong>
                                  </span>
                                </WapperInfoProduct>
                              </WapperProduct>
                            </Td>
                            <Td
                              style={{
                                fontFamily: "sans-serif",
                                opacity: load_size_change ? 0.5 : 1,
                              }}
                            >
                              R$ {Number(e.price1).toFixed(2)}
                            </Td>
                            <Td>
                              <AmountComponent
                                product_id={e.product_id}
                                amount={e.amount}
                                id={e.id}
                                repository={5}
                                size_fashion_id={e.color.size?.sizes_fashion_id!}
                              />
                            </Td>
                            <Td>
                              <div
                                style={{
                                  textAlign: "start",
                                  fontFamily: "sans-serif",
                                }}
                              >
                                R$ {e?.frete.por}
                              </div>
                            </Td>
                            <Td>
                              <div
                                style={{
                                  width: 90,
                                  textAlign: "start",
                                  fontFamily: "sans-serif",
                                  fontSize: 17,
                                }}
                              >
                                {e?.frete && (
                                  <>
                                    R${" "}
                                    {Number(
                                      Number(Number(e?.price1).toFixed(2)) *
                                      Number(e?.amount) +
                                      Number(Number(e?.frete.por).toFixed(2))
                                    ).toFixed(2)}
                                  </>
                                )}
                              </div>
                            </Td>
                          </Tr>
                        ))}
                      </tbody>
                    </Table>
                  ) : undefined}
                </WapperItemsCart>
              ) : undefined}
            </WapperContentPage>
            {size > 899 && (
              <WapperContentSaleContainer>
                <WapperTitleSale>
                  <ValueTitileSale>Resumo de valores</ValueTitileSale>
                </WapperTitleSale>
                <WapperInfomartionTotalSale>
                  <WapperContentSale>
                    <WapperQuestions>
                      <QuestionSale>Total frete</QuestionSale>
                      <ResponseSale>
                        R${" "}
                        {data_cart
                          .reduce((ac, ob) => ac + Number(ob.frete.por), 0)
                          .toFixed(2)}
                      </ResponseSale>
                    </WapperQuestions>
                    <WapperQuestions>
                      <QuestionSale>Total a pagar</QuestionSale>
                      <ResponseSale>
                        R${" "}
                        {data_cart
                          .reduce(
                            (ac, ob) =>
                              ac +
                              Number(Number(ob?.price1).toFixed(2)) * ob.amount +
                              Number(Number(ob?.frete.por).toFixed(2)),
                            0
                          )
                          .toFixed(2)}
                      </ResponseSale>
                    </WapperQuestions>
                  </WapperContentSale>
                  <WapperButtonPay>
                    <Link href="/pay">
                      <ButtonSale>Finalizar Compra</ButtonSale>
                    </Link>
                  </WapperButtonPay>
                </WapperInfomartionTotalSale>

                <WapperTitleSale>
                  <ValueTitileSale>Forma de Pagamento</ValueTitileSale>
                </WapperTitleSale>
                <WapperItensFormPay>
                  <CardItemFormPay>
                    <Item image="/images/logo-pix.png" />
                  </CardItemFormPay>
                </WapperItensFormPay>
              </WapperContentSaleContainer>
            )}
          </WapperInformationSale>

          <WapperRe>
            <AdvertisingSimpleComponent />
          </WapperRe>
        </>
      ) : undefined}
    </WapperContainer>
  );
};
