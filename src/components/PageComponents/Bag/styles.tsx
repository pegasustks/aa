import styled from "styled-components";

export const WapperContainer = styled.div`
  margin: 30px 0;
`;

export const WapperValueTitlePage = styled.div`
  margin: 20px 0;
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;

export const ValueTitlePage = styled.p`
  font-size: 17px;
  color: #000;
  font-size: 22px;
  font-weight: 300;
`;

export const WapperContentPage = styled.div`
  padding: 10px 0;
  width: 100%;
`;

export const WapperItemsCart = styled.div`
  @media screen and (max-width: 780px) {
    overflow-x: scroll;
  }
  @media screen and (max-width: 1155px) {
    padding-left: 10px;
  }
`;

export const Table = styled.table`
  min-width: 740px;
`;

export const Tr = styled.tr<{ load: boolean }>`
  opacity: ${({ load }) => (load ? 0.5 : 1)};
  :last-child td {
    border: 0;
  }
`;

export const Th = styled.th`
  padding: 5px 0;
  font-family: ${({ theme }) => theme.font.primary};
  color: #000;

  :first-child {
    width: 60%;
  }

  :nth-child(1) {
    text-align: start;
  }

  :nth-child(2) {
    width: 14%;
    text-align: start;
  }

  :nth-child(3) {
    width: 14%;
    text-align: start;
  }

  :nth-child(4) {
    width: 15%;
    text-align: start;
  }
  :nth-child(5) {
    width: 13%;
    text-align: start;
  }
  font-size: 14px;
`;

export const Td = styled.td<{ last?: boolean }>`
  border-bottom: ${({ last }) => !last && "1px solid #d9d9d9"};
  color: #000;
  padding: 10px 0;
`;

export const WapperProduct = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ImageProduct = styled.div`
  overflow: hidden;
  max-width: 119px;
  max-height: 289.12px;
`;

export const WapperInfoProduct = styled.div`
  width: 100%;
  padding: 0 5px 0 15px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Name = styled.p`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 16px;
  padding-right: 10px;
  color: ${({ theme }) => theme.colors.primary};
`;

export const WapperActionsProduct = styled.div`
  display: flex;
  align-items: center;
  margin-top: 10px;

  button {
    margin: 0 5px;

    :first-child {
      margin-left: 0;
    }

    :last-child {
      margin-right: 0;
    }
  }
`;

export const WapperInformationSale = styled.div`
  display: flex;
 
  @media screen and (max-width: 1030px) {
    flex-direction: column;
  }
`;

export const WapperContentSaleContainer = styled.div`
  max-width: 300px;
  min-width: 250px;
  width: 100%;
  margin-left: 15px;

  @media screen and (max-width: 640px) {
    max-width: 800px;
    margin-left: 0;
  }
  @media screen and (max-width: 1155px) {
    padding-right: 10px;
  }
`;

export const WapperInfomartionTotalSale = styled.div`
  background: #fff;
  box-shadow: 0 2px 4px #3b3b3b28;
  padding: 20px;
  margin-bottom: 20px;
`;

export const WapperTitleSale = styled.div`
  margin-bottom: 15px;
`;

export const ValueTitileSale = styled.h3`
  font-size: 18px;
  font-weight: 400;
  font-family: ${({ theme }) => theme.font.primary};
`;

export const WapperContentSale = styled.div`
  display: flex;
  flex-direction: column;
  height: 50px;
  margin-bottom: 15px;
  justify-content: space-between;
`;

export const WapperQuestions = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const QuestionSale = styled.span`
  font-size: 16px;
  color: #636363;
`;

export const ResponseSale = styled.span`
  font-size: 18px;
  font-family: sans-serif;
  color:  ${({ theme }) => theme.colors.primary};
`;

export const WapperButtonPay = styled.div``;

export const ButtonSale = styled.button`
  background:  ${({ theme }) => theme.background.primary};
  border: none;
  outline: none;

  width: 100%;
  height: 40px;
  font-size: 16px;
  letter-spacing: 0.4px;
  color:  ${({ theme }) => theme.colors.white};

  cursor: pointer;

  :hover {
    background:  ${({ theme }) => theme.background.hover.primary};
  }
`;

export const WapperItensFormPay = styled.div`
  margin-top: 10px;
`;

export const CardItemFormPay = styled.div`
  width: 25px;
  height: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Item = styled.div<{ image: string }>`
  width: 25px;
  height: 25px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-image: url(${({ image }) => image});
`;

export const WapperRe = styled.div`
  margin: 20px 0;
`;



// MOBILE

export const WapperItemsCartMobile = styled.div`
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;
export const WapperItemCartMobile = styled.div`
  border-bottom: 1px solid #eeeeee;
  padding: 10px 0;

  :first-child {
    padding-top: 0;
  }

  :last-child {
    padding-bottom: 0;
    border-bottom: 0;
  }
`;

export const WapperTopItem = styled.div`
  display: flex;
  column-gap: 10px;
  margin-bottom: 10px;
`;

export const WapperItemTopInfo = styled.div`
  min-height: 100%;
`;
export const TitleLink = styled.p`
  min-height: 50px;
  margin-bottom: 5px;
  color: ${({ theme }) => theme.colors.primary};
`;
export const WapperFeaturesItem = styled.div`
  display: flex;
  align-items: center;
  column-gap: 10px;
`;
export const WapperBottomItem = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
`;
export const WapperValuesPrice = styled.div`
  p {
    color: #929292;
    font-size: 14px;

    strong {
      color: #000;
    }
  }
`;
export const WapperAmountItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
export const SmallV = styled.small`
  margin-top: 5px;
  color: #929292;

  &#sub {
    margin-top: 0;
    margin-bottom: 5px;
  }
`;
export const WapperSubTotalItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const WapperOverflow = styled.div`
  overflow: hidden;
  padding-top: 23px;
  position: sticky;
  bottom: 0;
`;

export const WapperResumCart = styled.div`
  background: #fff;
  box-shadow: 0 -3px 20px #bababa5e;
  padding: 15px 10px;
`;
export const DivValuesInfo = styled.div`
  row-gap: 2px;
  display: flex;
  flex-direction: column;
`;
export const OfValues = styled.div`
  p {
    color: #929292;
  }
  display: flex;
  justify-content: space-between;
`;
export const ButtonFinishSale = styled.button`
  margin-top: 15px;
  width: 100%;
  height: 45px;
  border: none;
  background: #353535;
  font-weight: bold;
  color: #fff;
  font-size: 15px;
  letter-spacing: 0.4px;
  outline: none;
  :hover {
    background: #000;
  }
`;