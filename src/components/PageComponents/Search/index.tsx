import { GridProducts } from "@components/Global/styles";
import { LoadComponent } from "@components/Load";
import { ProductItem } from "@components/Product";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { BottomScrollListener } from "react-bottom-scroll-listener";
import { IoChevronBackSharp } from "react-icons/io5";
import { ProviderAPI } from "services/api";
import {
  ButtonToBack,
  HeaderTopPage,
  InfoSearch,
  NotFound,
  WapperContainer,
} from "./styles";

interface item_prop {
  main_image: string;
  id: string;
  main_price1: string;
  main_price2: string;
  title: string;
  sales: number;
}

export const SearchComponent: React.FC = (): JSX.Element => {
  const { query, back } = useRouter();
  const [load, set_load] = useState(false);
  const [items, set_items] = useState<item_prop[]>([] as item_prop[]);
  const [page_selected, set_page_selected] = useState<number>(0 as number);

  const [load_plus, set_load_plus] = useState<boolean>(false as boolean);

  const on_search = useCallback(
    async (page: number) => {
      try {
        if (page > 0) set_load_plus(true);
        const { data } = await ProviderAPI.get(
          `/v1/public/get/product-search/${page}/${query.search}`
        );

        set_items([...items, ...data.data]);
        if (page > 0) {
          setTimeout(() => {
            set_load_plus(false);
          }, 2700);
        } else {
          set_load(true);
        }
      } catch (error) {
        return undefined;
      }
    },
    [items, query.search]
  );

  useEffect(() => {
    on_search(page_selected);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page_selected]);

  return (
    <WapperContainer>
      <HeaderTopPage>
        <InfoSearch>
          <p>Buscando por:</p> <h1>{query.search}</h1>
        </InfoSearch>
      </HeaderTopPage>

      {!load && <LoadComponent />}
      {load && (
        <>
          {items.length === 0 && (
            <NotFound>
              <strong>Produto não encontrado :(</strong>
            </NotFound>
          )}
          {items.length > 0 && (
            <BottomScrollListener
              onBottom={() =>
                load && !load_plus && set_page_selected(page_selected + 1)
              }
            >
              <GridProducts>
                {items.map((e) => (
                  <ProductItem
                    key={'1'}
                    sales={e.sales}
                    id={e.id}
                    main_image={e.main_image}
                    main_price1={e.main_price1}
                    main_price2={e.main_price2}
                    title={e.title}
                  />
                ))}
              </GridProducts>
            </BottomScrollListener>
          )}
        </>
      )}
    </WapperContainer>
  );
};
