import styled from "styled-components";

export const WapperContainer = styled.div`
  margin: 20px 0;
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;

export const HeaderTopPage = styled.div`
  display: flex;
  margin-bottom: 20px;
`;
export const ButtonToBack = styled.button`
  height: 28px;
  width: 32px;
  border: none;
  outline: none;
  cursor: pointer;
  :hover {
    background: #f7f7f7;
  }
`;
export const InfoSearch = styled.div`
  display: flex;
  align-items: center;

  p {
    color: #1f1f1f;
    font-size: 14px;
    font-weight: 400;
  }

  h1 {
    font-size: 18px;
    margin-left: 8px;
    font-weight: 400;
  }
`;

export const NotFound = styled.div`
  width: 100%;
  min-height: 180px;
  display: flex;
  justify-content: center;
  align-items: center;

  strong {
    color: #cccccc;
  }
`;
