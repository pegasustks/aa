import { data_states } from "@data/states";
import cep from "cep-promise";
import { RegisterContext } from "contexts/RegisterContext";
import { useRouter } from "next/router";
import { useCallback, useContext, useState } from "react";
import { mask_input_cep } from "utils/mask-cep";

import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import useOnclickOutside from "react-cool-onclickoutside";

import useScript from "react-script-hook/lib/use-script";
import Head from "next/head";
import {
  ButtonRegister,
  DivIinputs,
  DropSu,
  Error,
  InfoTop,
  Input,
  ItSu,
  LabelText,
  Title,
  WapperButtonConfirmAddress,
} from "./styles";

interface vprop {
  value: string;
  disabled: boolean;
}
interface dataProps {
  CEP: string;
  city: vprop;
  state: vprop;
  address: string;
  district: vprop;
  number: string;
  complement: string;
  geolocation: string;
}

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
  disabled?: boolean;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <p>{title}</p>
      <Input
        type={props.type ?? "text"}
        value={props?.value ?? ""}
        error={!!props?.message_error}
        onChange={onChange}
        disabled={props.disabled}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

const RenderInputSelect = ({
  title,
  ...props
}: Omit<input_prop, "value" | "onChange" | "disabled" | "type"> & {
  set_value: (e: any) => void;
}): JSX.Element => {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    debounce: 300,
  });

  const handle_select = useCallback(
    ({ description, structured_formatting }: any) =>
      () => {
        setValue(structured_formatting.main_text, false);
        clearSuggestions();

        getGeocode({ address: description }).then((results) => {
          const { lat, lng } = getLatLng(results[0]);
          props.set_value({
            address: structured_formatting.main_text,
            geolocation: `${lat}, ${lng}`,
          });
        });
      },
    [clearSuggestions, props, setValue]
  );

  const ref = useOnclickOutside(() => {
    clearSuggestions();
  });

  return (
    <LabelText ref={ref}>
      <p>{title}</p>
      <Input
        value={value}
        onChange={(e) => setValue(e.target.value)}
        type="text"
        error={!!props?.message_error}
      />
      {data.length > 0 && (
        <DropSu>
          {data.map((e) => (
            <ItSu
              title={`${e.structured_formatting.main_text} - ${e.structured_formatting.secondary_text}`}
              key={e.place_id}
              onClick={handle_select(e)}
            >
              <strong>{e.structured_formatting.main_text}</strong>{" "}
              <small>{e.structured_formatting.secondary_text}</small>
            </ItSu>
          ))}
        </DropSu>
      )}
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

export const StepAddAddressComponent: React.FC = (): JSX.Element => {
  const { load_address, on_create_address } = useContext(RegisterContext);
  const { query } = useRouter();

  const [data, setData] = useState<dataProps>({} as dataProps);

  return (
    <div>
      <InfoTop>
        <Title>Meu endereço</Title>
        <p>Por ultimo precisamos do seu endereço de residencia.</p>
      </InfoTop>
      {/* <ButtonAdd onClick={undefined}>
        <span>Adicionar minha geolocalização</span>
        <BoxCheck>
          <div className="icon">
            <IoCheckmarkSharp color="#fff" size={22} />
          </div>
        </BoxCheck>
      </ButtonAdd> */}
      <DivIinputs load={load_address}>
        {renderInput({
          title: "Endereço/Rua",
          onChange: (e) =>
            setData({
              ...data,
              address: e.target.value,
            }),
          value: data?.address ?? "",
        })}
        {renderInput({
          title: "CEP",
          onChange: async (e) => {
            setData({
              ...data,
              CEP: mask_input_cep(e.target.value),
              city: { disabled: false, value: "" },
              complement: "",
              district: { disabled: false, value: "" },
              number: "",
              state: { disabled: false, value: "" },
            });
            if (e.target.value.length === 9) {
              try {
                const dataz = await cep(e.target.value.replace(/\D/g, ""));
                setData({
                  ...data,
                  CEP: mask_input_cep(e.target.value),
                  city: {
                    disabled: !!dataz.city,
                    value: dataz.city,
                  },
                  state: {
                    disabled: !!data_states[dataz.state],
                    value: data_states[dataz.state],
                  },
                  district: {
                    disabled: !!dataz.neighborhood,
                    value: dataz.neighborhood,
                  },
                });
              } catch (error) {
                return undefined;
              }
            }
          },
          value: data.CEP,
        })}
        {renderInput({
          title: "Estado",
          onChange: (e) =>
            setData({
              ...data,
              state: {
                value: e.target.value.replace(/\d/g, ""),
                disabled: false,
              },
            }),
          value: data?.state?.value ?? "",
          disabled: true,
        })}
        {renderInput({
          title: "Cidade",
          onChange: (e) =>
            setData({
              ...data,
              city: {
                value: e.target.value.replace(/\d/g, ""),
                disabled: false,
              },
            }),
          value: data?.city?.value ?? "",
          disabled: true,
        })}
        {renderInput({
          title: "Bairro",
          onChange: (e) =>
            setData({
              ...data,
              district: {
                value: e.target.value.replace(/\d/g, ""),
                disabled: false,
              },
            }),
          value: data?.district?.value ?? "",
          disabled: !!data?.district?.disabled,
        })}
        {renderInput({
          title: "Numero",
          onChange: (e) =>
            setData({ ...data, number: e.target.value.replace(/\D/g, "") }),
          value: data.number,
        })}
        {renderInput({
          title: "Complemento",
          onChange: (e) => setData({ ...data, complement: e.target.value }),
          value: data.complement,
        })}
      </DivIinputs>
      <WapperButtonConfirmAddress>
        <a href={query.p ? "/carrinho" : "/"}>Pular</a>
        <ButtonRegister
          onClick={() =>
            load_address
              ? undefined
              : on_create_address({ ...data, cep: data.CEP.replace(/\D/g, "") })
          }
        >
          {load_address ? <strong>. . .</strong> : "Finalizar cadastro"}
        </ButtonRegister>
      </WapperButtonConfirmAddress>
    </div>
  );
};
