import styled from 'styled-components';

export const WapperOuther = styled.div`
  display: flex;
  width: 100%;
  margin: 25px 0;

  #email {
    width: 100% !important;
    max-width: 100% !important;
  }

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;
  position: relative;

  p {
    font-size: 14px;
  }
`;

export const Input = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;

export const InfoTop = styled.div`
  margin-bottom: 20px;

  p {
    margin-top: 5px;
    color: #5a5a5a;
    font-size: 15px;
    font-family: sans-serif;
    letter-spacing: 1px;
  }
`;

export const DivIinputs = styled.div<{ load: boolean }>`
  opacity: ${({ load }) => (load ? 0.5 : 1)};
`;

export const ButtonAdd = styled.button`
  width: 100%;
  height: 45px;
  margin-bottom: 10px;
  cursor: pointer;

  border: none;
  outline: none;
  background: transparent;
  border: 1px solid #000;

  display: flex;
  align-items: center;
  justify-content: center;

  span {
    letter-spacing: 1px;
    font-size: 15px;
  }
  :hover {
    background: #f3f3f3;
  }
`;

export const BoxCheck = styled.div`
  position: relative;
  margin-left: 15px;
  background:  ${({ theme }) => theme.background.primary};

  width: 30px;
  height: 30px;
  border: 1px solid #000;

  .icon {
    top: 50%;
    left: 50%;
    position: absolute;
    transform: translate(-50%, -50%);
  }
`;

export const Title = styled.h3``;

export const WapperInfoAddAddress = styled.div``;
export const ValueInfo = styled.p``;

export const WapperButtonConfirmAddress = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 25px;

  a {
    color: #000;
    font-family: sans-serif;
  }
`;

export const ButtonRegister = styled.button`
  cursor: pointer;
  max-width: 240px;
  width: 100%;
  height: 40px;
  border: none;
  outline: none;
  font-weight: 500;
  letter-spacing: 0.5px;
  font-size: 14px;
  background: ${({ theme }) => theme.background.primary};
  color: #fff;
  :hover {
    background: ${({ theme }) => theme.background.hover.primary};
  }
`;

export const DropSu = styled.ul`
  position: absolute;
  top: 63px;
  width: 100%;
  background: #f3f3f3;
  list-style: none;
  z-index: 9;
  box-shadow: 0 2px 4px #bfbfbf7c;
`;

export const ItSu = styled.li`
  padding: 10px 5px;

  :hover {
    background: #eaeaea;
    cursor: pointer;
  }

  strong {
    font-size: 13px;
  }

  small {
    font-size: 11px;
  }
`;
