import styled from 'styled-components';

export const WapperOuther = styled.div`
  display: flex;
  width: 100%;
`;

export const InfoTop = styled.div`
  margin-bottom: 20px;

  p {
    margin-top: 5px;
    color: #5a5a5a;
    font-size: 15px;
    font-family: sans-serif;
    letter-spacing: 1px;
  }
`;

export const WapperButton = styled.div`
  display: flex;
  margin-top: 35px;
`;

export const ButtonRegister = styled.button<{ load: boolean }>`
  cursor: pointer;
  max-width: 240px;
  width: 100%;
  height: 40px;
  border: none;
  outline: none;
  font-weight: 500;
  letter-spacing: 0.5px;
  font-size: 14px;
  
  opacity: ${({ load }) => (load ? 0.5 : 1)};
  
  background: ${({ theme }) => theme.background.primary};
  color: #fff;
  :hover {
    background: ${({ theme }) => theme.background.hover.primary};
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;

  p {
    font-size: 14px;
  }
`;

export const Input = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;

export const Title = styled.h3``;

export const Form = styled.form<{ load: boolean }>`
  margin-top: 10px;
  opacity: ${({ load }) => (load ? 0.5 : 1)};
`;
