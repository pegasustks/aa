import { SelectNormalComponent } from "@components/SelectNormal";
import { RegisterContext } from "contexts/RegisterContext";
import { useContext } from "react";
import { mask_input_birth_date } from "utils/mask-birth_date";
import { mask_input_cpf } from "utils/mask-cpf";
import { stepProps } from "../..";
import {
  ButtonRegister,
  Error,
  Form,
  InfoTop,
  Input,
  LabelText,
  Title,
  WapperButton,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <p>{title}</p>
      <Input
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

export const StepCreateComponent: React.FC<stepProps> = ({
  setStepRegister,
}): JSX.Element => {
  const {
    data_fields,
    data_errors,
    load_register,
    set_data_fields,
    set_data_errors,
    on_register,
  } = useContext(RegisterContext);

  return (
    <>
      <InfoTop>
        <Title>Bem-vindo a Lariizana</Title>
      </InfoTop>
      <Form load={load_register}>
        {renderInput({
          title: "Nome completo",
          value: data_fields?.full_name ?? "",
          onChange: (e) => {
            set_data_fields({ ...data_fields, full_name: e.target.value });
            set_data_errors(data_errors.filter((e) => e.field !== "full_name"));
          },
          message_error: data_errors.some((e) => e.field === "full_name")
            ? data_errors?.find((e) => e.field === "full_name")?.message
            : undefined,
        })}
        {renderInput({
          title: "WhatsApp",
          value: data_fields?.whatsapp ?? "",
          onChange: (e) => {
            set_data_fields({ ...data_fields, whatsapp: e.target.value });
            set_data_errors(data_errors.filter((e) => e.field !== "whatsapp"));
          },
          message_error: data_errors.some((e) => e.field === "whatsapp")
            ? data_errors?.find((e) => e.field === "whatsapp")?.message
            : undefined,
        })}
        {renderInput({
          title: "CPF",
          value: data_fields?.cpf ?? "",
          onChange: (e) => {
            set_data_fields({
              ...data_fields,
              cpf: mask_input_cpf(e.target.value),
            });
            set_data_errors(data_errors.filter((e) => e.field !== "cpf"));
          },
          message_error: data_errors.some((e) => e.field === "cpf")
            ? data_errors?.find((e) => e.field === "cpf")?.message
            : undefined,
        })}
        {renderInput({
          title: "Data de nascimento",
          value: data_fields?.birth_date ?? "",
          onChange: (e) => {
            set_data_fields({
              ...data_fields,
              birth_date: mask_input_birth_date(e.target.value),
            });
            set_data_errors(
              data_errors.filter((e) => e.field !== "birth_date")
            );
          },
          message_error: data_errors.some((e) => e.field === "birth_date")
            ? data_errors?.find((e) => e.field === "birth_date")?.message
            : undefined,
        })}
        {renderInput({
          title: "Senha",
          value: data_fields?.password ?? "",
          onChange: (e) => {
            set_data_fields({ ...data_fields, password: e.target.value });
            set_data_errors(data_errors.filter((e) => e.field !== "password"));
          },
          message_error: data_errors.some((e) => e.field === "password")
            ? data_errors?.find((e) => e.field === "password")?.message
            : undefined,
          type: "password",
        })}
        {renderInput({
          title: "Confirmar senha",
          value: data_fields?.confirm_password ?? "",
          onChange: (e) => {
            set_data_fields({
              ...data_fields,
              confirm_password: e.target.value,
            });
            set_data_errors(
              data_errors.filter((e) => e.field !== "confirm_password")
            );
          },
          message_error: data_errors.some((e) => e.field === "confirm_password")
            ? data_errors?.find((e) => e.field === "confirm_password")?.message
            : undefined,
          type: "password",
        })}

        <SelectNormalComponent
          not_margin_horizontal
          data={["Femenino", "Masculino"]}
          value="Gênero"
          on_select={(e) => set_data_fields({ ...data_fields, genre: e })}
        />
      </Form>
      <WapperButton>
        <ButtonRegister
          load={load_register}
          onClick={() =>
            load_register ? undefined : on_register(setStepRegister)
          }
        >
          Continuar com o cadastro
        </ButtonRegister>
      </WapperButton>
    </>
  );
};
