import { RegisterContext } from "contexts/RegisterContext";
import Link from "next/link";
import { useCallback, useContext, useEffect, useState } from "react";
import { stepProps } from "../..";
import {
  Error,
  InfoTop,
  Input,
  LabelText,
  Title,
  WapperOuther,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <p>{title}</p>
      <Input
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
        maxLength={5}
        max={4}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

export const StepCodeConfirmComponent: React.FC<stepProps> = ({
  setStepRegister,
}): JSX.Element => {
  const { on_confirm_whatsapp, load_confirm, data_errors } =
    useContext(RegisterContext);

  const [code_value, set_code_value] = useState<string>("");

  useEffect(() => {
    if (code_value.length === 5) on_confirm_whatsapp(setStepRegister, code_value);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code_value]);

  return (
    <div>
      <InfoTop>
        <Title>Confirmar conta</Title>
        <p>Enviamos o código de confirmação para o seu WhatsApp, insira-o no campo abaixo para finalizar seu cadastro</p>
      </InfoTop>
      <WapperOuther load={load_confirm}>
        {renderInput({
          title: "Código",
          onChange: (e) => set_code_value(e.target.value.replace(/\D/g, "")),
          value: code_value,
          message_error: data_errors.some((e) => e.field === "code")
            ? data_errors?.find((e) => e.field === "code")?.message
            : undefined,
        })}
      </WapperOuther>
      <InfoTop style={{ marginBottom: 0 }}>
        <p>
          Ao inserir o código de confirmação você aceita com nossa{" "}
          <strong>
            <Link href="/company/information/terms_of_use">Política de dados</Link>
          </strong>
          {" e "}
          <strong>
            <Link href="/company/information/privacy_terms">Termos de uso</Link>
          </strong>
        </p>
      </InfoTop>
    </div>
  );
};
