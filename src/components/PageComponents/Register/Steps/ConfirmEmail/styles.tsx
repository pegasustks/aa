import styled from 'styled-components';

export const WapperOuther = styled.div<{ load: boolean }>`
  display: flex;
  width: 100%;
  margin: 25px 0;

  opacity: ${({ load }) => (load ? 0.5 : 1)};

  #email {
    width: 100% !important;
    max-width: 100% !important;
  }

  :first-child {
    margin-top: 0;
  }

  :last-child {
    margin-bottom: 0;
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;

  p {
    font-size: 14px;
  }
`;

export const Input = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const InfoTop = styled.div`
  margin-bottom: 20px;

  p {
    margin-top: 5px;
    color: #5a5a5a;
    font-size: 15px;
    font-family: sans-serif;
    letter-spacing: 1px;

    strong {
      color:  ${({ theme }) => theme.colors.primary};
    }
  }
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;

export const Title = styled.h3``;

export const WapperInfoEmail = styled.div``;
export const ValueInfo = styled.p``;
