import styled from 'styled-components';

export const WapperContainer = styled.div`
  width: 100%;
  min-height: calc(100vh - 195px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const MessageError = styled.div<{ open: boolean }>`
  max-width: 300px;
  width: 100%;
  height: 70px;
  background: red;

  position: fixed;
  top: 161px;
  right: ${({ open }) => (open ? '16px' : '-305px')};
  transition: 0.3s ease;
  padding: 20px 10px;
  display: flex;
  align-items: center;

  p {
    margin-left: 10px;
    color: #ffd7d7;
    font-weight: 600;
  }
`;

export const Content = styled.div`
  max-width: 400px;
  width: 100%;
  background-color: #fff;

  padding: 18px;
  min-height: auto;

  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin-top: 40px;
`;
