import { RegisterContext } from 'contexts/RegisterContext';
import { useContext, useState } from 'react';
import { MdOutlineError } from 'react-icons/md';
import { StepAddAddressComponent } from './Steps/AddAddress';
import { StepCodeConfirmComponent } from './Steps/ConfirmEmail';
import { StepCreateComponent } from './Steps/Create';
import { Content, MessageError, WapperContainer } from './styles';

export interface stepProps {
  setStepRegister: (value: number) => void;
}

export const RegisterComponent: React.FC = (): JSX.Element => {
  const [stepRegister, setStepRegister] = useState(0);

  const { response_error } = useContext(RegisterContext);

  return (
    <WapperContainer>
      <MessageError open={!!response_error}>
        <MdOutlineError size={35} color="#ffbebe" />
        <p>{response_error}</p>
      </MessageError>
      <Content>
        {stepRegister === 0 && (
          <StepCreateComponent setStepRegister={setStepRegister} />
        )}
        {stepRegister === 1 && (
          <StepCodeConfirmComponent setStepRegister={setStepRegister} />
        )}
        {stepRegister === 2 && <StepAddAddressComponent />}
      </Content>
    </WapperContainer>
  );
};
