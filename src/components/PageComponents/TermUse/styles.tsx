import styled from 'styled-components';

export const WapperContainer = styled.div`
  margin: 50px 0;
  width: 100%;

  h1 {
    margin-bottom: 20px;
  }
  h2 {
    margin: 15px 0;
  }
  p {
    margin: 5px 0;
  }
  li {
    margin: 5px 0;
  }

  @media screen and (max-width: 790px) {
    h1 {
      font-size: 24px;
    }
    p {
      font-size: 14px;
    }
  }

  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
`;
