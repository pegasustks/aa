import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { ProviderAPI } from "services/api";
import ReactMarkdown from "react-markdown";
import { WapperContainer } from "./styles";

export const TermUseComponent: React.FC = (): JSX.Element => {
  const { query } = useRouter();
  // const [load, set_load] = useState(false);
  const [text, set_text] = useState("");

  const on_get_field_company = useCallback(async (field: string) => {
    try {
      const { data } = await ProviderAPI.get(
        `/v1/public/get/company-field/${field}`
      );
      set_text(data.data[field]);
    } catch (error) {
      return undefined;
    }
  }, []);

  useEffect(() => {
    on_get_field_company(query.param as string);
  }, [on_get_field_company, query]);

  return (
    <WapperContainer>
      <ReactMarkdown
        components={{
          ul: ({ node, ...props }) => <ul style={{ paddingLeft: 20 }} {...props} />,
          p: ({ node, ...prop }) => (
            <>
              <p style={{ margin: '10px 0' }} {...prop} />
            </>
          )
        }}
      >
        {text}
      </ReactMarkdown>
    </WapperContainer>
  );
};
