import { LoadComponent } from "@components/Load";
import { PayContext } from "contexts/PayContext";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { usePageVisibility } from "react-page-visibility";
import { stepProps } from "..";
import { LayoutComponentPay } from "../Layout";
import {
  ButtonCopy,
  DivLoad,
  ImageQrCode,
  Input,
  TextValueInfoQrCode,
  ValueInfoPay,
  WapperConfirmPay,
  WapperContent,
  WapperInfoPay,
  WapperInfoQrCode,
  WapperInput,
  WapperNotification,
  WapperQRcode,
  WapperTextInfo,
} from "./styles";

export const ConfirmComponent: React.FC<stepProps> = (): JSX.Element => {
  const inputCode = useRef<any>(null);
  const {
    status_pay,
    data_order_payment,
    load_verify_status,
    on_verify_order_payment,
  } = useContext(PayContext);

  const isVisible = usePageVisibility();

  const [copy, setCopy] = useState(false);
  const [back_to_page, set_back_to_page] = useState<boolean>(false);

  useEffect(() => {
    if (isVisible) {
      if (data_order_payment?.id_request) {
        set_back_to_page(true);
        if (back_to_page) {
          on_verify_order_payment();
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  const onCopyCode = useCallback((text: string) => {
    navigator.clipboard.writeText(text);
    setCopy(true);
    setTimeout(() => setCopy(false), 2000);
  }, [setCopy]);

  return (
    <LayoutComponentPay
      step={3}
      title="Efetuar Pagamento"
      sub_title="Seu pedido é confirmado após o pagamento. QR code inspira em 10 minutos."
    >
      <>
        {!data_order_payment?.total_price && (
          <p>Processo interrompido após o parceiro de envios retornar error, tente novamente mais tarde.</p>
        )}
        {data_order_payment.total_price && (
          <WapperConfirmPay>
            <WapperInfoPay>
              <ValueInfoPay>
                Pague com QRcode ou se preferir, copie o codigo abaixo
              </ValueInfoPay>
              <span>
                Valor total da compra:{" "}
                <strong>
                  R$ {Number(data_order_payment?.total_price).toFixed(2)}
                </strong>
              </span>
            </WapperInfoPay>
            <WapperContent>
              <WapperQRcode status={!load_verify_status && status_pay}>
                {load_verify_status && (
                  <DivLoad>
                    <LoadComponent />
                  </DivLoad>
                )}
                <ImageQrCode qr={data_order_payment.url_qr} />
              </WapperQRcode>
              <WapperInfoQrCode>
                <WapperTextInfo>
                  <TextValueInfoQrCode>
                    Ou copie o codigo abaixo
                  </TextValueInfoQrCode>
                </WapperTextInfo>
                <WapperInput>
                  <Input
                    disabled
                    value={data_order_payment.qr_code ?? ""}
                    ref={inputCode}
                  />
                  <ButtonCopy onClick={() => onCopyCode(data_order_payment.qr_code ?? '')}>Copy</ButtonCopy>
                  <WapperNotification copy={copy}>
                    <span>Copiado!</span>
                  </WapperNotification>
                </WapperInput>
              </WapperInfoQrCode>
            </WapperContent>
          </WapperConfirmPay>
        )}
      </>
    </LayoutComponentPay>
  );
};
