import styled, { css } from 'styled-components';

// CONFIRM
export const WapperConfirmPay = styled.div``;
export const WapperInfoPay = styled.div`
  height: 80px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  span {
    margin-top: 5px;
    color: #000;
    font-family: ${({ theme }) => theme.font.primary};

    strong {
      color: #000;
      font-family: sans-serif;
      font-size: 18px;
    }
  }
`;

export const ValueInfoPay = styled.h3`
  font-family: sans-serif;
  font-weight: 300;
  color: #000;
  font-size: 20px;
`;

export const WapperContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`;

export const WapperQRcode = styled.div<{ status?: boolean }>`
  position: relative;

  ${({ status }) =>
    status &&
    css`
      ::after {
        z-index: 9999999;
        content: 'O pagamento foi realizado';
        position: absolute;
        width: 300px;
        height: 300px;
        background: #000000ea;
        top: 0;
        left: 0;

        text-align: center;
        line-height: 25px;
        letter-spacing: 1px;
        color: #fff;
        font-weight: bold;
        display: flex;
        justify-content: center;
        align-items: center;
      }
    `}
`;

export const DivLoad = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: #ffffffea;
  top: 0;
  left: 0;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ImageQrCode = styled.div<{ qr: string }>`
  width: 300px;
  height: 300px;

  background-image: url(${({ qr }) => qr});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;

  @media screen and (max-width: 440px) {
    width: 250px;
    height: 250px;
  }
`;

export const WapperInfoQrCode = styled.div`
  margin-top: 20px;
  border-top: 1px solid #ededed;
  width: 70%;
  padding-top: 10px;

  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const WapperTextInfo = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TextValueInfoQrCode = styled.p`
  font-family: sans-serif;
  color: #000;
`;

export const WapperInput = styled.div`
  position: relative;

  @media screen and (max-width: 490px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const Input = styled.input`
  border: none;
  background: #fff;
  outline: none;
  width: 220px;
  height: 35px;
  box-shadow: 0 0 6px #e9e9e9bc;
  border: 1px solid #dadada;
`;

export const ButtonCopy = styled.button`
  height: 35px;
  margin-left: 5px;
  border: none;
  background: #0f0f0f;
  outline: none;
  width: 75px;
  color: #fff;
  text-transform: uppercase;
  transition: 0.2s ease;
  cursor: pointer;
  font-weight: 600;
  letter-spacing: 1px;

  :hover {
    background: #000000;
  }

  @media screen and (max-width: 490px) {
    margin: 5px 0 0;
  }
`;

export const WapperNotification = styled.div<{ copy: boolean }>`
  background:  ${({ theme }) => theme.background.primary};
  width: 90px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;

  span {
    color: #fff;
    font-family: sans-serif;
    font-size: 14px;
  }

  position: absolute;
  top: 3px;
  left: 70px;

  transition: 0.2s ease;
  opacity: ${({ copy }) => (copy ? 1 : 0)};
`;
