import { AuthContext } from "contexts/AuthContext";
import { PayAddressContext } from "contexts/PayAddressContext";
import { PayContext } from "contexts/PayContext";
import { useContext, useEffect, useState } from "react";
import { mask_input_cep } from "utils/mask-cep";
import { mask_input_cpf } from "utils/mask-cpf";
import { stepProps } from "..";
import { LayoutComponentPay } from "../Layout";
import {
  CheckBox,
  ItemFormPay,
  ItemProduct,
  TopInfo,
  ValueInfoUser,
  ValueNameForm,
  ValueProduct,
  ValueResum,
  ValueTitleResum,
  WapperContentInfo,
  WapperContentInfoPay,
  WapperFormOfPayContent,
  WapperItemsFormOfPay,
  WapperOuther,
  WapperResumPay,
  WapperTitleResum,
} from "./styles";

interface ItemFormOfPayProps {
  value: string;
  available?: boolean;
  onClick(e: string): void;
  id: string;
}

const RenderItemFormOfPay: React.FC<ItemFormOfPayProps> = ({
  value,
  available,
  onClick,
  id,
}): JSX.Element => {
  const { method_pay } = useContext(PayContext);
  return (
    <ItemFormPay
      onClick={() => (available ? onClick(id) : undefined)}
      available={available}
    >
      <CheckBox
        onChange={() => undefined}
        checked={method_pay === id}
        type="radio"
        name="form-of-pay"
        disabled={!available}
      />
      <ValueNameForm>{value}</ValueNameForm>
    </ItemFormPay>
  );
};

interface ob_prices_prop {
  total_frete: number;
  total_price: number;
}

export const FormOfPayComponent: React.FC<stepProps> = ({
  setStep,
  setChangeStep,
}): JSX.Element => {
  const { user_data } = useContext(AuthContext);
  const { address } = useContext(PayAddressContext);
  const {
    set_method_pay,
    data_list_request,
    on_create_payment_sale,
    on_get_cart_sale,
  } = useContext(PayContext);

  useEffect(() => {
    on_get_cart_sale();
  }, [on_get_cart_sale]);

  const [ob_prices, set_ob_prices] = useState<ob_prices_prop | null>(
    null as ob_prices_prop | null
  );

  const [step_calculator, set_step_calculator] = useState<number>(0);

  useEffect(() => {
    (async () => {
      await setTimeout(async () => {
        if (data_list_request && data_list_request[0]?.frete) {
          set_ob_prices({
            total_frete: data_list_request
              ?.reduce((ac, ob) => ac + Number(ob?.frete), 0)
              ?.toFixed(2),
            total_price: data_list_request
              ?.reduce(
                (ac, ob) =>
                  ac +
                  Number(Number(ob?.price1).toFixed(2)) * ob.amount +
                  Number(Number(ob?.frete).toFixed(2)),
                0
              )
              .toFixed(2),
          });
          set_step_calculator(1);
        }
      }, 1800);
    })();
  }, [data_list_request, set_ob_prices]);

  return (
    <LayoutComponentPay
      step={2}
      funcClickNext={async () => {
        await on_create_payment_sale();
        setChangeStep(true);
        setTimeout(() => {
          setStep("PAY");
          setChangeStep(false);
        }, 600);
      }}
      funcClickBackTo={() => {
        setChangeStep(true);
        setTimeout(() => {
          setStep("ADDRESS");
          setChangeStep(false);
        }, 600);
      }}
      to_back
      title="Forma de Pagamento"
      sub_title="Como será a forma de pagamento?"
      next
    >
      <WapperFormOfPayContent>
        <WapperResumPay>
          <WapperTitleResum>
            <ValueTitleResum>Resumo do pedido</ValueTitleResum>
          </WapperTitleResum>
          <WapperContentInfoPay>
            <WapperContentInfo>
              <ValueInfoUser>{user_data?.full_name}</ValueInfoUser>
              <ValueInfoUser>
                {mask_input_cpf(user_data?.cpf ?? "")}
              </ValueInfoUser>
              <ValueInfoUser>
                {address?.address}, {address?.number}, {address?.district},{" "}
                {address?.city} - {address?.state}
              </ValueInfoUser>
              <ValueInfoUser>
                {mask_input_cep(address?.cep ?? "")}
              </ValueInfoUser>
            </WapperContentInfo>
            <WapperContentInfo>
              {data_list_request?.map((item) => (
                <ItemProduct key={item.id}>
                  <TopInfo>
                    <ValueProduct>{item.title}</ValueProduct>
                    <ValueProduct>
                      {String(item.amount).padStart(2, "0")}x
                    </ValueProduct>
                    <ValueProduct>
                      R${" "}
                      {(
                        Number(Number(item.price1).toFixed(2)) *
                        Number(item.amount)
                      ).toFixed(2)}
                    </ValueProduct>
                  </TopInfo>
                  <TopInfo>
                    <ValueProduct>
                      <strong>Cor: {item.color.color_name}</strong>
                    </ValueProduct>
                    <ValueProduct>
                      Tamanho: {item.color.size.value}
                    </ValueProduct>
                  </TopInfo>
                </ItemProduct>
              ))}
            </WapperContentInfo>
            {step_calculator === 0 && <p>Calculando valores</p>}
            {step_calculator === 1 && (
              <WapperContentInfo>
                <WapperOuther>
                  <ValueResum>Total frete</ValueResum>
                  <ValueResum>R$ {ob_prices?.total_frete}</ValueResum>
                </WapperOuther>
                <WapperOuther>
                  <ValueResum>Total a pagar</ValueResum>
                  <ValueResum id="total">
                    R$ {ob_prices?.total_price}
                  </ValueResum>
                </WapperOuther>
              </WapperContentInfo>
            )}
          </WapperContentInfoPay>
        </WapperResumPay>

        <WapperItemsFormOfPay>
          <RenderItemFormOfPay
            onClick={set_method_pay}
            id="pix"
            available
            value="PIX"
          />
        </WapperItemsFormOfPay>
      </WapperFormOfPayContent>
    </LayoutComponentPay>
  );
};
