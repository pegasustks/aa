import { PayAddressContext } from 'contexts/PayAddressContext';
import { PayContext } from 'contexts/PayContext';
import { useContext } from 'react';
import {
  ButtonNext,
  ValueSubTitlePage,
  ValueTitlePage,
  WapperButtonNext,
  WapperContainer,
  WapperContent,
  WapperTitlePage,
} from './styles';

interface LayoutPayProps {
  children: JSX.Element;
  title: string;
  sub_title: string;
  to_back?: boolean;
  next?: boolean;
  funcClickNext?: () => void;
  funcClickBackTo?: () => void;
  step: 1 | 2 | 3;
}

export const LayoutComponentPay: React.FC<LayoutPayProps> = ({
  children,
  title,
  sub_title,
  to_back,
  funcClickNext,
  funcClickBackTo,
  next,
  step,
}): JSX.Element => {
  const { address_selected_id } = useContext(PayAddressContext);
  const { method_pay } = useContext(PayContext);

  return (
    <WapperContainer>
      <WapperTitlePage>
        <div>
          <ValueTitlePage>{title}</ValueTitlePage>
          <ValueSubTitlePage>{sub_title}</ValueSubTitlePage>
        </div>
        <span>
          Etapa <strong style={{ letterSpacing: 1 }}>{step}/3</strong>
        </span>
      </WapperTitlePage>

      <WapperContent>
        <div>{children}</div>
        <WapperButtonNext to_back={to_back}>
          {to_back && (
            <ButtonNext onClick={funcClickBackTo} to_back={to_back}>
              Voltar
            </ButtonNext>
          )}
          {(step === 2 ? method_pay === 'pix' : address_selected_id) &&
            next && <ButtonNext onClick={funcClickNext}>Continuar</ButtonNext>}
        </WapperButtonNext>
      </WapperContent>
    </WapperContainer>
  );
};
