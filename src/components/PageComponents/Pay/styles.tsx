import styled from 'styled-components';

export const WapperContainer = styled.div``;

// STEPS

export const WapperStepsOfPay = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 15px 0;
`;

export const WapperCarouselSteps = styled.div`
  min-height: 660px;
  background:  ${({ theme }) => theme.background.white};
  width: 100%;
  display: flex;

  position: relative;
`;

export const WapperChangeStep = styled.div`
  position: absolute;
  background:  ${({ theme }) => theme.background.white};
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 999;
`;
