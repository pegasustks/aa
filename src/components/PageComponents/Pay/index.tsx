import { MaxContainerComponent } from '@components/Global/MaxContainer';
import { Dispatch, SetStateAction, useState } from 'react';
import { AddressComponent } from './Address';
import { ConfirmComponent } from './Confirm';
import { FormOfPayComponent } from './FormOfPay';
import {
  WapperCarouselSteps,
  WapperChangeStep,
  WapperContainer,
  WapperStepsOfPay,
} from './styles';

type stepStateProps = 'ADDRESS' | 'FORM' | 'PAY';
export interface stepProps {
  setStep: Dispatch<SetStateAction<stepStateProps>>;
  setChangeStep: (value: boolean) => void;
}

export const PayComponent: React.FC = (): JSX.Element => {
  const [step, setStep] = useState<stepStateProps>('ADDRESS');
  const [changeStep, setChangeStep] = useState(false);

  return (
    <WapperContainer>
      <WapperStepsOfPay>
        <MaxContainerComponent>
          <WapperCarouselSteps>
            {changeStep && (
              <WapperChangeStep>
                <p>Aguarde, Carregando...</p>
              </WapperChangeStep>
            )}
            {step === 'ADDRESS' && (
              <AddressComponent
                setChangeStep={setChangeStep}
                setStep={setStep}
              />
            )}
            {step === 'FORM' && (
              <FormOfPayComponent
                setChangeStep={setChangeStep}
                setStep={setStep}
              />
            )}
            {step === 'PAY' && (
              <ConfirmComponent
                setChangeStep={setChangeStep}
                setStep={setStep}
              />
            )}
          </WapperCarouselSteps>
        </MaxContainerComponent>
      </WapperStepsOfPay>
    </WapperContainer>
  );
};
