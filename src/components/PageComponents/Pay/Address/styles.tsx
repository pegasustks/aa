import styled from 'styled-components';

export const WapperAddNewAddress = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const WapperInformationNewAddress = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  min-height: 500px;
  max-width: 400px;
  text-align: center;
  margin: 0 auto;

  a {
    margin-top: 20px;
    height: 40px;
    background: #222222;
    color: #fff;
    display: flex;
    padding: 0 20px;
    justify-content: center;
    align-items: center;
    font-size: 14px;

    :hover {
      background: #000;
    }
  }
`;

export const WapperContent = styled.div`
  width: 100%;

  margin: 0 auto;

  :first-child {
    margin-left: 0;
  }
  :last-child {
    margin-right: 0;
  }
`;

export const WapperTitleStep = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 15px;
`;

export const ValueTitle = styled.p`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 17px;
`;

export const WapperOther = styled.div`
  display: flex;
`;

export const WapperInput = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin: 5px 5px;
  width: 100%;

  :first-child {
    margin-left: 0;
  }
  :last-child {
    margin-right: 0;
  }
`;

export const ValueInput = styled.span`
  font-family: sans-serif;
`;

export const Input = styled.input`
  margin-top: 5px;
  border: none;
  outline: none;
  height: 35px;
  width: 100%;
  border: 1px solid #d8d8d8;
  padding-left: 10px;
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 16px;
  box-shadow: 3px 2px 4px #ecececcc;
`;

export const ButtonTrocarSenha = styled.button`
  border: none;
  outline: none;
  height: 35px;
  background: ${({ theme }) => theme.background.white2};
  max-width: 140px;
  width: 100%;
  margin: 10px 0 10px 5px;

  color:  ${({ theme }) => theme.colors.white};
  font-size: 15px;
  font-weight: 600;
  cursor: pointer;

  transition: 0.4s ease;

  :hover {
    background:  ${({ theme }) => theme.background.hover.primary};
  }
`;

export const WapperButton = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  margin-top: 20px;
`;

export const ButtonSave = styled.button`
  max-width: 180px;
  width: 100%;
  background: #222222;
  border: none;
  outline: none;
  height: 41px;
  color: #fff;
  font-weight: 600;
  letter-spacing: 0.8px;
  transition: 0.4s ease;
  cursor: pointer;

  :hover {
    background: #000;
  }
`;

export const WapperItemAddress = styled.div`
  display: flex;
  justify-content: space-between;
  background: #f6f6f6;
  padding: 10px;
  border: 1px solid #dfdfdf;
`;

export const WapperContentItem = styled.div`
  :last-child {
    display: flex;
    flex-direction: column;
  }

  :first-child {
    padding: 2px 15px;
  }
`;

export const ValueItem = styled.p`
  font-family: ${({ theme }) => theme.font.primary};
  line-height: 24px;
`;

export const WapperActionIcon = styled.button`
  width: 70px;
  height: 25px;
  border: none;
  outline: none;
  background-color: transparent;

  margin: 3px 0;
  color: #323232;
  cursor: pointer;
  transition: 0.2s ease;

  :hover {
    color: #000;
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;
  position: relative;

  p {
    font-size: 14px;
  }
`;

export const Inputx = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const DropSu = styled.ul`
  position: absolute;
  top: 63px;
  width: 100%;
  background: #f3f3f3;
  list-style: none;
  z-index: 9;
  box-shadow: 0 2px 4px #bfbfbf7c;
`;

export const ItSu = styled.li`
  padding: 10px 5px;

  :hover {
    background: #eaeaea;
    cursor: pointer;
  }

  strong {
    font-size: 13px;
  }

  small {
    font-size: 11px;
  }
`;

export const Inputs = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};

  :disabled {
    opacity: 0.6;
  }
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;

export const ValueInformation = styled.p`
  font-size: 16px;
  font-family: ${({ theme }) => theme.font.primary};
  color: #000;
`;

// ITEM ADDRESS EXISTENT
export const WapperSetAddressExistent = styled.div``;

export const WapperTitleSetAddressExistent = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px 0;
`;

export const ValueTitleSetAddress = styled.p`
  font-size: 18px;
  font-family: ${({ theme }) => theme.font.primary};
`;

export const WapperContentAddressSave = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;

export const WapperAddressExistent = styled.label`
  display: flex;
  align-items: center;
  max-width: 280px;
  width: 100%;
  height: 80px;
  background: #e6e6e6;
  padding: 10px;
  cursor: pointer;
`;

export const CheckBox = styled.input`
  width: 20px;
  height: 20px;
`;

export const WapperInfoAddressEx = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: ${({ theme }) => theme.font.primary};
  margin-left: 10px;
`;

export const ValueAddress = styled.p``;

export const PlusAddress = styled.p`
  font-size: 15px;
  font-family: sans-serif;
  line-height: 18px;
`;
