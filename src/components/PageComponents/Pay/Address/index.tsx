import cep from "cep-promise";
import { PayAddressContext } from "contexts/PayAddressContext";
import { UserContext } from "contexts/UserContext";
import Link from "next/link";
import { useCallback, useContext, useEffect, useState } from "react";
import useOnclickOutside from "react-cool-onclickoutside";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import { mask_input_cep } from "utils/mask-cep";
import { stepProps } from "..";
import { LayoutComponentPay } from "../Layout";
import {
  ButtonSave,
  CheckBox,
  DropSu,
  Error,
  Input,
  Inputs,
  Inputx,
  ItSu,
  LabelText,
  PlusAddress,
  ValueAddress,
  ValueInformation,
  ValueInput,
  ValueTitle,
  ValueTitleSetAddress,
  WapperAddNewAddress,
  WapperAddressExistent,
  WapperButton,
  WapperContent,
  WapperContentAddressSave,
  WapperInfoAddressEx,
  WapperInformationNewAddress,
  WapperInput,
  WapperOther,
  WapperSetAddressExistent,
  WapperTitleSetAddressExistent,
  WapperTitleStep,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
  pla?: string;
  disabled?: boolean;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <ValueInput>{title}</ValueInput>
      <Inputs
        disabled={props.disabled}
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
        placeholder={props.pla}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

const RenderInputSelect = ({
  title,
  ...props
}: Omit<input_prop, "onChange" | "disabled" | "type"> & {
  set_value: (e: any) => void;
  set_cep: (e: string) => void;
}): JSX.Element => {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    debounce: 300,
  });

  const handle_select = useCallback(
    ({ description, structured_formatting }: any) =>
      () => {
        setValue(structured_formatting.main_text, false);
        clearSuggestions();

        getGeocode({ address: description }).then((results) => {
          const { lat, lng } = getLatLng(results[0]);
          props.set_cep("");
          props.set_value({
            address: structured_formatting.main_text,
            geolocation: `${lat}, ${lng}`,
            city: "",
            state: "",
            number: "",
            complement: "",
            district: "",
            cep: "",
          });
        });
      },
    [clearSuggestions, props, setValue]
  );

  const ref = useOnclickOutside(() => {
    clearSuggestions();
  });

  return (
    <LabelText ref={ref}>
      <p>{title}</p>
      <Inputx
        value={value || props.value}
        onChange={(e) => {
          setValue(e.target.value);
          props.set_value({ address: e.target.value });
        }}
        type="text"
        error={!!props?.message_error}
      />
      {data.length > 0 && (
        <DropSu>
          {data.map((e) => (
            <ItSu
              title={`${e.structured_formatting.main_text} - ${e.structured_formatting.secondary_text}`}
              key={e.place_id}
              onClick={handle_select(e)}
            >
              <strong>{e.structured_formatting.main_text}</strong>{" "}
              <small>{e.structured_formatting.secondary_text}</small>
            </ItSu>
          ))}
        </DropSu>
      )}
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

interface ItemAddress {
  address: string;
  number: string;
  city: string;
  state: string;
  district: string;
  onClick: (e: string) => void;
  id: string;
}

const RenderItemAddress: React.FC<ItemAddress> = ({
  address,
  city,
  district,
  number,
  state,
  onClick,
  id,
}): JSX.Element => {
  const { address_selected_id } = useContext(PayAddressContext);

  return (
    <WapperAddressExistent onClick={() => onClick(id)}>
      <CheckBox
        onChange={() => undefined}
        type="radio"
        checked={!!address_selected_id}
        name="address-exist"
      />
      <WapperInfoAddressEx>
        <ValueAddress>
          {address} - N° {number}, {district}, {city} - {state}
        </ValueAddress>
      </WapperInfoAddressEx>
    </WapperAddressExistent>
  );
};

interface field_prop {
  state: string;
  city: string;
  number: string;
  address: string;
  complement: string;
  district: string;
  geolocation: string;
}

export const AddressComponent: React.FC<stepProps> = ({
  setStep,
  setChangeStep,
}): JSX.Element => {
  const { address, on_get_address_user, set_address_id } =
    useContext(PayAddressContext);

  const [fields_value, set_fields_value] = useState<field_prop>(
    {} as field_prop
  );

  const [cep_va, set_cep] = useState<string>("");

  const [load_cep, set_load_cep] = useState<boolean>(false);
  const get_info_cep = useCallback(
    async (e: string) => {
      await set_load_cep(true);
      if (e.length !== 8) {
        set_load_cep(false);
        return;
      }
      const info_cep = await cep(e)
        .catch(() => {
          set_load_cep(false);
        })
        .then((e) => e);

      if (info_cep) {
        set_fields_value({
          ...fields_value,
          city: info_cep.city,
          state: info_cep.state,
        });
      }
      await set_load_cep(false);
    },
    [fields_value]
  );

  useEffect(() => {
    if (on_get_address_user) on_get_address_user();
  }, [on_get_address_user]);

  return (
    <LayoutComponentPay
      step={1}
      title="Endereço"
      sub_title="Onde vamos entregar a sua compra?"
      next
      funcClickNext={() => {
        setChangeStep(true);
        setTimeout(() => {
          setStep("FORM");
          setChangeStep(false);
        }, 600);
      }}
    >
      <>
        {!address && (
          <WapperInformationNewAddress>
            <ValueInformation>
              Não encontramos o seu endereço de entrega em nossos bancos de
              dados, para continuar com a compra adicione seu endereço em:{" "}
              <strong>Minha conta</strong> {">"} <strong>Endereço</strong>
            </ValueInformation>
            <Link href="/account/user?address=1">Adicionar meu endereço</Link>
          </WapperInformationNewAddress>
        )}
        {address && (
          <WapperSetAddressExistent>
            <WapperTitleSetAddressExistent>
              <ValueTitleSetAddress>
                Selecione o endereço de entrega salvo
              </ValueTitleSetAddress>
            </WapperTitleSetAddressExistent>
            <WapperContentAddressSave>
              <RenderItemAddress
                onClick={w => {
                  set_address_id(w);
                  setStep('FORM');
                }}
                id={address?.id ?? ""}
                number={address.number}
                city={address.city}
                address={address.address}
                state={address.state}
                district={address.district}
              />
            </WapperContentAddressSave>
          </WapperSetAddressExistent>
        )}
      </>
    </LayoutComponentPay>
  );
};
