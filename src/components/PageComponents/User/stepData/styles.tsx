import styled, { css } from 'styled-components';

export const WapperContentStep = styled.div`
  padding-bottom: 30px;
`;

export const WapperOther = styled.div<{ load: boolean }>`
  max-width: 360px;

  opacity: ${({ load }) => (load ? 0.5 : 1)};

  :nth-child(3) {
    flex-direction: row !important;
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    width: 100%;
  }
`;

export const WapperOther2 = styled.div`
  max-width: 360px;
  display: flex;
`;

export const WapperInput = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin: 10px 0;
  width: 100%;
`;

export const ValueInput = styled.span`
  font-family: sans-serif;
`;

export const Input = styled.input`
  margin-top: 5px;
  border: none;
  outline: none;
  height: 35px;
  width: 100%;
  border: 1px solid #d8d8d8;
  padding-left: 10px;
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 16px;
  box-shadow: 3px 2px 4px #ecececcc;
`;

export const ButtonTrocarSenha = styled.button<{ ys: boolean; s?: boolean }>`
  border: none;
  outline: none;
  height: 45px;
  background: ${({ s }) => (s ? '#11857f' : '#222222')};
  width: 100%;

  ${({ ys }) =>
    ys &&
    css`
      :first-child {
        margin-right: 5px;
      }
      :last-child {
        margin-left: 5px;
      }
    `}

  margin-top: 15px;

  color: #fff;
  font-weight: 600;
  cursor: pointer;

  transition: 0.4s ease;
`;

export const WapperButton = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  justify-content: flex-end;
`;

export const ButtonSave = styled.button`
  max-width: 266px;
  width: 100%;
  background: ${({ theme }) => theme.background.white2};
  border: none;
  outline: none;
  height: 41px;
  color:  ${({ theme }) => theme.colors.white};
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0.8px;
  transition: 0.4s ease;
  cursor: pointer;

  :hover {
    background:  ${({ theme }) => theme.background.hover.primary};
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;

  p {
    font-size: 14px;
  }
`;

export const Inputs = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000;
  }
`;
