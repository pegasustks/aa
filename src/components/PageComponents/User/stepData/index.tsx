import { UserContext } from "contexts/UserContext";
import { useContext, useEffect, useState } from "react";
import { mask_input_cpf } from "utils/mask-cpf";
import {
  ButtonSave,
  ButtonTrocarSenha,
  Error,
  Input,
  Inputs,
  LabelText,
  ValueInput,
  WapperButton,
  WapperContentStep,
  WapperInput,
  WapperOther,
  WapperOther2,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
  pla?: string;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <ValueInput>{title}</ValueInput>
      <Inputs
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
        placeholder={props.pla}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

interface fields_change_prop {
  actual: string;
  new: string;
  repeat: string;
}

export const StepDataComponent: React.FC = (): JSX.Element => {
  const {
    user_data,
    data_errors,
    change_pass,
    load_change_p,
    status_change_p,
    set_change_pass,
    set_data_errors,
    on_get_data_user,
    on_change_password,
  } = useContext(UserContext);

  useEffect(() => {
    if (on_get_data_user) on_get_data_user();
  }, [on_get_data_user]);

  // MANAGER COMPONENT

  const [fields_pass, set_fields_pass] = useState<fields_change_prop>(
    {} as fields_change_prop
  );

  return (
    <WapperContentStep>
      {!change_pass && (
        <WapperOther load={false}>
          <WapperInput>
            <ValueInput>Primeiro nome</ValueInput>
            <Input value={user_data.full_name?.split(" ")[0]} disabled />
          </WapperInput>
          <WapperInput>
            <ValueInput>Resto do nome</ValueInput>
            <Input
              value={user_data.full_name
                ?.split(" ")
                .splice(1)
                .toString()
                .replace(/,/g, " ")}
              disabled
            />
          </WapperInput>
          <WapperInput>
            <ValueInput>WhatsApp</ValueInput>
            <Input value={user_data?.whatsapp ?? ''} disabled />
          </WapperInput>
          <WapperInput>
            <ValueInput>CPF</ValueInput>
            <Input
              value={
                user_data?.cpf
                  ? mask_input_cpf(user_data?.cpf)
                  : "000.000.000-00"
              }
              disabled
            />
          </WapperInput>
          <WapperInput>
            <ValueInput>Data de Nascimento</ValueInput>
            <Input
              value={new Date(user_data?.birth_date ?? "").toLocaleDateString(
                "pt-br"
              )}
              disabled
            />
          </WapperInput>
        </WapperOther>
      )}
      {change_pass && (
        <WapperOther load={load_change_p}>
          <WapperInput>
            {renderInput({
              title: "Senha atual",
              onChange: (e) => {
                set_fields_pass({ ...fields_pass, actual: e.target.value });
                set_data_errors(
                  data_errors.filter((e) => e.field !== "actual")
                );
              },
              value: fields_pass.actual,
              type: "password",
              pla: "***",
              message_error: data_errors.some((e) => e.field === "actual")
                ? data_errors?.find((e) => e.field === "actual")?.message
                : undefined,
            })}
          </WapperInput>
          <WapperInput>
            {renderInput({
              title: "Nova senha",
              onChange: (e) => {
                set_fields_pass({ ...fields_pass, new: e.target.value });
                set_data_errors(data_errors.filter((e) => e.field !== "new"));
              },
              value: fields_pass.new,
              type: "password",
              pla: "***",
              message_error: data_errors.some((e) => e.field === "new")
                ? data_errors?.find((e) => e.field === "new")?.message
                : undefined,
            })}
          </WapperInput>
          <WapperInput>
            {renderInput({
              title: "Repetir nova senha",
              onChange: (e) => {
                set_fields_pass({ ...fields_pass, repeat: e.target.value });
                set_data_errors(
                  data_errors.filter((e) => e.field !== "repeat")
                );
              },
              value: fields_pass.repeat,
              type: "password",
              pla: "***",
              message_error: data_errors.some((e) => e.field === "repeat")
                ? data_errors?.find((e) => e.field === "repeat")?.message
                : undefined,
            })}
          </WapperInput>
        </WapperOther>
      )}
      <WapperOther2>
        <ButtonTrocarSenha
          ys={change_pass}
          onClick={() => set_change_pass(!change_pass)}
        >
          {change_pass ? "Cancelar" : "Alterar Senha"}
        </ButtonTrocarSenha>
        {change_pass && (
          <ButtonTrocarSenha
            s={!!status_change_p}
            onClick={async () => {
              if (!load_change_p) {
                await on_change_password(fields_pass);
                set_fields_pass({ actual: "", new: "", repeat: "" });
              }
            }}
            ys={change_pass}
          >
            {status_change_p
              ? "Senha foi alterada"
              : load_change_p
                ? "Alterando"
                : "Alterar Senha"}
          </ButtonTrocarSenha>
        )}
      </WapperOther2>

      {/* <WapperButton>
        <ButtonSave>Salvar</ButtonSave>
      </WapperButton> */}
    </WapperContentStep>
  );
};
