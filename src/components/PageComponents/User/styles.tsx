import styled from 'styled-components';

export const WapperContainer = styled.div`
  padding: 30px 0;
`;

export const WapperInfoPage = styled.div`
  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
  p {
    color: #535353;
    font-weight: 300;
  }
`;

export const ValueInfoPage = styled.h2`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 22px;
`;

export const WapperContent = styled.div`
  background: #fff;
  margin: 40px auto 20px auto;
  padding: 5px;
`;

export const WapperHeaderStep = styled.div`
  height: 115px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const ValueTitle = styled.h1`
  font-family: ${({ theme }) => theme.font.primary};
  font-weight: 300;
  font-size: 20px;
  padding: 15px 0 0 10px;
`;

export const ListSteps = styled.ul`
  display: flex;
  padding: 0 50px;
  list-style: none;
  border-bottom: 3px solid #eaeaea;
`;

export const ItemStep = styled.li<{ active: boolean }>`
  max-width: 180px;
  width: 100%;
  height: 30px;
  font-family: sans-serif;
  color: #000;
  text-align: center;
  cursor: pointer;
  transform: translateY(3px);

  border-bottom: 2px solid
    ${({ theme, active }) => (active ? '#000' : '#eaeaea')};
`;

export const ContentStepSelected = styled.div`
  margin-top: 20px;
  padding: 0 30px 20px;
`;
