import styled, { css } from 'styled-components';

export const WapperContentStep = styled.div`
  padding-bottom: 30px;
`;

export const FormCancel = styled.form`
  margin-top: 5px;
  background: #ffe0e0;
  padding: 5px;
  p {
    margin-bottom: 10px;
  }

  input {
    padding-left: 5px;
  }

  button {
    margin-top: 5px;
  }
`;
