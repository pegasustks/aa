import { UserContext } from "contexts/UserContext";
import { useCallback, useContext, useEffect, useState } from "react";
import { FormCancel, WapperContentStep } from "./styles";

type ob_number_T = { [x: string]: string };

const status_parther_request: { [x: string]: string } = {
  waiting: "Aguardando vendedor",
  canceled: "Cancelado",
  preparing: "Preparando",
  ok: "Pronto para envio",
  in_transit: "A caminho",
  delivered: "Entregue",
  pending: "Aguardando confirmação pagamento",
};

export const StepMySalesComponent: React.FC = (): JSX.Element => {
  const {
    on_get_my_sales_user,
    on_cancel_item_request_user,
    load_my_sales,
    my_sales,
  } = useContext(UserContext);

  useEffect(() => {
    if (on_get_my_sales_user) on_get_my_sales_user();
  }, [on_get_my_sales_user]);

  const [on_cancel, set_on_cancel] = useState<string>("");
  const [ob_number_whatsapp, set_ob_number_whatsapp] = useState<ob_number_T>(
    {} as ob_number_T
  );

  return (
    <WapperContentStep>
      {load_my_sales && <p>Carregando...</p>}
      {!load_my_sales && my_sales?.length === 0 && (
        <p>Você ainda não comprou.</p>
      )}
      {!load_my_sales && my_sales && my_sales.length > 0 && (
        <>
          {my_sales?.map((e, i) => (
            <div
              key={e.id}
              style={{
                margin: "10px 0",
                boxShadow: "0 0 4px #000",
                padding: 5,
              }}
            >
              <p>Código do pedido: {e.code_sale}</p>
              <p>
                Data de compra:{" "}
                {String(new Date(e.date_created).toLocaleDateString("pt-br"))}
              </p>
              <p>
                Status: <strong>{e.status}</strong>
              </p>
              <p>Destino: {`${e.address} - N° ${e.number}, ${e.district}, ${e.cep}`}</p>
              <div>
                <ul style={{ margin: "10px 0 0 0" }}>
                  {e.items.map((es) => (
                    <li
                      style={{
                        margin: `${my_sales.length - 1 === i ? "10px 0 0" : `10px 0`
                          }`,
                        padding: 5,
                      }}
                      key={es.id}
                    >
                      <p>Produto: {es.title}</p>
                      <p>Previsão de entrega: {new Date(String(es.delivery_date)).toLocaleDateString()}</p>
                      <p>Serviço: {es.delivery_service}</p>
                      {es.tracking_code && <p>Codigo de rastreio: {es.tracking_code}</p>}
                      <p>Quantidade: {es.amount}</p>
                      <p>Preço: {es.price}</p>
                      <p>Frete: {es.frete_price}</p>
                      <p>Tamanho: {es.size_value}</p>
                      <p>Status: {es.delivery_service === 'kangu' ? es.data_status : status_parther_request[es.status]}</p>
                      {es.status === "canceled" && (
                        <p>Estornado: {es.reversed ? "Sim" : "Não"}</p>
                      )}
                      {es.delivery_service !== 'kangu' && es?.status_public?.map((e, i) => (
                        <p key={`${e.date_create}${i}`}>{new Date(e.date_create).toLocaleDateString('pt-br')} {e.message.split(' :notpreview=:')[0]}</p>
                      ))}
                      {es.delivery_service === 'kangu' && es?.historico?.length ? (
                        <p>{es.historico}</p>
                      ) : undefined}
                      {es.status !== "canceled" &&
                        es.status !== "delivered" && (
                          <>
                            <button
                              style={{ marginTop: 5 }}
                              onClick={() => set_on_cancel(es.id)}
                              type="button"
                            >
                              Cancelar item
                            </button>
                            {on_cancel === es.id && (
                              <FormCancel
                                onSubmit={async (e) => {
                                  e.preventDefault();
                                  if (ob_number_whatsapp[es.id]?.length >= 11) {
                                    await on_cancel_item_request_user(
                                      es.id,
                                      ob_number_whatsapp[es.id]
                                    );
                                  }
                                }}
                              >
                                <label>
                                  <p>Insira seu numero de contato whatsapp</p>
                                  <input
                                    onChange={(t) =>
                                      set_ob_number_whatsapp({
                                        ...ob_number_whatsapp,
                                        [es.id]: t.target.value,
                                      })
                                    }
                                    type="text"
                                    placeholder="Não salvamos seu número."
                                  />
                                  <div>
                                    <small>
                                      Entraremos em contato para finalizar
                                      devolução do valor.
                                    </small>
                                  </div>
                                </label>
                                {ob_number_whatsapp[es.id]?.length >= 11 && (
                                  <button type="submit">Cancelar</button>
                                )}
                              </FormCancel>
                            )}
                          </>
                        )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ))}
        </>
      )}
    </WapperContentStep>
  );
};
