import { LayoutComponent } from '@components/Global/Layout';
import { MaxContainerComponent } from '@components/Global/MaxContainer';
import { UserContext } from 'contexts/UserContext';
import { useContext, useState } from 'react';
import { StepAddressComponent } from './stepAddress';
import { StepDataComponent } from './stepData';
import { StepMySalesComponent } from './stepMySales';
import {
  ContentStepSelected,
  ItemStep,
  ListSteps,
  ValueInfoPage,
  ValueTitle,
  WapperContainer,
  WapperContent,
  WapperHeaderStep,
  WapperInfoPage,
} from './styles';

export const UserComponent: React.FC = (): JSX.Element => {
  const { step, setStep } = useContext(UserContext);

  return (
    <MaxContainerComponent>
      <>
        <WapperContainer>
          <WapperInfoPage>
            <ValueInfoPage>Area do usuário</ValueInfoPage>
            <p>
              Obs: Esta sessão está em processo de desenvolvimento. Nossa equipe
              está empenhada em finalizá-la o mais breve possível. Pedimos
              desculpas pelo inconveniente e garantimos que estamos fazendo o
              melhor para garantir a sua experiência e satisfação em nossa
              plataforma.
            </p>
          </WapperInfoPage>

          <WapperContent>
            <WapperHeaderStep>
              <ValueTitle>Meu Perfil</ValueTitle>
              <ListSteps>
                <ItemStep onClick={() => setStep(1)} active={step === 1}>
                  Dados do Usuário
                </ItemStep>
                <ItemStep onClick={() => setStep(2)} active={step === 2}>
                  Endereço
                </ItemStep>
                <ItemStep onClick={() => setStep(3)} active={step === 3}>
                  Meus Pedidos
                </ItemStep>
              </ListSteps>
            </WapperHeaderStep>
            <ContentStepSelected>
              {step === 1 && <StepDataComponent />}
              {step === 2 && <StepAddressComponent />}
              {step === 3 && <StepMySalesComponent />}
            </ContentStepSelected>
          </WapperContent>
        </WapperContainer>
      </>
    </MaxContainerComponent>
  );
};
