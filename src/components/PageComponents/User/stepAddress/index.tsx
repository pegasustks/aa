import { LoadComponent } from "@components/Load";
import cep from "cep-promise";
import { UserContext } from "contexts/UserContext";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useState } from "react";
import useOnclickOutside from "react-cool-onclickoutside";
import { AiOutlineDelete, AiOutlineEdit } from "react-icons/ai";
// import usePlacesAutocomplete, {
//   getGeocode,
//   getLatLng,
// } from "use-places-autocomplete";
import { mask_input_cep } from "utils/mask-cep";
import {
  ButtonSave,
  ButtonTrocarSenha,
  DropSu,
  Error,
  Input,
  Inputs,
  Inputx,
  ItSu,
  LabelText,
  ValueInput,
  ValueItem,
  ValueTitle,
  WapperActionIcon,
  WapperButton,
  WapperContent,
  WapperContentItem,
  WapperContentStep,
  WapperInput,
  WapperItemAddress,
  WapperOther,
  WapperTitleStep,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
  pla?: string;
  disabled?: boolean;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <ValueInput>{title}</ValueInput>
      <Inputs
        disabled={props.disabled}
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
        placeholder={props.pla}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

const RenderInputSelect = ({
  title,
  ...props
}: Omit<input_prop, "onChange" | "disabled" | "type"> & {
  set_value: (e: any) => void;
  set_cep: (e: string) => void;
}): JSX.Element => {
  // const {
  //   ready,
  //   value,
  //   suggestions: { status, data },
  //   setValue,
  //   clearSuggestions,
  // } = usePlacesAutocomplete({
  //   debounce: 300,
  // });

  // const handle_select = useCallback(
  //   ({ description, structured_formatting }: any) =>
  //     () => {
  //       setValue(structured_formatting.main_text, false);
  //       clearSuggestions();
  // 
  //       getGeocode({ address: description }).then((results) => {
  //         const { lat, lng } = getLatLng(results[0]);
  //         props.set_cep("");
  //         props.set_value({
  //           address: structured_formatting.main_text,
  //           geolocation: `${lat}, ${lng}`,
  //           city: "",
  //           state: "",
  //           number: "",
  //           complement: "",
  //           district: "",
  //           cep: "",
  //         });
  //       });
  //     },
  //   [clearSuggestions, props, setValue]
  // );
  // 
  // const ref = useOnclickOutside(() => {
  //   clearSuggestions();
  // });

  return (
    <LabelText>
      <p>{title}</p>
      <Inputx
        value={props.value ?? ''}
        onChange={(e) => {
          // setValue(e.target.value);
          props.set_value({ address: e.target.value });
        }}
        type="text"
        error={!!props?.message_error}
      />
      {/*data.length > 0 && (
        <DropSu>
          {data.map((e) => (
            <ItSu
              title={`${e.structured_formatting.main_text} - ${e.structured_formatting.secondary_text}`}
              key={e.place_id}
              onClick={handle_select(e)}
            >
              <strong>{e.structured_formatting.main_text}</strong>{" "}
              <small>{e.structured_formatting.secondary_text}</small>
            </ItSu>
          ))}
        </DropSu>
          )*/}
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

interface field_prop {
  state: string;
  city: string;
  number: string;
  address: string;
  complement: string;
  district: string;
  geolocation: string;
}

export const StepAddressComponent: React.FC = (): JSX.Element => {
  const {
    address,
    data_errors,
    load_delete,
    load_address,
    load_create_address,
    load_update_address,
    set_data_errors,
    on_update_address_user,
    on_get_address_user,
    on_create_address_user,
  } = useContext(UserContext);

  const { query } = useRouter();

  const [fields_value, set_fields_value] = useState<field_prop>(
    {} as field_prop
  );

  const [cep_va, set_cep] = useState<string>("");

  useEffect(() => {
    if (on_get_address_user) on_get_address_user();
  }, [on_get_address_user]);

  const [edit, set_edit] = useState<boolean>(false);
  const [edit_value, set_edit_value] = useState<
    Partial<field_prop & { cep?: string }>
  >({} as Partial<field_prop>);

  const [load_cep, set_load_cep] = useState<boolean>(false);
  const get_info_cep = useCallback(
    async (e: string) => {
      await set_load_cep(true);
      if (e.length !== 8) {
        set_load_cep(false);
        return;
      }
      const info_cep = await cep(e)
        .catch(() => {
          set_load_cep(false);
        })
        .then((e) => e);

      if (info_cep) {
        set_fields_value({
          ...fields_value,
          city: info_cep.city,
          state: info_cep.state,
        });
        if (edit) {
          set_edit_value({
            ...edit_value,
            city: info_cep.city,
            state: info_cep.state,
            cep: mask_input_cep(e),
          });
        }
      }
      await set_load_cep(false);
    },
    [edit, edit_value, fields_value]
  );

  return (
    <WapperContentStep>
      {load_address ? (
        <LoadComponent />
      ) : (
        <>
          {(!address || edit) && (
            <WapperContent style={{ maxWidth: 500 }}>
              <WapperTitleStep>
                <ValueTitle>
                  {query?.address
                    ? "Para continuar com a compra é nescessario adicionar o seu endereço de entrega"
                    : "Adicionar endereço de entrega"}
                </ValueTitle>
              </WapperTitleStep>

              <WapperOther>
                <WapperInput>
                  <RenderInputSelect
                    set_value={!edit ? set_fields_value : set_edit_value}
                    title="Endereço"
                    value={
                      edit
                        ? edit_value?.address ?? address?.address ?? ""
                        : fields_value?.address ?? ""
                    }
                    set_cep={set_cep}
                  />
                </WapperInput>
              </WapperOther>
              <WapperOther>
                <WapperInput>
                  {renderInput({
                    title: "CEP",
                    onChange: async (e) => {
                      if (!edit) {
                        set_fields_value({
                          ...fields_value,
                          district: "",
                          state: "",
                        } as unknown as field_prop);
                        set_cep(mask_input_cep(e.target.value));
                      }
                      if (edit) {
                        set_edit_value({
                          ...edit_value,
                          city: "",
                          district: "",
                          state: "",
                          cep: mask_input_cep(e.target.value),
                        });
                      }
                      await get_info_cep(e.target.value.replace(/\D/g, ""));
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "cep")
                      );
                    },
                    message_error: data_errors.some((e) => e.field === "cep")
                      ? data_errors?.find((e) => e.field === "cep")?.message
                      : undefined,
                    value: edit
                      ? edit_value?.cep ?? mask_input_cep(address?.cep ?? "")
                      : cep_va || mask_input_cep(address?.cep ?? ""),
                  })}
                </WapperInput>
                <WapperInput>
                  {renderInput({
                    title: "UF",
                    onChange: (e) => {
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "state")
                      );
                      set_fields_value({
                        ...fields_value,
                        state: mask_input_cep(e.target.value),
                      });
                    },
                    message_error: data_errors.some((e) => e.field === "state")
                      ? data_errors?.find((e) => e.field === "state")?.message
                      : undefined,
                    value: edit
                      ? edit_value?.state ?? address?.state ?? ""
                      : fields_value?.state ?? address?.state,
                    disabled: true,
                  })}
                </WapperInput>
              </WapperOther>
              <WapperOther>
                <WapperInput>
                  {renderInput({
                    title: "Cidade",
                    onChange: (e) => {
                      set_fields_value({
                        ...fields_value,
                        city: e.target.value,
                      });
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "city")
                      );
                    },
                    message_error: data_errors.some((e) => e.field === "city")
                      ? data_errors?.find((e) => e.field === "city")?.message
                      : undefined,
                    value: edit
                      ? edit_value?.city ?? address?.city ?? ""
                      : address?.city ?? fields_value?.city,
                    disabled: true,
                  })}
                </WapperInput>
              </WapperOther>
              <WapperOther>
                <WapperInput>
                  {renderInput({
                    title: "Bairro",
                    onChange: (e) => {
                      set_fields_value({
                        ...fields_value,
                        district: e.target.value,
                      });
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "district")
                      );
                      if (edit) {
                        set_edit_value({
                          ...edit_value,
                          district: e.target.value,
                        });
                      }
                    },
                    message_error: data_errors.some(
                      (e) => e.field === "district"
                    )
                      ? data_errors?.find((e) => e.field === "district")
                        ?.message
                      : undefined,
                    value: edit
                      ? edit_value?.district ?? address?.district ?? ""
                      : address?.district ?? fields_value?.district,
                  })}
                </WapperInput>
                <WapperInput>
                  {renderInput({
                    title: "Número",
                    onChange: (e) => {
                      set_fields_value({
                        ...fields_value,
                        number: e.target.value.replace(/\D/g, ""),
                      });
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "number")
                      );
                      if (edit) {
                        set_edit_value({
                          ...edit_value,
                          number: e.target.value,
                        });
                      }
                    },
                    message_error: data_errors.some((e) => e.field === "number")
                      ? data_errors?.find((e) => e.field === "number")?.message
                      : undefined,
                    value: edit
                      ? edit_value?.number ?? address?.number ?? ""
                      : address?.number ?? fields_value?.number,
                  })}
                </WapperInput>
              </WapperOther>
              <WapperOther>
                <WapperInput>
                  {renderInput({
                    title: "Complemento",
                    onChange: (e) => {
                      set_fields_value({
                        ...fields_value,
                        complement: e.target.value,
                      });
                      set_data_errors(
                        data_errors.filter((e) => e.field !== "complement")
                      );
                      if (edit) {
                        set_edit_value({
                          ...edit_value,
                          complement: e.target.value,
                        });
                      }
                    },
                    message_error: data_errors.some(
                      (e) => e.field === "complement"
                    )
                      ? data_errors?.find((e) => e.field === "complement")
                        ?.message
                      : undefined,
                    value: edit
                      ? edit_value?.complement ?? address?.complement ?? ""
                      : address?.complement ?? fields_value?.complement,
                  })}
                </WapperInput>
              </WapperOther>

              <WapperButton>
                <ButtonSave
                  style={{ opacity: load_create_address ? 0.7 : 1 }}
                  onClick={async () => {
                    if (!load_create_address && !load_update_address) {
                      if (edit) {
                        await on_update_address_user(
                          {
                            ...edit_value,
                            cep: edit_value?.cep?.replace(/\D/g, ""),
                          },
                          address?.id ?? ""
                        );
                        await set_edit_value({});
                        await set_edit(false);
                      } else {
                        await on_create_address_user({
                          ...fields_value,
                          cep: cep_va.replace(/\D/g, ""),
                        });
                      }
                    } else {
                      return undefined;
                    }
                  }}
                >
                  {load_create_address
                    ? edit
                      ? "Salvando"
                      : "Adicionando"
                    : edit
                      ? "Salvar"
                      : "Adicionar"}
                </ButtonSave>
              </WapperButton>
            </WapperContent>
          )}
          {address && (
            <WapperContent style={{ opacity: load_delete ? 0.5 : 1 }}>
              <WapperTitleStep>
                <ValueTitle>Meu endereço</ValueTitle>
              </WapperTitleStep>

              <WapperItemAddress>
                <WapperContentItem>
                  <ValueItem>
                    {address.address}, N° {address.number}
                  </ValueItem>
                  <ValueItem>
                    {address.city} - {address.state}
                  </ValueItem>
                  <ValueItem>
                    {address.district} -{" "}
                    {address?.cep ? mask_input_cep(address.cep) : ""}
                  </ValueItem>
                  <ValueItem>{address.complement}</ValueItem>
                </WapperContentItem>
                <WapperContentItem>
                  {/*
                  <WapperActionIcon>
                    <AiOutlineDelete
                      size={20}
                      onClick={() =>
                        !load_delete
                          ? on_delete_address_user(address.id)
                          : undefined
                      }
                    />
                  </WapperActionIcon>
                   */}
                  <WapperActionIcon onClick={() => set_edit(true)}>
                    Editar
                  </WapperActionIcon>
                </WapperContentItem>
              </WapperItemAddress>
            </WapperContent>
          )}
        </>
      )}
    </WapperContentStep>
  );
};
