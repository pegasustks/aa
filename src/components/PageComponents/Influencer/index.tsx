import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { useRouter } from "next/router";
import { DivContainer, DivContent, DivContentMain, DivTop, Item, ListItens } from "./styles";
import { useCallback, useEffect, useState } from "react";
import { ProviderAPI } from "services/api";

export interface result_promoted_sales {
  data: {
    price: string;
    date_created: Date;
  }[]
  percentage: string;
}

export const InfluencerPage: React.FC = (): JSX.Element => {
  const { query } = useRouter();

  const [load, set_load] = useState<boolean>(false as boolean);

  const [influencer, set_influencer] = useState<result_promoted_sales>({} as result_promoted_sales);

  const get_info_influencer = useCallback(async (user: string) => {
    try {
      const { data } = await ProviderAPI.get(`/v1/public/get/promoted-sales-influencer/${user}`);
      set_influencer(data.data);
      set_load(true);
    } catch (error: any) {
      set_load(true);
      alert(`Error ao buscar influencer: @${user}`);
    }
  }, []);

  useEffect(() => {
    if (query?.influencer) {
      get_info_influencer(query.influencer as string);
    }
  }, [get_info_influencer, query.influencer]);

  return (
    <MaxContainerComponent>
      <DivContainer>
        {!load && !influencer?.percentage && (
          <p>Carregando...</p>
        )}
        {load && !influencer?.percentage && (
          <p>Dados para @{query.influencer} não foi encontrado.</p>
        )}
        {load && influencer?.percentage && (
          <DivContent>
            <DivContentMain>
              <DivTop>
                <p>@{query.influencer}</p>
              </DivTop>
              {influencer.data.length > 0 && (
                <ListItens>
                  {influencer.data.map(e => (
                    <Item key={e.date_created + ''}>
                      <span>{new Date(e.date_created).toLocaleString('pt-br')}</span>
                      <strong>R$ {((Number(influencer.percentage) / 100) * Number(e.price)).toFixed(2)}</strong>
                    </Item>
                  ))}
                </ListItens>
              )}
              <div style={{ margin: '15px 0' }}>
                <p>Vazio</p>
              </div>
              <DivTop>
                <div />
                <span>Líquido: <strong>R$ {influencer.data.reduce((ac, ob) => ac + ((Number(influencer.percentage) / 100) * Number(ob.price)), 0).toFixed(2)}</strong></span>
              </DivTop>
            </DivContentMain>
          </DivContent>
        )}
      </DivContainer>
    </MaxContainerComponent>
  );
};
