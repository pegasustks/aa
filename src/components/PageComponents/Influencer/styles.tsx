import styled from "styled-components";

export const DivContainer = styled.div`
  padding: 10px 0;
`;

export const DivContent = styled.div`
  margin-top: 10px;
`;

export const DivTop = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const DivContentMain = styled.div`
  margin: 0 auto;
  max-width: 320px;
  width: 100%;
  background: #fff;
  box-shadow: 0px 12px 25px -6px rgba(0,0,0,0.14);
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const ListItens = styled.ul`
  list-style: none;
  width: 100%;
  margin: 15px 0;
`;
export const Item = styled.li`
  width: 100%;
  display: flex;
  justify-content: space-between;

  strong {
    font-size: 14px;
  }
`;