import styled from "styled-components";

export const WapperContainer = styled.div`
  margin: 50px 0;
  width: 100%;
`;

export const WapperContent = styled.div`
  max-width: 900px;
  width: 100%;
  background: #fff;
  margin: 0 auto;
  box-shadow: 0 0 10px #cdcdcd2f;
  padding: 5px 5px 20px 5px;
  border: 2px solid #d1d1d1ba;
  padding: 30px;
`;

export const WapperInfoTitlePage = styled.div`
  height: 80px;
  display: flex;
  align-items: center;
`;

export const ValueTitlePage = styled.h1`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 28px;
`;

export const WapperInfoDescription = styled.div`
  margin-bottom: 20px;
`;

export const ValueDescrition = styled.p`
  font-family: ${({ theme }) => theme.font.primary};
`;

export const WapperInputs = styled.div``;

export const ItemInput = styled.div`
  margin: 10px 0;
`;

export const WapperButton = styled.div`
  margin-top: 20px;
`;

export const Button = styled.button`
  background: ${({ theme }) => theme.background.white2};
  font-family: sans-serif;
  border: none;
  outline: none;
  cursor: pointer;
  max-width: 200px;
  width: 100%;
  height: 35px;
  color: #fff;
  font-size: 16px;
  font-weight: 600;
  transition: 0.2s ease;

  :hover {
    background: #0253aa;
  }
`;

export const DescritionInfoTerms = styled.div`
  margin-bottom: 10px;
`;
export const Value = styled.p`
  font-size: 15px;
  font-family: sans-serif;
  color: #000;
  strong {
    color: ${({ theme }) => theme.colors.secundary};
  }

  a {
    text-decoration: underline;
    color: #078ad0;
  }
`;
