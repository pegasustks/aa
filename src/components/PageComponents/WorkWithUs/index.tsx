import { SelectNormalComponent } from "@components/SelectNormal";
import Link from "next/link";
import {
  Button,
  DescritionInfoTerms,
  ItemInput,
  Value,
  ValueDescrition,
  ValueTitlePage,
  WapperButton,
  WapperContainer,
  WapperContent,
  WapperInfoDescription,
  WapperInfoTitlePage,
  WapperInputs,
} from "./styles";

export const WorkWithUsComponent: React.FC = (): JSX.Element => {
  return (
    <WapperContainer>
      <WapperContent>
        <WapperInfoTitlePage>
          <ValueTitlePage>Trabalhe Conosco</ValueTitlePage>
        </WapperInfoTitlePage>
        <WapperInfoDescription>
          <ValueDescrition>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Pariatur
            quasi aliquam soluta accusantium molestiae deserunt cum aspernatur
            voluptatum eaque? Dolor omnis amet cumque, voluptatum ab recusandae
            ducimus error a temporibus!
          </ValueDescrition>
        </WapperInfoDescription>

        <WapperInputs>
          <ItemInput>
            <input type="text" name="name" />
          </ItemInput>
          <ItemInput>
            <input type="text" name="tell" />
          </ItemInput>
          <ItemInput>
            <input type="text" name="whatsapp" />
          </ItemInput>
          <ItemInput>
            <input type="text" name="email" />
          </ItemInput>
          <ItemInput>
            <SelectNormalComponent
              not_margin_horizontal
              data={["Whatsapp", "Telefone", "Email"]}
              value="Preferencia de contato"
            />
          </ItemInput>
          <ItemInput>
            <input type="text" name="cidade" />
          </ItemInput>
          <ItemInput>
            <SelectNormalComponent
              not_margin_horizontal
              data={["Gerente de entrega", "Empacotador", "Estoquista"]}
              value="Área de atuação"
            />
          </ItemInput>
          <ItemInput>
            <textarea name="" id="" cols={30} rows={10}>
              fale um pouco dobre voce
            </textarea>
          </ItemInput>
          <ItemInput>
            <input type="text" name="pretensao" />
          </ItemInput>
        </WapperInputs>

        <WapperButton>
          <DescritionInfoTerms>
            <Value>
              Ao clicar em <strong>Enviar</strong>, você aceita todos os{" "}
              <Link href="/">termos de uso de dados</Link>.
            </Value>
          </DescritionInfoTerms>
          <Button>Enviar</Button>
        </WapperButton>
      </WapperContent>
    </WapperContainer>
  );
};
