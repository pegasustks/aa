import { AuthContext } from "contexts/AuthContext";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import {
  Button,
  Center,
  Content,
  Error,
  Footer,
  FormLogin,
  Input,
  LabelText,
  Title,
  WapperContainer,
} from "./styles";

interface input_prop {
  title: string;
  value: string;
  message_error?: string;
  type?: string;
  onChange(e: any): void;
}

const renderInput = ({
  title,
  onChange,
  ...props
}: input_prop): JSX.Element => {
  return (
    <LabelText>
      <p>{title}</p>
      <Input
        type={props.type ?? "text"}
        value={props.value}
        error={!!props?.message_error}
        onChange={onChange}
      />
      {props?.message_error && (
        <Error>
          <p>{props.message_error}</p>
        </Error>
      )}
    </LabelText>
  );
};

interface data_prop {
  password: string;
  whatsapp: string;
}

export const LoginComponent: React.FC = (): JSX.Element => {
  const { on_login, error_data, load_login, set_error_data } =
    useContext(AuthContext);

  const { asPath, query } = useRouter();
  const [data, set_data] = useState<data_prop>({} as data_prop);

  return (
    <WapperContainer>
      <Content>
        <div>
          <Title>Bem vindo de volta</Title>
          <p>Entre com seus dados para acessar sua conta</p>
        </div>
        <FormLogin onSubmit={e => {
          e.preventDefault();
          if (load_login) {
            return undefined;
          }
          on_login(data);
        }} st={load_login}>
          {renderInput({
            title: "WhatsApp",
            onChange: (e) => {
              set_data({ ...data, whatsapp: e.target.value });
              set_error_data(error_data.filter((e) => e.field !== "whatsapp"));
            },
            value: data.whatsapp,
            message_error: error_data?.some((e) => e.field === "whatsapp")
              ? error_data?.find((e) => e.field === "whatsapp")?.message
              : undefined,
          })}
          {renderInput({
            title: "Senha",
            onChange: (e) => {
              set_data({ ...data, password: e.target.value });
              set_error_data(error_data.filter((e) => e.field !== "password"));
            },
            value: data.password,
            message_error: error_data?.some((e) => e.field === "password")
              ? error_data?.find((e) => e.field === "password")?.message
              : undefined,
            type: "password",
          })}
          {/**
           * 
          <p id="pass">
            <Link href="/">Esqueceu a senha?</Link>
          </p>
           */}
          <Center>
            <Button
              type="button"
              onClick={() => (load_login ? undefined : on_login(data, query))}
            >
              Entrar
            </Button>
          </Center>
        </FormLogin>
        <Footer>
          <p>
            <Link href={asPath.replace("/account/login", "/account/register")}>
              Ainda não tem conta? <strong>Criar conta</strong>
            </Link>
          </p>
        </Footer>
      </Content>
    </WapperContainer>
  );
};
