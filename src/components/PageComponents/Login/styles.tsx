import styled from 'styled-components';

export const WapperContainer = styled.div`
  width: 100%;
  min-height: calc(100vh - 195px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  background-color: #fff;
  max-width: 400px;
  width: 100%;

  padding: 18px;
  min-height: 470px;

  display: flex;
  justify-content: space-between;
  flex-direction: column;

  :first-child p {
    margin-top: 5px;
    color: #5a5a5a;
    font-size: 15px;
  }
`;

export const Title = styled.h3``;

export const FormLogin = styled.form<{ st: boolean }>`
  opacity: ${({ st }) => (st ? 0.5 : 1)};

  div :nth-child(2) {
    margin-bottom: 0 !important;
  }

  #pass {
    font-weight: 600;
    text-align: end;
    margin-top: 5px;
    font-size: 14px;
    a {
      color: #07214d !important;

      :hover {
        color: #00050e !important;
      }
    }
  }
`;

export const LabelText = styled.div`
  margin: 7px 0;
  width: 100%;

  p {
    font-size: 14px;
  }
`;

export const Input = styled.input<{ error: boolean }>`
  width: 100%;
  height: 40px;
  margin-top: 5px;
  padding-left: 10px;
  font-size: 16px;
  border: 0;
  background: #f8f8f8;

  border-left: ${({ error }) => (error ? '3px solid red' : '')};
`;

export const Error = styled.div`
  border-left: 3px solid red;
  padding: 8px;

  background: #ffdfdf;
  p {
    color: #ec0000 !important;
  }
`;

export const Center = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
`;

export const Button = styled.button`
  max-width: 200px;
  width: 100%;
  height: 40px;

  border: 0;
  outline: none;
  font-weight: 600;
  letter-spacing: 1px;
  cursor: pointer;
  
  background: ${({ theme }) => theme.background.primary};
  color:  ${({ theme }) => theme.colors.white};
  :hover {
    background:  ${({ theme }) => theme.background.hover.primary};
  }
`;

export const Footer = styled.div`
  p {
    font-size: 14px;

    a {
      color: #5a5a5a !important;
      font-family: sans-serif;
      strong {
        color: #000;
      }
    }
  }
`;
