import styled from "styled-components";

export const DivLoading = styled.div`
  position: fixed;
  width: 100%;
  height: 100vh;
  background: #edf1d6f6;
  z-index: 999999;

  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;


export const WapperProduct = styled.div`
  width: 100%;
`;

export const WapperOfferProductItem = styled.div`
  width: 100%;
`;

export const SectionCategories = styled.section`
  background-color: #fff;
  margin: 40px 0;
  box-shadow: 0px 1px 10px rgba(88, 94, 70, 0.137);
`;
export const TitleSection = styled.h3`
  padding: 21px 0 0 21px;
  text-transform: uppercase;
  color: #787878;
  font-size: 16px;
`;
export const ListCategories = styled.ul`
  margin-top: 29px;
  display: grid;
  list-style: none;

  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr;

  #prod {
    max-width: 100%;
  }

  @media screen and (max-width: 1034px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
  }
  @media screen and (max-width: 888px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  }
  @media screen and (max-width: 743px) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }
  @media screen and (max-width: 600px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }
  @media screen and (max-width: 456px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
  @media screen and (max-width: 336px) {
    grid-template-columns: 1fr 1fr;
    column-gap: 5px;
  }
`;
export const ItemCategorie = styled.li`
  a {
    width: 100%;
    padding: 13px 8px;
    border: 0.5px solid #F3F3F3;
    display: grid;
    grid-template-rows: 1fr 35px;
    place-items: center;
    row-gap: 10px;
    max-width: 145px;
    color: #787878;
    transition: .2s ease;

    :hover {
      color: #1e1e1e;
      background-color: #fcfcfc;
      border-color: ${({ theme }) => theme.background.primary};
    }
  }
`;
export const DivName = styled.div`
  text-align: center;
  height: 100%;
`;


export const SectionStick = styled.div`
`;
export const HeaderSectionStick = styled.div`
  margin: 40px 0;
  background-color: #fff;
  padding: 21px;
  border-bottom: 3px solid ${({ theme }) => theme.background.primary};
  position: sticky;
  top: 136.19px;
  color: ${({ theme }) => theme.colors.primary};
  font-weight: 400;
  z-index: 1;

  @media screen and (max-width: 405px) {
    top: 99px;  
  }
`;
export const PrintItens = styled.div``;
