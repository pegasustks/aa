/* eslint-disable @next/next/no-img-element */
import { MaxContainerComponent } from "@components/Global/MaxContainer";
import { GridProducts } from "@components/Global/styles";
import { ProductItem } from "@components/Product";
import { SectionComponent } from "@components/Section";
import { getCookie, removeCookies, setCookie } from "cookies-next";
import { useCallback, useContext, useEffect, useState } from "react";
import { ProviderAPI } from "services/api";
import { WapperProduct, WapperOfferProductItem, DivName, ItemCategorie, ListCategories, SectionCategories, TitleSection, HeaderSectionStick, PrintItens, SectionStick, DivLoading } from "./styles";
import { AdvertisingPrincipalPageHome } from "@components/Advertising/Principal";
import Link from "next/link";
import { LogoComponent } from "@components/Logo";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";

interface Attachment_I {
  title: string;
  id: string;
  product_attachments: {
    id: string;
    product2: {
      title: string;
      id: string;
      sales: number | null;
      main_price1: number;
      main_price2: string | null;
      main_image: string;
    };
  }[];
}

interface CategoryHome_I {
  id: string;
  name: string;
  image: string;
  category_id: string;
  subcategory_id: string;
  subcategory2_id: string;
}

export const HomePage: React.FC = (): JSX.Element => {
  const { set_load_click } = useContext(TotalLoadingDataContext);

  const [last_seen, set_last_seen] = useState<any[]>([]);

  const [attachments, setAttachments] = useState<Attachment_I[]>(
    [] as Attachment_I[]
  );

  const [category_home, set_category_home] = useState<CategoryHome_I[]>([] as CategoryHome_I[]);

  useEffect(() => {
    const d = getCookie("last_seen");
    if (d) {
      const list_last = JSON.parse(String(d));
      (async () => {
        const dataa = await Promise.all(
          list_last.map(async (id: string) => {
            const { data } = await ProviderAPI.get(
              `/v1/public/get/product2-one/${id}`
            );
            if (data?.data?.message === "Empty." || data?.data?.message === "Error dataBase") {
              const newa = list_last.filter((e: any) => e !== id);
              removeCookies("last_seen");
              setCookie("last_seen", newa);
              return undefined;
            }
            return data?.data;
          })
        );
        const next_data = dataa.filter((e) => e !== undefined);
        set_last_seen(next_data);
      })();
    }
  }, []);

  const getAttachments = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get("/v1/public/get/attachments");
      return setAttachments(data.data);
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, []);

  const get_categories_home = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get("/v1/public/get/categories-home");
      set_category_home(data.data);
    } catch (error: any) {
      return null;
    }
  }, []);

  useEffect(() => {
    get_categories_home();
    getAttachments();
    set_load_click(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <AdvertisingPrincipalPageHome />

      <MaxContainerComponent>
        <>
          {category_home.length ? (
            <SectionCategories>
              <TitleSection>Categorias</TitleSection>
              <ListCategories>
                {category_home.map(cat => (
                  <ItemCategorie key={cat.id}>
                    <Link prefetch={false} href={`/categoria?category=${cat.category_id}&subcategory=${cat.subcategory_id}&subcategory2=${cat.subcategory2_id}&subcategory3=${cat.id}`}>
                      <img src={`${ProviderAPI.defaults.baseURL}/v1/images/categories/${cat.image}`} style={{ maxWidth: "100%", height: 'auto' }} alt={cat.name} />
                      <DivName>
                        <span>{cat.name}</span>
                      </DivName>
                    </Link>
                  </ItemCategorie>
                ))}
              </ListCategories>
            </SectionCategories>
          ) : undefined}

          {last_seen?.length > 0 && (
            <div>
              <HeaderSectionStick style={{ marginBottom: 0 }}>
                <span>Histórico de visualização</span>
              </HeaderSectionStick>
              <SectionStick style={{ marginTop: 10 }}>
                <PrintItens>
                  <GridProducts>
                    {last_seen?.map(
                      (item) =>
                        item && (
                          <WapperOfferProductItem key={item.id}>
                            <ProductItem {...item} />
                          </WapperOfferProductItem>
                        )
                    )}
                  </GridProducts>
                </PrintItens>
              </SectionStick>
            </div>
          )}

          {attachments.map((att) => (
            <div key={att.id}>
              <HeaderSectionStick style={{ marginBottom: 0 }}>
                <span>{att.title}</span>
              </HeaderSectionStick>
              <SectionStick style={{ marginTop: 10 }}>
                <GridProducts>
                  {att.product_attachments.map((item) => (
                    <WapperOfferProductItem key={item.id}>
                      <ProductItem
                        id={item.product2.id}
                        main_image={item.product2.main_image}
                        main_price1={String(item.product2.main_price1)}
                        main_price2={item.product2.main_price2}
                        sales={item.product2.sales}
                        title={item.product2.title}
                      />
                    </WapperOfferProductItem>
                  ))}
                </GridProducts>
              </SectionStick>
            </div>
          ))}
        </>
      </MaxContainerComponent>
    </>
  );
};
