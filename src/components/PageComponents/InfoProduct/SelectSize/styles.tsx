import styled from 'styled-components';

export const WapperContainer = styled.div`
  margin-top: 10px;
  display: flex;
`;

export const ButtonSelect = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const WapperOther = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  width: 100%;
  position: relative;
  margin-top: 5px;
`;

export const Value = styled.span`
  font-family: sans-serif;
  letter-spacing: 0.4px;
  font-size: 15px;
  font-weight: 600;
`;

export const ValueQnt = styled.span`
  font-family: sans-serif;
  font-size: 17px;
  letter-spacing: 0.4px;

  margin-right: 10px;
`;

export const WapperSelect = styled.div<{ active?: boolean }>`
  min-width: 35px;
  height: 35px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  margin: 0 3px;
  padding: 0 4px;

  :first-child {
    margin-left: 0;
  }
  :last-child {
    margin-right: 0;
  }

  background: ${({ active }) => (active ? '#e9e9e9' : '#fff')};
  border: ${({ active }) => (active ? '1px solid #000' : '0')};
`;

export const OptionColor = styled.div<{ color: string; active?: boolean }>`
  opacity: ${({ active }) => (active ? 1 : 0.7)};
  :hover {
    opacity: 1;
  }
  width: 27px;
  height: 27px;
  border-radius: 50%;
  background-color: ${({ color }) => color};

  transition: 0.3s ease;
`;
