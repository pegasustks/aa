import { InfoProductContext } from "contexts/InfoProductContext";
import { useContext, useState } from "react";
import {
  ButtonSelect,
  OptionColor,
  Value,
  WapperContainer,
  WapperOther,
  WapperSelect,
} from "./styles";

interface SelectSize {
  data: {
    value: string;
    sizes_fashion_id: string;
  }[];
}

export const SelectSizeComponent: React.FC<SelectSize> = ({
  data,
}): JSX.Element => {
  const { data_request, handle_value_request, on_select_size_product } =
    useContext(InfoProductContext);

  return (
    <WapperContainer>
      <ButtonSelect>
        <Value>Tamanho:</Value>
        <WapperOther>
          {data.map((item) => (
            <WapperSelect
              key={item.sizes_fashion_id}
              active={data_request?.sizes_fashion_id === item.sizes_fashion_id}
              onClick={() => {
                handle_value_request("sizes_fashion_id", item.sizes_fashion_id);
                on_select_size_product(item.sizes_fashion_id);
              }}
            >
              {item.value}
            </WapperSelect>
          ))}
        </WapperOther>
      </ButtonSelect>
    </WapperContainer>
  );
};
