import { ProductItem } from "@components/Product";
import { SectionComponent } from "@components/Section";
import { fifteenProduct } from "data/15product";
import {
  WapperContainer,
  WapperItemsProduct,
  WapperOfferProductItem,
} from "./styles";

export const OtherProductsComponent: React.FC = (): JSX.Element => {
  return (
    <WapperContainer>
      <SectionComponent title={{ value: "Talvez você goste" }}>
        <WapperItemsProduct>
          {fifteenProduct.map((item) => (
            <WapperOfferProductItem key={item.id}>
              <ProductItem
                id="aa"
                main_image="aa"
                main_price1="10"
                main_price2="20"
                title="T"
              />
            </WapperOfferProductItem>
          ))}
        </WapperItemsProduct>
      </SectionComponent>
    </WapperContainer>
  );
};
