import styled from 'styled-components';

export const WapperContainer = styled.div`
  margin: 50px 0;
`;

export const WapperItemsProduct = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 280px);
  grid-column-gap: 10px;
  grid-row-gap: 25px;
  margin-top: 10px;
  width: 100%;
`;

export const WapperOfferProductItem = styled.div`
  width: 100%;
`;
