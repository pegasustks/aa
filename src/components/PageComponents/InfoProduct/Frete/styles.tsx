import styled, { keyframes, css } from "styled-components";

export const DivFrete = styled.div`
  #title_ {
    color: ${({ theme }) => theme.colors.black2};
    font-size: 16px;
  }
  #info_ {
    color:  ${({ theme }) => theme.colors.grey};
  }
`;

export const MessageError = styled.div<{ open: boolean }>`
  opacity: ${({ open }) => (open ? 1 : 0)};
  transition: 0.3s ease;
  width: 100%;
  cursor: default;
  height: 16px;

  strong {
    color: red;
    font-size: 15px;
  }
`;

export const BoxCep = styled.div`
  margin-top: 3px;
  height: 40px;
  width: 100%;
  display: flex;
  column-gap: 5px;
  align-items: flex-end;
  button {
    height: 40px;
    width: 45px;
    border-radius: 0;
    outline: none;
    border: 1px solid #000;
    background:  ${({ theme }) => theme.background.white};
    font-weight: bold;
    cursor: pointer;

    :hover {
      background:  ${({ theme }) => theme.background.hover.white2};
    }
  }

  input {
    margin-top: 10px;
    width: 100%;
    height: 40px;
    padding-left: 10px;
    font-size: 15px;
    ::placeholder {
      letter-spacing: 1px;
    }
  }
`;

export const DivInput = styled.div`
  position: relative;
  width: 100%;
`;

export const animation = keyframes`
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(360deg);
  }
`;

export const Load = styled.div<{ load: boolean }>`
  position: absolute;
  display: none;
  ${({ load }) =>
    load &&
    css`
      animation: ${animation} linear infinite 1s;
      display: block;
    `}
  top: calc(50% - 3px);
  transform: rotateZ(0deg);
  right: 10px;
  width: 16px;
  height: 16px;
`;

// resultshipping

export const ResultShipping = styled.div`
  border: 1px solid #c3c3c3;
  padding: 12px;
`;

export const PrintCity = styled.div`
  display: flex;
  align-items: center;
  column-gap: 5px;
  border-bottom: 1px solid #c3c3c3;
  padding-bottom: 12px;
  margin-bottom: 12px;
`;

export const DivValuePrice = styled.div`
  #content_dv {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  #text_ent {
    font-size: 13px;
    letter-spacing: 1px;
    color:  ${({ theme }) => theme.colors.grey};
  }
  #text_v {
    display: flex;
    align-items: center;
    column-gap: 5px;
  }
`;

export const ValueFre = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  s {
    font-size: 15px;
  }
  strong {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 17px;
  }
`;

export const DivAlert = styled.div`
  margin-top: 18px;
`;

export const DivTexts = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 10px;

  span {
    color: ${({ theme }) => theme.colors.grey};
    font-size: 14px;
  }
`;

export const DivAl = styled.div`
  display: flex;
  align-items: center;
  column-gap: 5px;
`;
