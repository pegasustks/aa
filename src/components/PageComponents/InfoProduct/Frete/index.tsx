import cep from "cep-promise"
import { AuthContext } from "contexts/AuthContext"
import { InfoProductContext } from "contexts/InfoProductContext"
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext"
import { setCookie } from "cookies-next"
import { useCallback, useContext, useMemo, useState } from "react"
import { FaSpinner } from "react-icons/fa"
import { MdOutlineLocationOn } from "react-icons/md"
import { TbAlertTriangle, TbTruck } from 'react-icons/tb';
import { mask_input_cep } from "utils/mask-cep"
import { BoxCep, DivAl, DivAlert, DivFrete, DivInput, DivTexts, DivValuePrice, Load, MessageError, PrintCity, ResultShipping, ValueFre } from "./styles"
import { useTheme } from "styled-components"

const meses: { [x: number]: string } = {
  1: 'janeiro',
  2: 'fevereiro',
  3: 'março',
  4: 'abril',
  5: 'maio',
  6: 'junho',
  7: 'julho',
  8: 'agosto',
  9: 'setembro',
  10: 'outubro',
  11: 'novembro',
  12: 'dezembro'
}

export const FreteComponent: React.FC = (): JSX.Element => {
  const { data_frete, on_reload_frete_product } = useContext(InfoProductContext);
  const { set_city, city } = useContext(TotalLoadingDataContext);
  const { user_data, set_user_data } = useContext(AuthContext);

  const [cep_value, set_cep_value] = useState<string>("");
  const [load, set_load] = useState<boolean>(false as boolean);
  const [message_error, set_message_error] = useState<string | null>(null);

  const { colors } = useTheme();

  const get_info_cep = useCallback(async () => {
    set_load(true);
    if (cep_value.length !== 9) {
      set_message_error("CEP incorreto!");
      setTimeout(() => {
        set_message_error(null);
      }, 1200);
      set_load(false);
      return;
    }
    const info_cep = await cep(cep_value)
      .catch(() => {
        set_message_error("CEP incorreto!");
        set_cep_value('');
        setTimeout(() => {
          set_message_error(null);
        }, 1200);
        set_load(false);
      })
      .then((e) => e);

    if (info_cep) {
      const data = JSON.stringify(info_cep);
      setCookie("SID-site-location", data, {
        secure: true,
        path: "/",
        sameSite: "strict",
        maxAge: 2147483647
      });
      set_city(`${info_cep.state} - ${info_cep.city}`);
      await on_reload_frete_product(info_cep.cep);
      set_cep_value('');
      set_user_data({ ...user_data, cep: cep_value, city: info_cep.city });
    } else {
      set_cep_value('');
      set_message_error("CEP incorreto!");
    }
    set_load(false);
  }, [cep_value, set_city, on_reload_frete_product, set_user_data, user_data]);

  const getDay = useMemo(() => {
    if (data_frete?.prazoEnt) {
      const date = new Date();
      date.setDate(date.getDate() + data_frete?.prazoEnt)
      return { dia: date.getDate(), mes: meses[date.getMonth() + 1] };
    }

    return undefined;
  }, [data_frete?.prazoEnt]);

  return (
    <>
      <DivFrete>
        <span id="title_">
          Calcular frete e prazo{' '}
          {!data_frete && (
            <strong style={{ color: 'red' }}>
              Serviço de envio indisponível
            </strong>
          )}
        </span>
        <BoxCep>
          <DivInput>
            <input
              onChange={(e) => set_cep_value(mask_input_cep(e.currentTarget.value))}
              value={cep_value}
              type="text"
              placeholder="Digite o CEP"
            />
            <Load load={load}>
              <FaSpinner />
            </Load>
          </DivInput>
          <button onClick={get_info_cep} type="button" className="next">
            OK
          </button>
        </BoxCep>
        <MessageError open={!!message_error}>
          <strong>{message_error}</strong>
        </MessageError>
        {!user_data?.cep && (
          <span id="info_">
            *Calcule o Frete para adicionar produtos ao carrinho de compras.
          </span>
        )}
        {data_frete && (
          <ResultShipping>
            <PrintCity>
              <MdOutlineLocationOn size={25} />
              <span>{user_data?.city ?? city}</span>
            </PrintCity>
            <DivValuePrice>
              <div id="content_dv">
                <div id="text_v">
                  <TbTruck size={25} />
                  <span>
                    <strong>receba até</strong>
                    {` ${getDay?.dia} de ${getDay?.mes}`}
                  </span>
                </div>
                <ValueFre>
                  {data_frete?.price.de === data_frete?.price.por && (
                    <>
                      <strong>R$ {data_frete?.price.por.toFixed(2)}</strong>
                    </>
                  )}
                  {data_frete?.price.de > data_frete?.price.por && (
                    <>
                      <s style={{ color: colors.grey }}>R$ {data_frete?.price.de.toFixed(2)}</s>
                      <strong>R$ {data_frete?.price.por.toFixed(2)}</strong>
                    </>
                  )}
                  {data_frete?.price.por > data_frete?.price.de && (
                    <strong>R$ {data_frete?.price.por.toFixed(2)}</strong>
                  )}
                </ValueFre>
              </div>
              <span id="text_ent">Entrega rápida {data_frete?.service === 'kangu' && 'SEDEX'}</span>
            </DivValuePrice>
            {data_frete.alertas?.length > 0 && (
              <DivAlert>
                <DivAl style={{ borderBottom: 0 }}>
                  <TbAlertTriangle size={24} />
                  <strong>Alerta{data_frete.alertas.length > 1 ? 's!' : '!'}</strong>
                </DivAl>
                <DivTexts>
                  {data_frete.alertas.map((tx, a) => (
                    <>
                      <div key={a}>
                        <span>
                          {tx}
                        </span>
                      </div>
                    </>
                  ))}
                </DivTexts>
              </DivAlert>
            )}
          </ResultShipping>
        )}
      </DivFrete>
    </>
  )
}