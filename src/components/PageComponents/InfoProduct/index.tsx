/* eslint-disable @next/next/no-img-element */
import { BackRouterComponent } from "@components/BackRouter";
import { InfoProductContext } from "contexts/InfoProductContext";
import { useContext, useEffect, useRef, useState } from "react";
import { BiMinus, BiPlus } from "react-icons/bi";
import { AuthContext } from "contexts/AuthContext";
import { useRouter } from "next/router";
import Image from "next/image";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import { getCookie, setCookie } from "cookies-next";
import { DescriptionInfoProductComponent } from "./Description";
import { SelectColorComponent } from "./SelectColor";
import { SelectAmountComponent } from "./SelectQuant";
import { SelectSizeComponent } from "./SelectSize";
import { ShareComponent } from "./Share";
import {
  ButtonAction,
  ContentMeasu,
  DivFeatures,
  DivFrete,
  InfoMensu,
  // ButtonSeeOptions,
  ItemImage,
  ItemIs,
  ListImages,
  ListIs,
  LoadFixed,
  TitleProduct,
  ValueActualPrice,
  ValueInfoTop,
  ValueOldPrice,
  ValuePorcent,
  ValueTop,
  WapperActionUpProduct,
  WapperButtonActionsProduct,
  WapperContainer,
  WapperContentInfos,
  WapperContentInfoTop,
  WapperImage,
  WapperImagesProduct,
  WapperInfoOfProduct,
  WapperInfoPlusProduct,
  WapperInfoProduct,
  WapperInfoTop,
  WapperPrices,
  WapperSelectActionProduct,
  WapperValueActualPrice,
} from "./styles";
import { ProviderAPI } from "services/api";
import { FreteComponent } from "./Frete";
import { AdvertisingSimpleComponent } from "@components/Advertising/Simple";

interface prop_values_correios {
  valor?: string | null;
  prazoEntrega?: string | null;
  message?: string;
}

export const InfoProductComponent: React.FC = (): JSX.Element => {
  const {
    sizes,
    prices,
    colors,
    images,
    load_change,
    sizes_values,
    data_request,
    data_features,
    colapse_feature,
    load_change_size,
    colapse_measurements,
    categories_of_product,
    set_colapse_feature,
    set_colapse_measurements,
    on_add_product_in_cart_sale,
    product,
  } = useContext(InfoProductContext);
  const { exist, city, location_user_logout, set_load_click } = useContext(TotalLoadingDataContext);
  const { user_data } = useContext(AuthContext);
  const { push } = useRouter();
  const reff = useRef<any>(null);

  const [imageActive, setImageActive] = useState<string | null>(null);

  useEffect(() => {
    if (product) {
      const d = getCookie("last_seen");
      if (d !== undefined && product?.id) {
        const last_seen: string[] = JSON.parse(String(d));
        if (last_seen.length >= 8) {
          const next_list = last_seen.slice(0, 8);
          const new_last_seen_list = [...new Set(next_list)];
          new_last_seen_list.pop();
          new_last_seen_list.unshift(product.id);
          setCookie("last_seen", JSON.stringify(new_last_seen_list), {
            secure: true,
            path: "/",
            sameSite: "strict",
            maxAge: 60 * 60 * 24 * 300,
          });
          return;
        }
        if (!last_seen.find((e: any) => e === product.id)) {
          setCookie(
            "last_seen",
            JSON.stringify([product.id, ...last_seen]),
            {
              secure: true,
              path: "/",
              sameSite: "strict",
              maxAge: 60 * 60 * 24 * 300,
            }
          );
          return;
        }
        const new_last_seen_list = [...new Set(last_seen)];
        const state_filter = new_last_seen_list.filter(
          (e) => e !== product.id
        );
        state_filter.unshift(product.id);
        setCookie("last_seen", JSON.stringify(state_filter), {
          secure: true,
          path: "/",
          sameSite: "strict",
          maxAge: 60 * 60 * 24 * 300,
        });
      }
      if (!d && product?.id) {
        setCookie("last_seen", JSON.stringify([product.id]), {
          secure: true,
          path: "/",
          sameSite: "strict",
          maxAge: 60 * 60 * 24 * 300,
        });
      }
    }
  }, [product]);

  useEffect(() => {
    setImageActive(images ? images[0] : null);
  }, [images]);

  const [link, set_link] = useState<string>("" as string);

  useEffect(() => {
    set_load_click(false);
    set_link(window.location.href.toString());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {!product && <p>Produto não encontrado</p>}
      {product && (
        <WapperContainer>
          {categories_of_product?.category && (
            <BackRouterComponent
              value={[
                {
                  id: "1",
                  value: categories_of_product.category,
                },
                {
                  id: "2",
                  value: categories_of_product.sub_category1,
                },
                {
                  id: "3",
                  value: categories_of_product.sub_category2,
                },
                {
                  id: "4",
                  value: categories_of_product.sub_category3,
                },
                {
                  id: "5",
                  value: product.title,
                },
              ]}
            />
          )}

          <WapperInfoProduct>
            {/*!loadProduct && (
              <LoadFixed>
                <LoadComponent />
              </LoadFixed>
            )*/}
            <WapperContentInfoTop>
              <WapperImagesProduct>
                <ListImages>
                  {images!.length > 1 &&
                    images?.map((item) => (
                      <ItemImage
                        key={item}
                        active={imageActive === item}
                        onClick={() => setImageActive(item)}
                      >
                        <Image
                          // src={`http://34.83.111.46:6868/v1/images/products/${item}`}
                          alt="imagem"
                          width={67.2}
                          height={76}
                          src={`${ProviderAPI.defaults.baseURL}/v1/images/products/${item}`}
                        />
                      </ItemImage>
                    ))}
                </ListImages>
                <WapperImage ref={reff}>
                  <img
                    src={`${ProviderAPI.defaults.baseURL}/v1/images/products/${imageActive}`}
                    alt="imagem"
                    style={{ height: "auto", maxWidth: "100%" }}
                  />
                </WapperImage>
              </WapperImagesProduct>
              <WapperInfoOfProduct>
                <div>
                  {product?.sales && Number(product?.sales) > 0 ? (
                    <WapperInfoTop>
                      <ValueInfoTop>
                        {Number(product.sales) > 1 ? 'vendidos' : 'vendido'}
                      </ValueInfoTop>
                    </WapperInfoTop>
                  ) : undefined}
                  <WapperContentInfos>
                    <TitleProduct>{product?.title}</TitleProduct>
                    <WapperPrices change={load_change || load_change_size}>
                      <div>
                        <ValueOldPrice>
                          {prices?.price_unit2 ? (
                            <>
                              <span>R$</span>{" "}
                              {Number(prices?.price_unit2).toFixed(2)}
                            </>
                          ) : (
                            ""
                          )}
                        </ValueOldPrice>
                      </div>
                      <WapperValueActualPrice>
                        <ValueActualPrice>
                          {prices?.price_unit && (
                            <>
                              R${" "}
                              {Number(prices?.price_unit).toFixed(2)}
                            </>
                          )}
                        </ValueActualPrice>
                        {prices?.price_unit2 &&
                          prices?.price_unit < prices?.price_unit2 && (
                            <ValuePorcent>
                              {String(
                                ((Number(prices?.price_unit2) -
                                  Number(prices?.price_unit)) /
                                  Number(prices?.price_unit2)) *
                                100
                              ).slice(0, 2)}
                              {"% "}
                              <span>OFF</span>
                            </ValuePorcent>
                          )}
                      </WapperValueActualPrice>
                    </WapperPrices>
                  </WapperContentInfos>
                </div>

                <FreteComponent />

                <WapperSelectActionProduct>
                  <WapperActionUpProduct>
                    <SelectAmountComponent repository={5} />

                    <SelectColorComponent data={colors!} />
                  </WapperActionUpProduct>
                  <div style={{ opacity: load_change ? 0.5 : 1 }}>
                    <SelectSizeComponent data={sizes_values!} />
                  </div>
                </WapperSelectActionProduct>

                <div>
                  <WapperButtonActionsProduct>
                    {user_data?.cep && (
                      <ButtonAction
                        in_={!!exist}
                        onClick={
                          user_data?.cpf
                            ? !exist
                              ? on_add_product_in_cart_sale
                              : undefined
                            : () =>
                              push(
                                `/account/login?p=${product.id}&a=${data_request!.amount}&c=${data_request!.color_id}&s=${data_request!.sizes_fashion_id}&it=${product.it}`
                              )
                        }
                        background="#222222"
                        hover="#0f0f0f"
                      >
                        {exist
                          ? "Produto no carrinho"
                          : "Adicionar ao Carrinho"}
                      </ButtonAction>
                    )}
                    {!user_data?.cep && user_data?.cpf && (
                      <ButtonAction
                        in_={!!exist}
                        onClick={() =>
                          push(
                            `http://localhost:3000/account/user?address=1&it=${product.it ?? ''}&prod=${product.id}`
                          )
                        }
                        background="#4d4d4d"
                        hover="#2e2e2e"
                      >
                        Adicionar Endereço
                      </ButtonAction>
                    )}
                  </WapperButtonActionsProduct>
                </div>

                <div style={{ marginBottom: 5 }}>
                  <DivFeatures change={false}>
                    <ValueTop
                      onClick={() => set_colapse_feature(!colapse_feature)}
                    >
                      <p>Caracterisicas do produto</p>
                      {colapse_feature ? <BiMinus /> : <BiPlus />}
                    </ValueTop>
                    {colapse_feature && (
                      <ListIs>
                        {data_features!.map((item) => (
                          <ItemIs key={item.name}>
                            <p>{item.name}:</p>
                            <span>{item.value}</span>
                          </ItemIs>
                        ))}
                      </ListIs>
                    )}
                  </DivFeatures>
                  <DivFeatures change={load_change}>
                    <ValueTop
                      onClick={() =>
                        set_colapse_measurements(!colapse_measurements)
                      }
                    >
                      <p>Medições do produto</p>
                      {colapse_measurements ? <BiMinus /> : <BiPlus />}
                    </ValueTop>
                    {colapse_measurements && (
                      <>
                        <ContentMeasu>
                          <table cellSpacing="0" cellPadding="0">
                            <tr>
                              {sizes?.header?.map((e) => (
                                <th key={e}>{e}</th>
                              ))}
                            </tr>
                            {sizes?.body?.map((e) => (
                              <tr key={e[0]}>
                                {e.map((es, index) => (
                                  <td key={`${es}-${String(index)}`}>{es}</td>
                                ))}
                              </tr>
                            ))}
                          </table>
                        </ContentMeasu>
                        <InfoMensu>
                          <p>
                            Dados obtidos com a medição manual do produto, as
                            medições podem mudar 2-3 cm.
                          </p>
                        </InfoMensu>
                      </>
                    )}
                  </DivFeatures>
                </div>

                <ShareComponent link={link} />
              </WapperInfoOfProduct>
            </WapperContentInfoTop>

            <WapperInfoPlusProduct>
              {product?.resum && (
                <DescriptionInfoProductComponent
                  description={product?.resum}
                />
              )}
            </WapperInfoPlusProduct>
          </WapperInfoProduct>
        </WapperContainer>
      )}
      <AdvertisingSimpleComponent />
    </>
  );
};
