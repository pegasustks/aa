import { InfoProductContext } from 'contexts/InfoProductContext';
import { useContext, useState } from 'react';
import {
  ButtonSelect,
  OptionColor,
  Value,
  WapperContainer,
  WapperOther,
  WapperSelect,
} from './styles';

interface SelectColor {
  data: {
    title: string;
    id: string;
    value: string;
  }[];
}

export const SelectColorComponent: React.FC<SelectColor> = ({
  data,
}): JSX.Element => {
  const { data_request, handle_value_request, on_select_color_product } =
    useContext(InfoProductContext);

  return (
    <WapperContainer>
      <ButtonSelect>
        <Value>Cor:</Value>
        <WapperOther>
          {data.map(item => (
            <WapperSelect
              active={data_request?.color_id === item.id}
              key={item.id}
              onClick={() => {
                handle_value_request('color_id', item.id);
                on_select_color_product(item.id);
              }}
            >
              <OptionColor
                active={data_request?.color_id === item.id}
                color={item.value}
                title={item.title}
                key={item.id}
              />
            </WapperSelect>
          ))}
        </WapperOther>
      </ButtonSelect>
    </WapperContainer>
  );
};
