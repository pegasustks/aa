import styled from "styled-components";

export const WapperContainer = styled.div`
  padding-bottom: 40px;
  margin-bottom: 40px;
  margin-top: 40px;
  border-bottom: 2px solid #f3f3f3;

  :last-child {
    border-bottom: 0;
    padding-bottom: 20px;
    margin-bottom: 0;
  }
`;

export const WapperInfoTopPlus = styled.div<{ padding?: boolean }>`
  padding-left: ${({ padding }) => padding && "20px"};
`;

export const ValueTopInfoInfoPlus = styled.h2`
  font-family: ${({ theme }) => theme.font.primary};
  font-weight: 300;
  font-size: 25px;
  line-height: 35px;
`;

export const DescriptionProduct = styled.div`
  margin-top: 10px;
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 16px;
  letter-spacing: 0.2px;

  h1 {
    margin-bottom: 20px;
    font-size: 19px;
  }
  h2 {
    margin: 15px 0;
    font-size: 18px;
  }
  h3 {
    margin: 15px 0;
    font-size: 17px;
  }
  p {
    line-height: 20px;
  }
`;
