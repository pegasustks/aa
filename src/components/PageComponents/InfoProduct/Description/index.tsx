import { SectionComponent } from "@components/Section";
import { DescriptionProduct } from "./styles";
import ReactMarkdown from "react-markdown";

interface Description_I {
  description: string;
}

export const DescriptionInfoProductComponent: React.FC<Description_I> = ({
  description,
}): JSX.Element => {
  return (
    <SectionComponent
      title={{
        style: { size: 26 },
        value: "Descrição",
      }}
    >
      <DescriptionProduct>
        <ReactMarkdown
          components={{
            ul: ({ node, ...props }) => <ul style={{ paddingLeft: 20 }} {...props} />,
            p: ({ node, ...prop }) => (
              <>
                <p style={{ margin: '10px 0' }} {...prop} />
              </>
            )
          }}
        >{description}</ReactMarkdown>
      </DescriptionProduct>
    </SectionComponent>
  );
};
