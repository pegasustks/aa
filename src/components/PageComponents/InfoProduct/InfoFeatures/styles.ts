import styled from 'styled-components';

export const WapperContainer = styled.div<{ padding?: boolean }>`
  padding-bottom: 40px;
  margin-bottom: 40px;
  margin-top: 40px;
  border-bottom: 2px solid #f3f3f3;

  :last-child {
    border-bottom: 0;
    padding-bottom: 20px;
    margin-bottom: 0;
  }
`;

export const WapperInfoTopPlus = styled.div<{ padding?: boolean }>`
  padding-left: ${({ padding }) => padding && '20px'};
`;
export const ValueTopInfoInfoPlus = styled.h2`
  font-family: ${({ theme }) => theme.font.primary};
  font-weight: 300;
  font-size: 25px;
  line-height: 35px;
`;
export const WapperCardsInfos = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-top: 5px;
`;

export const ListInformationProductPlus = styled.ul`
  max-width: 300px;
  width: 100%;
  margin: 15px 0;
`;

export const ItemInformation = styled.li`
  display: flex;
  flex-direction: column;
`;

export const ValueInformation = styled.span`
  font-family: ${({ theme }) => theme.font.primary};
  font-size: 20px;
`;

export const WapperAllValueOfInformation = styled.div`
  margin-top: 8px;

  max-width: 300px;
  width: 100%;
  background: #f8f8f8;
  border-radius: 5px;
  overflow: hidden;
`;

export const WapperValues = styled.div`
  margin: 5px 0;
  padding: 15px 15px;
  :nth-child(odd) {
    background: #f0f0f0;
  }

  font-family: sans-serif;
  display: flex;
  justify-content: space-between;

  :first-child {
    margin-top: 0;
  }
  :last-child {
    margin-bottom: 0;
  }
`;

export const ResponseValueInfo = styled.span`
  color: #444;
`;
export const ValueInfoContent = styled.span`
  color: #666;
`;
