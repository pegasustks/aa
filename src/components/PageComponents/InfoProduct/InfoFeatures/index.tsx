import { SectionComponent } from '@components/Section';
import {
  WapperInfoTopPlus,
  WapperCardsInfos,
  ItemInformation,
  WapperAllValueOfInformation,
  ValueInfoContent,
  ResponseValueInfo,
  WapperValues,
  ValueInformation,
  ListInformationProductPlus,
  ValueTopInfoInfoPlus,
  WapperContainer,
} from './styles';

interface InfoFeatures_I {
  title: string;
}

export const InfoFeturesComponent: React.FC<InfoFeatures_I> = ({
  title,
}): JSX.Element => {
  return (
    <SectionComponent
      title={{
        style: { size: 30 },
        value: `Mais informações para o produto: ${title}`,
      }}
    >
      <WapperCardsInfos>
        <ListInformationProductPlus>
          <ItemInformation>
            <ValueInformation>Caracteristicas gerais</ValueInformation>
            <WapperAllValueOfInformation>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Tamanho</ValueInfoContent>
                <ResponseValueInfo>420mm x 320mm</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
            </WapperAllValueOfInformation>
          </ItemInformation>
        </ListInformationProductPlus>
        <ListInformationProductPlus>
          <ItemInformation>
            <ValueInformation>Caracteristicas gerais</ValueInformation>
            <WapperAllValueOfInformation>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Tamanho</ValueInfoContent>
                <ResponseValueInfo>420mm x 320mm</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
            </WapperAllValueOfInformation>
          </ItemInformation>
        </ListInformationProductPlus>
        <ListInformationProductPlus>
          <ItemInformation>
            <ValueInformation>Caracteristicas gerais</ValueInformation>
            <WapperAllValueOfInformation>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Tamanho</ValueInfoContent>
                <ResponseValueInfo>420mm x 320mm</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
            </WapperAllValueOfInformation>
          </ItemInformation>
        </ListInformationProductPlus>
        <ListInformationProductPlus>
          <ItemInformation>
            <ValueInformation>Caracteristicas gerais</ValueInformation>
            <WapperAllValueOfInformation>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Tamanho</ValueInfoContent>
                <ResponseValueInfo>420mm x 320mm</ResponseValueInfo>
              </WapperValues>
              <WapperValues>
                <ValueInfoContent>Cor</ValueInfoContent>
                <ResponseValueInfo>Vermelho</ResponseValueInfo>
              </WapperValues>
            </WapperAllValueOfInformation>
          </ItemInformation>
        </ListInformationProductPlus>
      </WapperCardsInfos>
    </SectionComponent>
  );
};
