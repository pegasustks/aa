import styled, { css } from "styled-components";

export const WapperContainer = styled.div`
  margin: 20px 0;
`;

export const WapperInfoProduct = styled.div`
  margin-top: 20px;

  background: ${({ theme }) => theme.colors.white};
  padding: 10px;
  position: relative;

  // @media screen and (max-width: 920px) {
  //   grid-template-columns: 1fr;
  //   row-gap: 20px;
  //   margin-top: 7px;
  // }
`;

export const WapperContentInfoTop = styled.div`
  display: grid;
  grid-template-columns: 1fr minmax(260px, 350px);
  align-items: flex-start;

  @media screen and (max-width: 963px) {
    grid-template-columns: 1fr 310px;
  }
  @media screen and (max-width: 781px) {
    grid-template-columns: 1fr 290px;
  }
  @media screen and (max-width: 715px) {
    grid-template-columns: 100%;
    row-gap: 20px;
  }

  margin-bottom: 40px;
`;

export const WapperImagesProduct = styled.div`
  display: flex;

  @media screen and (max-width: 925px) {
    display: flex;
    flex-direction: column-reverse;
  }
`;

export const ListImages = styled.ul`
  width: 71.2px;
  margin-right: 10px;
  max-height: 450px;
  height: 100%;
  list-style: none;
  @media screen and (max-width: 925px) {
    display: flex;
    width: 100%;
    margin-right: 0;
    height: 76px;
    // margin-top: 3px;
  }
`;

interface ItemImageProps {
  active?: boolean;
}

export const ItemImage = styled.li<ItemImageProps>`
  max-width: 71.2px;
  max-height: 80px;
  height: 80px;
  border: 2px solid ${({ active, theme }) => (active ? theme.background.primary : "#ffffff")};
  margin: 5px 0;

  cursor: pointer;

  :first-child {
    margin-top: 0;
  }
  :last-child {
    margin-bottom: 0;
  }

  @media screen and (max-width: 925px) {
    margin: 0 3px;
    :first-child {
      margin-left: 0;
    }
    :last-child {
      margin-right: 0;
    }
  }
`;

export const WapperImage = styled.div<{}>`
  max-width: 700px;
  min-width: 270px;
  max-height: 789px;
  width: 100%;
  overflow: hidden;

  img {
    object-fit: cover;
    display: block;
  }
`;

export const WapperInfoOfProduct = styled.div`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media screen and (max-width: 920px) {
    margin-left: 8px;
  }
  @media screen and (max-width: 715px) {
    margin-left: 0px;
  }
`;

export const LoadFixed = styled.div`
  position: absolute;
  z-index: 999;
  background: #f5f5f5ea;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  @media screen and (max-width: 1155px) {
    width: calc(100% - 20px);
    transform: translateX(10px);
  }
`;

export const DivFeatures = styled.div<{ change: boolean }>`
  margin: 3px 0;
  width: 100%;

  opacity: ${({ change }) => (change ? 0.5 : 1)};
`;

export const ValueTop = styled.button`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding: 15px 0;
  margin-bottom: 5px;

  background: transparent;
  border: 0;
  cursor: pointer;

  p {
    color: ${({ theme }) => theme.colors.black};;
    font-size: 16px;
    font-family: sans-serif;
    font-weight: 600;
    letter-spacing: 0.4px;
  }
`;

export const WapperInfoTop = styled.div``;

export const ValueInfoTop = styled.span`
  font-family: ${({ theme }) => theme.font.primary};
  color: ${({ theme }) => theme.colors.grey};
  font-size: 15px;
  cursor: default;
  :hover {
    color: ${({ theme }) => theme.colors.hover.black};
  }
  transition: 0.3s ease;
`;

export const WapperContentInfos = styled.div`
  margin-top: 10px;
`;

export const TitleProduct = styled.h1`
  font-family: ${({ theme }) => theme.font.primary};
  min-height: 90px;
  width: 100%;
  font-size: 30px;
  font-weight: 300;
  line-height: 29px;
  color: ${({ theme }) => theme.colors.black};
`;

export const WapperPrices = styled.div<{ change: boolean }>`
  margin: 20px 0;
  display: flex;
  flex-direction: column;
  opacity: ${({ change }) => (change ? 0.5 : 1)};
`;

export const ValueOldPrice = styled.span`
  font-size: 24px;
  position: relative;
  color:  ${({ theme }) => theme.colors.grey};

  position: relative;
  ::after {
    content: "";
    width: 100%;
    height: 2px;
    background:  ${({ theme }) => theme.colors.grey};
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
  }
`;

export const WapperValueActualPrice = styled.div`
  display: flex;
  align-items: center;
`;

export const ValuePorcent = styled.small`
  color:  ${({ theme }) => theme.colors.grey};
  margin-left: 10px;
  font-size: 18px;
  font-family: sans-serif;
  font-weight: 300;
  letter-spacing: 0.4px;

  span {
    font-size: 14px;
  }
`;

export const ValueActualPrice = styled.span`
  font-family: sans-serif;
  color:  ${({ theme }) => theme.colors.primary};
  font-size: 45px;
  font-weight: 500;
  letter-spacing: -0.4px;
`;

export const WapperFrete = styled.div`
  display: flex;
  flex-direction: row;
`;

export const ValueFrete = styled.span`
  color: ${({ theme }) => theme.colors.black};
  font-size: 22px;
  font-weight: 600;
  text-transform: uppercase;
  margin-bottom: 5px;
`;

export const ValueInfoFrete = styled.span`
  color: ${({ theme }) => theme.colors.grey};
  font-size: 20px;
  strong {
    color: ${({ theme }) => theme.colors.black};
  }
`;

export const WapperSelectActionProduct = styled.div`
  margin: 10px 0;
`;

export const DivFrete = styled.div`
  box-shadow: 0 1px 2px #49494952;
  background: ${({ theme }) => theme.background.white2};
  display: flex;
  align-items: center;
  font-size: 14px;

  padding: 10px 20px;

  div :first-child {
    margin-right: 15px;
  }

  p {
    line-height: 21px;
    margin: 3px 0;
    font-family: sans-serif;
  }

  svg {
    color:  ${({ theme }) => theme.colors.primary};
    fill: ${({ theme }) => theme.colors.primary};
  }
`;

export const WapperActionUpProduct = styled.div`
  display: grid;
  grid-template-columns: 140px 1fr;
  grid-column-gap: 10px;
  width: 100%;
`;

export const WapperRepository = styled.div`
  margin-top: 10px;
`;

export const ValueRepository = styled.p<{ on?: boolean }>`
  color: ${({ on, theme }) => (on ? "#f70d0d" : theme.colors.grey)};
  font-family: sans-serif;

  font-size: ${({ on }) => (on ? "14px" : "14px")};
  font-weight: ${({ on }) => (on ? "600" : "400")};
`;

export const WapperButtonActionsProduct = styled.div`
  margin-bottom: 10px;
`;

interface Button_I {
  background: string;
  hover: string;
  in_?: boolean;
}

export const ButtonAction = styled.button<Button_I>`
  outline: none;
  background: ${({ theme }) => theme.background.primary};
  cursor: ${({ in_ }) => (in_ ? "default" : "pointer")};
  //'#4fa300'
  :hover {
    background: ${({ theme }) => theme.background.hover.primary};
  }

  font-weight: 600;
  height: 45px;
  max-width: 240px;
  width: 100%;
  color:  ${({ theme }) => theme.colors.white};
  font-size: 14px;
  letter-spacing: 0.8px;
  margin: 0 5px;
  border: none;

  :first-child {
    margin-left: 0;
  }

  :last-child {
    margin-right: 0;
  }
`;

export const ListIs = styled.ul``;

export const ItemIs = styled.li`
  margin: 5px 0;
  :first-child {
    margin-top: 0;
  }
  :last-child {
    margin-bottom: 0;
  }

  display: flex;
  font-size: 15px;
  p {
    letter-spacing: 0.4px;
    font-weight: 600;
    margin-right: 15px;
  }
`;

export const ContentMeasu = styled.div`
  overflow: hidden;
  overflow-x: scroll;
  max-width: 100%;
  padding-bottom: 4px;

  ::-webkit-scrollbar-track {
    background-color: ${({ theme }) => theme.background.white};
  }

  ::-webkit-scrollbar {
    background-color: ${({ theme }) => theme.background.white};
    height: 4px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 20px;
    background-color:  ${({ theme }) => theme.background.primary};
  }

  th {
    padding: 0 4px;
    min-width: 120px;
    text-align: start;
    font-size: 14px;
  }
  tr {
    :first-child {
      :hover {
        background: ${({ theme }) => theme.background.white};
      }
    }
    :hover {
      background: ${({ theme }) => theme.background.hover.white2};
    }
  }
  td {
    font-size: 14px;
    padding: 0 4px;
    height: 37px;
    :hover {
      background: ${({ theme }) => theme.background.hover.white2};
    }
  }
`;

export const InfoMensu = styled.div`
  margin: 5px 0;
  p {
    font-size: 14px;
    font-family: sans-serif;
    color: ${({ theme }) => theme.colors.grey};
  }
`;

// export const ButtonSeeOptions = styled.button``;

export const WapperInfoPlusProduct = styled.div`
  margin-top: 40px;
`;
