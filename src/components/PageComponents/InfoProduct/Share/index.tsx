import { AiFillInstagram, AiOutlineLink } from "react-icons/ai";
import { IoLogoWhatsapp } from "react-icons/io";
import { Item, ListShare, Notf, WapperContainer } from "./styles";
import copy from "copy-to-clipboard";
import { useState } from "react";

interface share_prop {
  link: string;
}

export const ShareComponent: React.FC<share_prop> = ({
  link,
}: share_prop): JSX.Element => {
  const [on_copy, set_on_copy] = useState<boolean>(false as boolean);

  return (
    <WapperContainer>
      <ListShare>
        <span>Compartilhe:</span>
        <Item title={"Compartilhar no Whatsapp"}>
          <a
            target="_blank"
            rel="noreferrer"
            href={`whatsapp://send?text=${encodeURIComponent(link)}`}
          >
            <IoLogoWhatsapp size={25} color={"#1f1f1f"} />
          </a>
        </Item>
        <Item
          onClick={() => {
            set_on_copy(false);
            set_on_copy(true);
            setTimeout(() => {
              copy(link);
              set_on_copy(false);
            }, 1400);
          }}
          title={"Copiar link"}
        >
          <AiOutlineLink size={27} color={"#1f1f1f"} />
          <Notf onClick={() => alert("o")} cop={on_copy}>
            <small>Copiado</small>
          </Notf>
        </Item>
      </ListShare>
    </WapperContainer>
  );
};
