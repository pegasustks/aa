import styled from "styled-components";

export const WapperContainer = styled.div``;

export const ListShare = styled.div`
  display: flex;
  align-items: center;

  span {
    transform: translateY(-2px);
    color: ${({ theme }) => theme.colors.grey};
    font-size: 14px;
  }
`;

export const Item = styled.div`
  margin: 0 10px;
  cursor: pointer;
  position: relative;
  :first-child {
    margin-left: 0;
  }
  :last-child {
    margin-right: 0;
  }
`;

export const Notf = styled.div<{ cop: boolean }>`
  visibility: ${({ cop }) => (cop ? "visible" : "hidden")};
  position: absolute;
  bottom: -40%;
  left: -50%;
  transform: translateX(4px);
`;
