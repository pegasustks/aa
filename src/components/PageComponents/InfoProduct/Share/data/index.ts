import { IoLogoWhatsapp } from 'react-icons/io';
import { IoLinkSharp } from 'react-icons/io5';

export const share = [
  {
    title: 'Compartilhar no Whatsapp',
    Icon: IoLogoWhatsapp,
    id: '0',
    color: '#0bc404',
    link: '/',
  },
  {
    title: 'Compartilhar Link com amigo',
    Icon: IoLinkSharp,
    id: '1',
    color: '#000',
    link: '/',
  },
];
