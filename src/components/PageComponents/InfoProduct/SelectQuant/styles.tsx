import styled from "styled-components";

export const WapperContainer = styled.div`
  margin-top: 10px;
  display: flex;
  position: relative;
  max-width: 140px;
  width: 100%;
  z-index: 98;
`;

export const ButtonSelect = styled.button`
  width: 100%;
  display: flex;
  flex-direction: column;
  z-index: 999;

  background: none;
  outline: none;
  border: none;
  cursor: pointer;
`;

export const WapperOther = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  position: relative;
  height: 35px;
  padding: 0 10px;
  margin-top: 5px;
  background: ${({ active }) => (active ? "#c8c8c8" : "#f0f0f0")};
`;

export const ValueNumberSelected = styled.span`
  font-size: 15px;
  font-weight: 600;
`;

export const WapperIcon = styled.div``;

export const Value = styled.span`
  font-family: sans-serif;
  font-size: 15px;
  font-weight: 600;
`;

export const ValueQnt = styled.span`
  font-family: sans-serif;
  font-size: 13px;
  letter-spacing: 0.4px;

  margin-right: 10px;
`;

export const WapperModalSelect = styled.div<{ open: boolean }>`
  display: ${({ open }) => (open ? "flex" : "none")};
  flex-direction: column;
  position: absolute;

  top: 37px;
  right: 0;

  width: 100%;
  padding: 3px;
  background:  ${({ theme }) => theme.background.white2};
  box-shadow: 4px 4px 4px #7d7d7d63;
`;

export const OptionQnt = styled.span`
  padding: 12px 6px;
  font-size: 16px;
  font-family: sans-serif;
  text-align: start;

  :hover {
    background: ${({ theme }) => theme.background.hover.white2};
  }
`;

export const WapperSelectM = styled.div`
  display: flex;
  align-items: center;
`;

export const InputAmount = styled.input`
  width: 100%;
  border: none;
  background: ${({ theme }) => theme.background.white};
  outline: none;
  height: 30px;
  margin-right: 10px;
  margin-left: 5px;

  padding-left: 10px;
  font-size: 17px;
  ::placeholder {
    color: ${({ theme }) => theme.colors.black};
  }
  -moz-appearance: textfield;
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

export const WapperButton = styled.div`
  border: none;
  background: ${({ theme }) => theme.colors.primary};
  transition: 0.3s ease;
  cursor: pointer;
  :hover {
    background:  ${({ theme }) => theme.background.hover.primary};
  }
  outline: none;
  height: 30px;
  min-width: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
