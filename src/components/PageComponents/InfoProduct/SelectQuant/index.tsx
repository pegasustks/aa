import { AuthContext } from "contexts/AuthContext";
import { InfoProductContext } from "contexts/InfoProductContext";
import { useCallback, useContext, useRef, useState } from "react";
import { BiChevronDown, BiChevronUp } from "react-icons/bi";
import { GoCheck } from "react-icons/go";
import {
  WapperButton,
  ButtonSelect,
  InputAmount,
  OptionQnt,
  Value,
  ValueNumberSelected,
  WapperContainer,
  WapperIcon,
  WapperModalSelect,
  WapperOther,
  WapperSelectM,
} from "./styles";

interface SelectAmount {
  repository?: number;
}

export const SelectAmountComponent: React.FC<SelectAmount> = ({
  repository,
}): JSX.Element => {
  const [open, setOpen] = useState(false);
  const [mouseEnter, setMouseEnter] = useState(false);

  const { user_data } = useContext(AuthContext);
  const { handle_value_request, data_request, on_reload_frete_product } = useContext(InfoProductContext);

  const input = useRef<any>(null);

  const handle = useCallback(
    async (vl: number) => {
      if (user_data?.cep) {
        await on_reload_frete_product(user_data.cep, String(vl));
      }
      handle_value_request("amount", vl);
      setOpen(false);
    },
    [handle_value_request, on_reload_frete_product, user_data?.cep]
  );

  return (
    <WapperContainer
      onBlur={
        !mouseEnter ? () => setTimeout(() => setOpen(false), 110) : undefined
      }
    >
      <ButtonSelect onClick={!open ? () => setOpen(true) : undefined}>
        <Value>Quantidade:</Value>
        <WapperOther active={open}>
          <ValueNumberSelected>
            {data_request?.amount}{" "}
            {data_request?.amount! > 1 ? "unidades" : "unidade"}
          </ValueNumberSelected>
          <WapperIcon>
            {open ? <BiChevronUp size={20} /> : <BiChevronDown size={20} />}
          </WapperIcon>
          <WapperModalSelect open={open}>
            <OptionQnt onClick={() => handle(1)}>1 unidade</OptionQnt>
            <OptionQnt onClick={() => handle(2)}>2 unidades</OptionQnt>
            <OptionQnt onClick={() => handle(3)}>3 unidades</OptionQnt>
            {repository && repository > 3 && (
              <OptionQnt style={{ padding: 5 }}>
                <WapperSelectM>
                  <InputAmount
                    type="number"
                    ref={input}
                    onMouseLeave={() => setMouseEnter(false)}
                    onMouseEnter={() => setMouseEnter(true)}
                    defaultValue={4}
                    max={repository}
                  />
                  <WapperButton
                    onClick={() => handle(Number(input.current?.value))}
                  >
                    <GoCheck size={18} color="#fff" />
                  </WapperButton>
                </WapperSelectM>
              </OptionQnt>
            )}
          </WapperModalSelect>
        </WapperOther>
      </ButtonSelect>
    </WapperContainer>
  );
};
