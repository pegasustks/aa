import styled from 'styled-components';

export const WapperContainer = styled.div``;

export const WapperCategory = styled.div`
  display: flex;
  margin: 20px 0;
`;
export const WapperItem = styled.div`
  margin: 10px 0;
  width: 100%;
`;

export const ValueInfoTop = styled.h2`
  font-size: 14px !important;
  strong {
    text-transform: capitalize;
  }
  font-family: ${({ theme }) => theme.font.primary};
  font-weight: 300;
  color: ${({ theme }) => theme.colors.primary};
`;
