import { AdvertisingSimpleComponent } from "@components/Advertising/Simple";
import { GridProducts } from "@components/Global/styles";
import { ProductItem } from "@components/Product";
import { SectionComponent } from "@components/Section";
import { product_I } from "pages/categoria";
import {
  ValueInfoTop,
  WapperCategory,
  WapperContainer,
  WapperItem,
} from "./styles";

interface Values_I {
  trail: {
    category: string | null;
    subcategory: string | null;
    subcategory2?: string | null;
    subcategory3?: string | null;
  } | null;
  products: product_I[];
}

export const SubCategoryComponent: React.FC<Values_I> = ({
  trail,
  products,
}): JSX.Element => {
  return (
    <WapperContainer>
      <AdvertisingSimpleComponent />

      <SectionComponent
        title={{
          children: trail?.category ? (
            <ValueInfoTop>
              {`${trail.category} > ${trail.subcategory} > ${trail.subcategory2} > ${trail.subcategory3}`}
            </ValueInfoTop>
          ) : undefined,
        }}
      >
        <GridProducts>
          {products.map((item) => (
            <ProductItem key={item.id} {...item} />
          ))}
        </GridProducts>
      </SectionComponent>
    </WapperContainer>
  );
};
