import styled, { css } from "styled-components";

export const SuperC = styled.div`
  max-width: 252px;
  width: 100%;
  background-color: #fff;

  padding: 1px;
  :hover {
    padding: 0;
    border: 1px solid ${({ theme }) => theme.background.primary};
  }
  box-shadow: 0px 10px 10px -8px rgba(88, 94, 70, 0.178);
  user-select: none;
  a {
    user-select: none;
  }
`;

export const DivOff = styled.div`
  position: absolute;
  background-color: ${({ theme }) => theme.background.primary};
  padding: 5px;
  color: #fff;
  font-size: 13px;
  font-weight: 500;
`;

export const Container = styled.div<{ frete: boolean }>`
  width: 100%;
  transition: 0.4s ease;
  cursor: pointer;
`;

export const ContainerImage = styled.div`
  overflow: hidden;
  max-width: 100%;
  max-height: 289.12px;
`;

export const Image = styled.div<{ img?: string; size?: number }>`
  max-width: 100%;
  height: auto;
  transform: translateX(0.5px);

  background-image: url(${({ img }) => img});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const WapperInfo = styled.div`
  // max-width: 300px;
  padding: 7px;
  width: 100%;
`;

export const DivBoxTitle = styled.div`
  
`;

export const TitleProduct = styled.p`
  font-weight: 300;
  font-size: 14.5px;
  color: #202020;

  height: 33px;
  display: flex;
  justify-content: center;
  align-items: center;

  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  line-clamp: 2;
  -webkit-box-orient: vertical;
`;

export const DivBottomProduct = styled.div`
  display: flex;
  flex-direction: column;
`;
export const ValuesPriceAndFrete = styled.div`
  width: 100%;
`;
export const CartSale = styled.button`
  border: none;
  outline: none;
  background: #f5f5f5;
  min-width: 55px;
  min-height: 55px;
  transform: translateY(-13px);
  margin-right: 10px;

  cursor: pointer;
`;

export const WapperPrice = styled.div`
  width: 100%;
  margin-top: 5px;
  display: flex;
  flex-wrap: wrap;

  column-gap: 4px;
  align-items: center;
  justify-content: space-between;
`;

export const PriceOld = styled.s`
  font-size: 16px;
  color: #b6b6b6;
`;

export const WapperDraftPrice = styled.div`
  display: flex;
  align-items: center;
  margin-top: 3px;
`;

export const DivPrices = styled.div`
  margin-bottom: 4px;
  display: flex;
  align-items: center;
`;

export const PriceActual = styled.strong`
  font-size: 19px;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.primary};
`;

export const PorcetPrice = styled.span`
  color: #8b8b8b;
  font-family: sans-serif;
  margin-left: 10px;
  font-size: 13px;
  font-weight: 100;
  letter-spacing: 0.4px;

  span {
    font-size: 14px;
  }
`;

export const Sales = styled.small`
  color: #6b6b6b;
`;
