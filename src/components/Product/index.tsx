import Image from "next/image";
import { useRouter } from "next/router";
import { product_I } from "pages/categoria";
import { ProviderAPI } from "services/api";
import {
  Container,
  ContainerImage,
  DivBottomProduct,
  DivBoxTitle,
  DivOff,
  DivPrices,
  PorcetPrice,
  PriceActual,
  PriceOld,
  Sales,
  SuperC,
  TitleProduct,
  WapperInfo,
  WapperPrice,
} from "./styles";
import Link from "next/link";
import { useContext } from "react";
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";

export const ProductItem: React.FC<product_I> = ({ ...props }): JSX.Element => {
  const { push } = useRouter();
  const { set_load_click } = useContext(TotalLoadingDataContext);

  return (
    <SuperC id="prod">
      <Link onClick={() => set_load_click(true)} href={`/${props.id}`} prefetch={false}>
        {props?.main_price2 && Number(props.main_price1) < Number(props.main_price2) && (
          <DivOff>
            <span>
              {String(((Number(props.main_price2) - Number(props.main_price1)) /
                Number(props.main_price2)) * 100).slice(0, 2)
              }%<br />OFF
            </span>
          </DivOff>
        )}
        <Container
          frete={false}
        // frete={(user_data.city ?? city?.split(' - ')[1]) === props.city}
        >

          <ContainerImage>
            <Image
              src={`${ProviderAPI.defaults.baseURL}/v1/images/products/${props.main_image}`}
              // src={props.main_image}
              width={250}
              height={278.75}
              alt="Imagem"
              style={{ height: "auto", maxWidth: "100%" }}
            />
          </ContainerImage>

          <WapperInfo>
            <DivBoxTitle>
              <TitleProduct title={props.title}>{props.title}</TitleProduct>
            </DivBoxTitle>

            <DivBottomProduct>
              <WapperPrice>
                <div>
                  <PriceOld>
                    {props.main_price2 && (
                      <>R$ {Number(props.main_price2).toFixed(2)}</>
                    )}
                  </PriceOld>
                </div>
                <DivPrices>
                  <PriceActual>
                    R$ {Number(props.main_price1).toFixed(2)}
                  </PriceActual>
                </DivPrices>
              </WapperPrice>
              <Sales>
                {props.sales ?? 0}{" "}
                {props.sales === 1 || !props.sales ? "vendido" : "vendidos"}
              </Sales>
            </DivBottomProduct>
          </WapperInfo>
        </Container>
      </Link>
    </SuperC>
  );
};
