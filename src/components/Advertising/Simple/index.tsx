/* eslint-disable @next/next/no-img-element */
import { TotalLoadingDataContext } from "contexts/TotalLoadingDataContextContext";
import Link from "next/link";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { ProviderAPI } from "services/api";
import { useIsVisible } from "react-is-visible";
import { LoadComponent } from "@components/Load";
import { ListenSizeContext } from "contexts/ListenSize";
import { WapperPublicity, ImagePublicity } from "./styles";
import Image from "next/image";

type item_prop = {
  id: string;
  link: string;
  long_image: string;
  medium_image: string;
  small_image: string;
  title: string;
};

export const AdvertisingSimpleComponent: React.FC = (): JSX.Element => {
  // buscar o propaganda simples
  const [item, set_item] = useState<item_prop | null>(null as item_prop | null);
  const nodeRef = useRef(null);
  const isVisible = useIsVisible(nodeRef, { once: true });

  const on_increment_advertising = useCallback(async (id: string) => {
    await ProviderAPI.put(`/v1/public/update/a-increment/${id}`);
  }, []);

  const [load, set_load] = useState<boolean>(false);
  useEffect(() => {
    if (load) {
      on_increment_advertising(item!.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  const get_advertising = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get(
        "/v1/public/get/outhers-advertising"
      );
      set_item(data.data);
      set_load(true);
    } catch (error) {
      return undefined;
    }
  }, []);

  useEffect(() => {
    if (get_advertising) get_advertising();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [get_advertising]);

  return (
    <WapperPublicity>
      {load && item && (
        <>
          {item?.link !== "undefined" && (
            <Link ref={nodeRef} href={item.link}>
              <img
                src={`${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${item.long_image}`}
                alt="imagem"
                style={{ height: "auto", maxWidth: "100%" }}
              />
            </Link>
          )}
          {item?.link === "undefined" && (
            <a ref={nodeRef}>
              <img
                src={`${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${item.long_image}`}
                alt="imagem"
                style={{ height: "auto", maxWidth: "100%" }}
              />
            </a>
          )}
        </>
      )}
    </WapperPublicity>
  );
};
