import styled from 'styled-components';

export const WapperContainer = styled.div``;

export const WapperImage = styled.div``;

export const Image = styled.div<{ img?: string }>`
  width: 100%;
  height: 510px;

  background-image: url(${({ img }) => img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const WapperPublicity = styled.div`
  width: 100%;
  margin-top: 10px;

  display: flex;
  justify-content: center;
  align-items: center;

  a {
    width: 100%;
  }
`;

export const ImagePublicity = styled.div<{ img: string }>`
  width: 100%;
  height: 470px;

  background-image: url(${({ img }) => img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;
