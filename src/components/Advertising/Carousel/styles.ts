import styled from "styled-components";

export const WapperContainer = styled.div`
  .react-multiple-carousel__arrow {
    z-index: 0 !important;
  }
`;

export const WapperImage = styled.div``;

export const Imagex = styled.div<{ img?: string; size: number }>`
  width: 100%;
  height: ${({ size }) =>
    size <= 410 ? "335px" : size <= 670 ? "380px" : "470px"};

  background-image: url(${({ img }) => img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;
