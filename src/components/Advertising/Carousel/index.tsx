/* eslint-disable @next/next/no-img-element */
import Carousel from "react-multi-carousel";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { useIsVisible } from "react-is-visible";
import { Advertising_I } from "contexts/TotalLoadingDataContextContext";
import { ProviderAPI } from "services/api";
import { ListenSizeContext } from "contexts/ListenSize";
import { WapperContainer, Imagex, WapperImage } from "./styles";
import Image from "next/image";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const RenderItemsAdvertising: React.FC<
  Advertising_I & { visibled: (e: string) => void }
> = ({ ...item }): JSX.Element => {
  const nodeRef = useRef(null);
  const isVisible = useIsVisible(nodeRef, { once: true });
  const { size } = useContext(ListenSizeContext);

  useEffect(() => {
    if (isVisible) {
      item.visibled(item.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  return (
    <WapperImage ref={nodeRef}>
      <img src={
        size <= 410
          ? item.small_image
          : size <= 670
            ? item.medium_image
            : item.long_image
      } alt="imagem"
        style={{ height: "auto", maxWidth: "100%" }}
      />
    </WapperImage>
  );
};

export const AdvertisingCarousel: React.FC = (): JSX.Element => {
  const [a, set_a] = useState<string[]>([]);

  // PUBLICIDADE
  const [advertisings, setAdvertisings] = useState<Advertising_I[]>(
    [] as Advertising_I[]
  );

  const getPrincipalAdvertisings = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get(
        "/v1/public/get/principal-advertising"
      );
      const newState: Advertising_I[] = data.data?.map((e: any) => {
        return {
          id: e.id,
          link: e.link,
          title: e.title,
          long_image: `${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${e.long_image}`,
          medium_image: `${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${e.medium_image}`,
          small_image: `${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${e.small_image}`,
        };
      });
      setAdvertisings(newState);
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, [setAdvertisings]);

  const on_increment_advertising = useCallback(async (id: string) => {
    await ProviderAPI.put(`/v1/public/update/a-increment/${id}`);
  }, []);

  useEffect(() => {
    getPrincipalAdvertisings();
  }, [getPrincipalAdvertisings]);

  return (
    <WapperContainer>
      <Carousel
        pauseOnHover
        responsive={responsive}
        autoPlay
        autoPlaySpeed={7000}
        infinite={advertisings.length > 1}
      >
        {advertisings.map((e) => (
          <RenderItemsAdvertising
            visibled={async (es) => {
              set_a([...a, es]);
              if (a.length > advertisings.length) {
                await on_increment_advertising(e.id);
              }
            }}
            {...e}
            key={e.id}
          />
        ))}
      </Carousel>
    </WapperContainer>
  );
};
