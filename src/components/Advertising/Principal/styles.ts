import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  background-color: #fff;
  padding: 10px 0;
  border-bottom: 1px solid #D5D5D5;
  box-shadow: 0px 2px 10px rgba(174, 198, 105, 0.08);

  @media screen and (max-width: 786px) {
    padding: 8px 0 5px;
  }
`;

export const WapperContainer = styled.div`
  .react-multiple-carousel__arrow {
    z-index: 0 !important;
  }
`;

export const WapperImage = styled.div``;

export const Imagex = styled.div<{ img?: string; size: number }>`
  width: 100%;
  height: ${({ size }) =>
    size <= 410 ? "335px" : size <= 670 ? "380px" : "470px"};

  background-image: url(${({ img }) => img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const DivGrid = styled.div`
  display: grid;
  grid-template-columns: 2.06fr 1fr;
  column-gap: 7px;

  @media screen and (max-width: 786px) {
    grid-template-columns: 1fr;
  }
`;

export const DivGrid2 = styled.div`
  display: grid;
  row-gap: 5px;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 1fr;

  @media screen and (max-width: 786px) {
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr;
    row-gap: 0px;
    column-gap: 5px;
  }
`;
