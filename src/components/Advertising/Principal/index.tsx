/* eslint-disable @next/next/no-img-element */
import Carousel from "react-multi-carousel";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { useIsVisible } from "react-is-visible";
import { Advertising_I } from "contexts/TotalLoadingDataContextContext";
import { ProviderAPI } from "services/api";
import { ListenSizeContext } from "contexts/ListenSize";
import { WapperContainer, WapperImage, Container, DivGrid, DivGrid2 } from "./styles";
import { MaxContainerComponent } from "@components/Global/MaxContainer";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const RenderItemsAdvertising: React.FC<
  Advertising_I & { visibled: (e: string) => void }
> = ({ ...item }): JSX.Element => {
  const nodeRef = useRef(null);
  const isVisible = useIsVisible(nodeRef, { once: true });

  useEffect(() => {
    if (isVisible) {
      item.visibled(item.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  return (
    <WapperImage ref={nodeRef}>
      <img src={item.long_image} alt="imagem"
        style={{ height: "auto", maxWidth: "100%" }}
      />
    </WapperImage>
  );
};

export const AdvertisingPrincipalPageHome: React.FC = (): JSX.Element => {
  const [a, set_a] = useState<string[]>([]);

  // PUBLICIDADE
  const [advertisings, setAdvertisings] = useState<Advertising_I[]>(
    [] as Advertising_I[]
  );

  const [advertisings2, setAdvertisings2] = useState<Advertising_I[]>(
    [] as Advertising_I[]
  );

  const getPrincipalAdvertisings = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get(
        "/v1/public/get/principal-advertising"
      );
      const newState: Advertising_I[] = data.data?.map((e: any) => {
        return {
          id: e.id,
          link: e.link,
          title: e.title,
          long_image: `${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${e.long_image}`,
        };
      });
      setAdvertisings(newState);
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, [setAdvertisings]);

  const getPrincipal2Advertisings = useCallback(async () => {
    try {
      const { data } = await ProviderAPI.get(
        "/v1/public/get/principal-2-advertising"
      );
      const newState: Advertising_I[] = data.data?.map((e: any) => {
        return {
          id: e.id,
          link: e.link,
          title: e.title,
          long_image: `${ProviderAPI.defaults.baseURL}/v1/images/advertisings/${e.long_image}`,
        };
      });
      setAdvertisings2(newState);
    } catch (error: any) {
      if (error?.response?.status === 401) throw { status: 401 };
      if (error?.response?.status === 500) throw { status: 500 };
      throw { status: 0 };
    }
  }, [setAdvertisings2]);

  const on_increment_advertising = useCallback(async (id: string) => {
    await ProviderAPI.put(`/v1/public/update/a-increment/${id}`);
  }, []);

  useEffect(() => {
    getPrincipalAdvertisings();
    getPrincipal2Advertisings();
  }, [getPrincipalAdvertisings, getPrincipal2Advertisings]);

  return (
    <Container>
      <MaxContainerComponent>
        <WapperContainer>
          <DivGrid>
            <Carousel
              containerClass="carousel_pi"
              pauseOnHover
              responsive={responsive}
              autoPlay
              infinite
              arrows={false}
              autoPlaySpeed={7000}
            >
              {advertisings.map((e) => (
                <RenderItemsAdvertising
                  visibled={async (es) => {
                    set_a([...a, es]);
                    if (a.length > advertisings.length) {
                      await on_increment_advertising(e.id);
                    }
                  }}
                  {...e}
                  key={e.id}
                />
              ))}
            </Carousel>
            <DivGrid2>
              {advertisings2.map((e) => (
                <RenderItemsAdvertising
                  visibled={async (es) => {
                    set_a([...a, es]);
                    if (a.length > advertisings.length) {
                      await on_increment_advertising(e.id);
                    }
                  }}
                  {...e}
                  key={e.id}
                />
              ))}
            </DivGrid2>
          </DivGrid>
        </WapperContainer>
      </MaxContainerComponent>
    </Container>
  );
};
