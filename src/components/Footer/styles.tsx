import styled from 'styled-components';

export const Container = styled.footer`
  background: ${({ theme }) => theme.background.primary};
  margin-top: 25px;
  box-shadow: 0px -2px 7px rgba(88, 88, 87, 0.685);

  @media screen and (max-width: 1155px) {
    padding: 0 10px;
  }
  // border-top: 1px solid #7c7c7c;
`;

export const Content = styled.div`
  padding: 30px 0;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  height: 100%;

  text-align:center;
`;

export const Row = styled.div`
  width:100%;

  color: #fff;

  &#redes {
    max-width: 200px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin: 0 auto;
  }

  a {
    color: #ff5100;
  }
`;

export const ListC = styled.ul`
  list-style: none;
  max-width: 750px;
  width: 100%;
  margin: 15px auto;
  display: flex;
  justify-content: center;  
  flex-wrap: wrap;
  column-gap: 20px;
  row-gap: 10px;

  li a {
    font-weight: 300;
    color: #f3f3f3;
  }
`;

export const ListLinksCompany = styled.ul`
  list-style: none;
  display: flex;
  max-width: 500px;
  width: 100%;
`;

export const ItemLInk = styled.li`
  color: #f3f3f3;
  font-family: sans-serif;
  font-size: 15px;
  margin: 0 10px;

  :first-child {
    margin-left: 0;
  }
  :last-child {
    margin-right: 0;
  }
`;

export const WapperInfoCompany = styled.div`
  margin-top: 10px;
`;

export const WapperEnd = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
`;

export const Copy = styled.p`
  color: #fff;
  font-family: sans-serif;
  font-size: 15px;

  a {
    font-weight: 500;
    color: #ffffff;
  }
`;
