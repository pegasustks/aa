import { MaxContainerComponent } from '@components/Global/MaxContainer';
import { linksFooter } from '@data/linksFooter';
import { useRouter } from 'next/router';
import {
  Container,
  Content,
  Copy,
  ItemLInk,
  ListC,
  ListLinksCompany,
  Row,
  WapperEnd,
} from './styles';
import { useContext } from 'react';
import { TotalLoadingDataContext } from 'contexts/TotalLoadingDataContextContext';
import { BsInstagram } from 'react-icons/bs';
import { FiTwitter } from 'react-icons/fi';
import { RiFacebookBoxLine } from 'react-icons/ri';
import { useTheme } from 'styled-components';
import Link from 'next/link'

export const FooterComponent: React.FC = (): JSX.Element => {
  const { push } = useRouter();
  const { social } = useContext(TotalLoadingDataContext);
  const { colors } = useTheme();

  return (
    <Container>
      <MaxContainerComponent>
        {/**
        <Content>
          <ListLinksCompany>
            {linksFooter.map(item => (
              <ItemLInk onClick={() => push(item.link)} key={item.id}>
                {item.value}
              </ItemLInk>
            ))}
          </ListLinksCompany>
          {/*
          <WapperInfoCompany>
            <WapperCnpjEnd>
              <Cnpj>CNPJ n.º {company.cnpj}</Cnpj>
              <End>{company.address}</End>
            </WapperCnpjEnd>
          </WapperInfoCompany>
           *
          <WapperEnd>
            <Copy>Copyright © 2023 - Desenvolvido por <a href="https://www.instagram.com/mercrian/" target='_blank' rel="noreferrer">Rian C.</a></Copy>
          </WapperEnd>
        </Content>*/}
        <Content>
          <Row id='redes'>
            {(social?.facebook || social?.instagram || social?.linkedin || social?.twitter) && (
              <>
                {social?.instagram && (
                  <a
                    href={social.instagram}
                    title={"Instagram"}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <BsInstagram size={20} color={colors.hover.white} />
                  </a>
                )}
                {social?.twitter && (
                  <a
                    href={social.twitter}
                    title={"Twitter"}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <FiTwitter size={23} color={colors.hover.white} />
                  </a>
                )}
                {social?.facebook && (
                  <a
                    href={social.facebook}
                    title={"Facebook"}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <RiFacebookBoxLine size={28} color={colors.hover.white} />
                  </a>
                )}
              </>
            )}
          </Row>
          <Row id={"rowli"}>
            <ListC style={{ marginTop: (social?.facebook || social?.instagram || social?.linkedin || social?.twitter) ? 15 : 0 }}>
              <li>
                <a href="https://wa.me/5571986751101?text=Quero%20saber%20mais%20sobre%20como%20vender%20na%20Lariizana" target='_blank' rel="noreferrer">Vender na Lariizana</a>
              </li>
              <li>
                <a href="https://wa.me/5571986751101?text=Quero%20saber%20mais%20sobre%20trocas%20ou%20devoluções" target='_blank' rel="noreferrer">Trocas - Devoluções - Garantia</a>
              </li>
              {linksFooter.map(item => (
                <li key={item.id}>
                  <Link href={item.link}>{item.value}</Link>
                </li>
              ))}
            </ListC>
          </Row>
          <Row>
            <small>
              Copyright © 2023 Lariizana - Todos os direitos reservados
            </small>
          </Row>
        </Content>
      </MaxContainerComponent>
    </Container>
  );
};
