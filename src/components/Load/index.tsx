import styled, { keyframes } from 'styled-components';

const elasticball = keyframes`
0% {
  transform: translateY(0) scaleY(1)
} 50% {
  transform: translateY(4rem) scaleY(.7)
} 100% {
  transform: translateY(0) scaleY(1)
}
`;
const elasticballColor = keyframes`
  0% {
    background-color: #000;
  } 30% {
    background-color: #000;
  } 50% {
    background-color: #000;
  } 80% {
    background-color: #000;
  }
`;
const elasticballBottom = keyframes`
  0% {
    transform: scale(1, 0.5) translateY(1rem);
    opacity: 0.3;
  } 100% {
    transform: scale(2, 0.5) translateY(1rem);
    opacity: 0;
  }
`;

const Elasticball = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  height: 6rem;
  margin-top: 20px;

  &:before {
    content: '';
    width: 2rem;
    height: 2rem;
    bottom: 0;
    opacity: 0;
    border-radius: 50%;
    position: absolute;
    background-color: #0000007b;
    transform: scaleY(0.5) translateY(1rem);
    animation: ${elasticballBottom} 0.5s 0.3s infinite;
  }
`;
const Elasticballball = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  transform-origin: 50% 100%;
  animation: ${elasticball} 0.5s infinite cubic-bezier(1, -0.01, 0, 1);

  i {
    width: 100%;
    height: 100%;
    display: block;
    border-radius: 50%;
    background-color: #000;

    animation: ${elasticballColor} 2s infinite;
  }
`;

export const LoadComponent: React.FC = (): JSX.Element => {
  return (
    <Elasticball>
      <Elasticballball>
        <i />
      </Elasticballball>
    </Elasticball>
  );
};
