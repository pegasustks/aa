export interface Product_I {
  id: string;
  title: string;
  price: {
    actual: string;
    old?: string;
    porcent?: string;
  };
  frete?: boolean;
  image: string;
  slug: {
    category: string;
    subCategory: string;
    filter: string;
    idProduct: string;
  };
}

export interface AllInfoProduct_I extends Product_I {
  qnt_sales: string;
  description: string;
  new?: boolean;
  repository: number;
  colors: {
    title: string;
    id: string;
    value: string;
    name: string;
  }[];
  images: {
    id: string;
    image?: string;
  }[];
}

export interface ItemCategory_I {
  items: {
    name: string;
    id: string;
    image?: string;
    link: string;
  }[];
}
