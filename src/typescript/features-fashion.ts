

export type FeaturesFashion = {
  translucent: number;
  genre: number;
  age: string[];
  season: string[];
  length: string;
  sleeve: string;
  model: string;
  styles_fashion: string[];
  composition: string;
  collar_type: string;
  print: string;
  tissue: string;
  type_neckline: string;
  closure: string;
}
